��    <      �  S   �      (     )  4  F     {  R   �  g   �  Z   O  '   �     �     �     �  	   	  	   	  	   	  (   )	  )   R	     |	     �	  '   �	  "   �	     �	     �	     	
     
     
      ,
     M
     U
  !   d
     �
  '   �
  %   �
  	   �
  '   �
       P        f     }     �  
   �  ;   �  3   �  Q        g     v  &   �     �  *   �      �        /   2     b  =   ~  	   �     �     �     �     �  (   �       �  %  4   
  4  ?  R   t  �   �  $  �  �   �  a   �     *  Y   8  F   �  (   �  !        $  �   @  v   �     Q  7   m  �   �  a   ,  a   �  F   �  	   7     A  .   `  v   �       (     G   B  $   �  �   �  t   5  H   �  �   �  .   v  �   �  (   m  Y   �     �     �  �     �   �  �   ;   N   !!  G   p!  a   �!  :   "  �   U"  g   �"  c   O#  �   �#  5   \$  |   �$  -   %  '   =%     e%     z%  '   �%  }   �%  C   0&     $             3       0                  4                    -       	           7          1   )           #   *      (                 "                &   +      /         5   6            9   ;         <       2           '   !           ,             :   .   %       
                   8        - the GNOME session manager %s [OPTION...] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can't recover.
Please log out and try again. A problem has occurred and the system can't recover. All extensions have been disabled as a precaution. A problem has occurred and the system can't recover. Please contact a system administrator A session named ‘%s’ already exists AUTOSTART_DIR Additional startup _programs: Allow logout Browse… Co_mmand: Comm_ent: Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Do not load user-specified applications Don't prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Wayland Ignoring any existing inhibitors Log out Not responding Oh no!  Something has gone wrong. Options Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Session %d Session names are not allowed to contain ‘/’ characters Session names are not allowed to start with ‘.’ Session names are not allowed to start with ‘.’ or contain ‘/’ characters Session to use Show extension warning Show the fail whale dialog for testing Startup Programs This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME This session logs you into GNOME, using Wayland Version of this application _Automatically remember running applications when logging out _Continue _Log Out _Log out _Name: _New Session _Remember Currently Running Applications _Remove Session Project-Id-Version: gnome-session.master.or
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-session&keywords=I18N+L10N&component=general
PO-Revision-Date: 2014-08-25 17:04+0530
Last-Translator: Manoj Kumar Giri <mgiri@redhat.com>
Language-Team: Oriya <oriya-it@googlegroups.com>
Language: or
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n!=1);







  - GNOME ଅଧିବେଶନ ପରିଚାଳକ %s [OPTION...] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s ଏକ ସ୍ୱତନ୍ତ୍ରଚର ଆବଶ୍ୟକ କରିଥାଏ
 ଏକ ସମସ୍ୟା ଘଟିଛି ଏବଂ ତନ୍ତ୍ର ଠିକ ହୋଇପାରୁ ନାହିଁ। 
ଦୟାକରି ଲଗଆଉଟ କରି ପୁଣିଥରେ ଚେଷ୍ଟା କରନ୍ତୁ। ଏକ ସମସ୍ୟା ଘଟିଛି ଏବଂ ତନ୍ତ୍ର ଠିକ ହୋଇପାରୁ ନାହିଁ। ସୁରକ୍ଷା ଦୃଷ୍ଟିକୋଣରୁ ସମସ୍ତ ଅନୁଲଗ୍ନଗୁଡ଼ିକୁ ନିଷ୍କ୍ରିୟ କରାଯାଇଛି। ଏକ ସମସ୍ୟା ଘଟିଛି ଏବଂ ତନ୍ତ୍ର ଠିକ ହୋଇପାରୁ ନାହିଁ। ଦୟାକରି ତନ୍ତ୍ର ପ୍ରଶାସକଙ୍କ ସହିତ ସମ୍ପର୍କ କରନ୍ତୁ ‘%s’ ନାମକ ଅଧିବେଶନ ପୂର୍ବରୁ ଅବସ୍ଥିତ ଅଛି AUTOSTART_DIR ଅତିରିକ୍ତ ବ୍ଯବସ୍ଥାପକ ପ୍ରୋଗ୍ରାମ (_p): ଲଗଆଉଟ  ପାଇଁ ଅନୁମତି ଦିଅନ୍ତୁ ବ୍ରାଉଜ କରନ୍ତୁ... ନିର୍ଦ୍ଦେଶ (_m): ମନ୍ତବ୍ୟ (_e): ଅଧିବେଶନ ପରିଚାଳକଙ୍କ ସହିତ ସମ୍ପର୍କ ସ୍ଥାପିତ କରିପାରିଲା ନାହିଁ ICE କୁ ମାନୁଥିବା ସକେଟ ନିର୍ମାଣ କରିପାରିଲା ନାହିଁ: %s ଇଚ୍ଛାରୂପଣ ଇଚ୍ଛାମୁତାବକ ଅଧିବେଶନ ଚାଳକ ନିର୍ଦ୍ଧାରିତ ପ୍ରୟୋଗଗୁଡ଼ିକୁ ଧାରଣ କରନ୍ତୁ ନାହିଁ ଚାଳକ ପାଇଁ ନିଶ୍ଚିତକରଣ ଆବଶ୍ୟକ କରିନଥାଏ ତ୍ରୁଟି ନିବାରଣ ସଂକେତକୁ ସକ୍ରିୟ କରନ୍ତୁ %s କୁ ନିଷ୍ପାଦନ କରିବାରେ ବିଫଳ
 ନୋମ GNOME ପ୍ରତିରୂପ ୱେଲ୍ୟାଣ୍ଡ ଉପରେ GNOME ଯେକୌଣସି ସ୍ଥିତବାନ ବାଧାବିଘ୍ନକୁ ଅମାନ୍ୟ କରୁଅଛି ଲଗଆଉଟ୍ ଉତ୍ତର ଦେଉନାହିଁ ଆହା! କିଛି ଗୋଟିଏ ଭୁଲ ହୋଇଗଲା। ବିକଳ୍ପଗୁଡ଼ିକ ମାନକ ସ୍ୱୟଂଚାଳିତ ଡିରେକ୍ଟୋରୀଗୁଡ଼ିକୁ ନବଲିଖନ କରନ୍ତୁ ଚଲାଇବା ପାଇଁ ଏକ ଇଚ୍ଛାମୁତାବକ ଅଧିବେଶନ ବାଛନ୍ତୁ ବିଦ୍ୟୁତ ପ୍ରବାହ ବନ୍ଦ କରନ୍ତୁ ଦ୍ୱନ୍ଦମୟ ବିକଳ୍ପଗୁଡ଼ିକ ସହିତ ଡକାହେଉଥିବା ପ୍ରଗ୍ରାମ ପୁନର୍ଚାଳନ କରନ୍ତୁ ନୂତନ କ୍ଲାଏଣ୍ଟ ସଂଯୋଗକୁ ମନାକରୁଅଛି କାରଣ ସେହି ଅଧିବେଶନଟି ବର୍ତ୍ତମାନ ବନ୍ଦ ହୋଇଛି
 ମନେଥିବା ପ୍ରୟୋଗ ଅଧିବେଶନର ନାମ ପରିବର୍ତ୍ତନ କରନ୍ତୁ (_m) SESSION_NAME ଅଧିବେଶନ %d ଅଧିବେଶନ ନାମଗୁଡ଼ିକ ‘/’ ବର୍ଣ୍ଣ ଧାରଣ କରିବା ପାଇଁ ଅନୁମତି ନାହିଁ ଅଧିବେଶନ ନାମଗୁଡ଼ିକ ‘.’ ରୁ ଆରମ୍ଭ ହେବା ପାଇଁ ଅନୁମତି ନଥାଏ ଅଧିବେଶନ ନାମଗୁଡ଼ିକ  ‘.’ ରୁ ଆରମ୍ଭ କରିବା ପାଇଁ ଅନୁମତି ଦେଇନଥାଏ କିମ୍ବା ‘/’ ଅକ୍ଷର ଧାରଣ କରିନଥାଏ ବ୍ୟବହାର କରିବାକୁ ଥିବା ଅଧିବେଶନ ଅନୁଲଗ୍ନ ଚେତାବନି ଦର୍ଶାନ୍ତୁ ପରୀକ୍ଷା ପାଇଁ ବିଫଳ ସଂଳାପକୁ ଦର୍ଶାନ୍ତୁ ବ୍ଯବସ୍ଥାପକ ପ୍ରୋଗ୍ରାମ ଏହି ଭରଣଟି ଆପଣଙ୍କୁ ଏକ ସଂରକ୍ଷିତ ଅଧିବେଶନ ବାଛିବାକୁ ଦେଇଥାଏ ଏହି ପ୍ରଗ୍ରାମଟି ଲଗଆଉଟ ହେବାକୁ ଦେଉନାହିଁ। ଏହି ଅଧିବେଶନ ଆପଣଙ୍କୁ ନୋମ ରେ ଲଗ କରାଇଥାଏ ୱେଲ୍ୟାଣ୍ଡ ବ୍ୟବହାର କରି, ଏହି ଅଧିବେଶନ ଆପଣଙ୍କୁ GNOME ରେ ଲଗଇନ୍‌ କରାଇଥାଏ ଏହି ପ୍ରୟୋଗର ସଂସ୍କରଣ ଲଗଆଉଟ ହେବା ସମୟରେ ସ୍ୱୟଂଚାଳିତ ଭାବରେ ମନେରଖନ୍ତୁ (_A) ଅଗ୍ରସର ହୁଅନ୍ତୁ (_C) ଲଗଆଉଟ କରନ୍ତୁ (_L) ଲଗଆଉଟ (_L) ନାମ (_N): ନୂତନ ଅଧିବେଶନ (_N) ବର୍ତ୍ତମାନ ଚାଲୁଥିବା ପ୍ରୟୋଗଗୁଡ଼ିକୁ ମନେରଖନ୍ତୁ (_R) ଅଧିବେଶନକୁ ବାହାର କରନ୍ତୁ (_R) 