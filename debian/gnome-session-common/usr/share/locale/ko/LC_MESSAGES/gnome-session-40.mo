ή    D      <  a   \      ΰ     α  p        q  T     i   ί  \   I	  '   ¦	     Ξ	     ά	  (   ι	  )   
     <
     C
  #   R
  '   v
  $   
     Γ
     Ω
     ο
     υ
       q     G     >   Ι           )     1     ?  !   N  '   p  %     	   Ύ  '   Θ  #   π       P        l       %     M   Ή          "     /     =  
   O  ;   Z  3     Q   Κ          +  &   B     i  +     #   ―  P   Σ  *   $      O      p  B        Τ     σ  p     	                       ©    Ή     A  ‘  ^        e          d     3   j          ΄  .   Θ  3   χ     +     <  8   T  C     (   Ρ  #   ϊ          0     7     E     S  @   Ϋ  F     -   c               ²  &   ΐ  8   η  -         N  ?   \  =        Ϊ  W   θ     @     W  .   s  a   ’                0     >  	   P  ?   Z  E     ]   ΰ     >     O  7   d       +   Έ  $   δ  j   	  4   t  8   ©  7   β  A         \      y         
   !     !     0!     A!     P!         !                    #       *   9   6   4           0   )              8       (      '   $   %           >                     	          -          A   D   ,                                        B              ?   ;              :      
       +   .   3          /   5   1           2   =          @      "      C                7   &      <        β the GNOME session manager %s [OPTIONβ¦] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system canβt recover.
Please log out and try again. A problem has occurred and the system canβt recover. All extensions have been disabled as a precaution. A problem has occurred and the system canβt recover. Please contact a system administrator A session named β%sβ already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Donβt prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain β/β characters Session names are not allowed to start with β.β Session names are not allowed to start with β.β or contain β/β characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2020-08-29 19:20+0900
Last-Translator: Changwoo Ryu <cwryu@debian.org>
Language-Team: GNOME Korea <gnome-kr@googlegroups.com>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
  β κ·Έλ μΈμ κ΄λ¦¬μ %s [μ΅μ...] <λͺλ Ή>

μΌλΆ κ·Έλ μΈμ κΈ°λ₯μ κΈμ§νλ©΄μ <λͺλ Ή>μ μ€νν©λλ€.

  -h, --help        μ΄ λμλ§ νμ
  --version         νλ‘κ·Έλ¨ λ²μ  νμ
  --app-id <ID>     κΈμ§ν  λ μ¬μ©νλ €λ νλ‘κ·Έλ¨ μμ΄λ(μ΅μ)
  --reason <μ΄μ >   κΈμ§νλ μ΄μ (μ΅μ)
  --inhibit <ν­λͺ©>  κΈμ§ν  ν­λͺ©μ μΌνλ‘ κ΅¬λΆν λͺ©λ‘:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    <λͺλ Ή>μ μ€ννμ§ μκ³  κ³μ λκΈ°
  -l, --list        νμ¬ κΈμ§ λͺ©λ‘μ νμνκ³ , λλ©λλ€

--inhibit μ΅μμ μ§μ νμ§ μμλ€λ©΄, idleμ΄λΌκ³  κ°μ ν©λλ€.
 %s requires an argument
 λ¬Έμ κ° λ°μνμ§λ§ λ³΅κ΅¬νμ§ λͺ»νμ΅λλ€.
λ‘κ·Έμμνκ³  λ€μ μλνμ­μμ€. λ¬Έμ κ° λ°μνμ§λ§ λ³΅κ΅¬νμ§ λͺ»νμ΅λλ€. μμ μ μν΄ λͺ¨λ  νμ₯ κΈ°λ₯μ μ¬μ©νμ§ μλλ‘ λ§λ­λλ€. λ¬Έμ κ° λ°μνμ§λ§ λ³΅κ΅¬νμ§ λͺ»νμ΅λλ€. μμ€ν κ΄λ¦¬μμκ² λ¬Έμνμ­μμ€ μ΄λ¦μ΄ β%sβμΈ μΈμμ΄ μ΄λ―Έ μμ΅λλ€ <μλμμ_ν΄λ> λ‘κ·Έμμ νμ© μΈμ κ΄λ¦¬μμ μ°κ²°ν  μ μμ΅λλ€ ICE λ¦¬μ€λ μμΌμ λ§λ€ μ μμ΅λλ€: %s μ¬μ©μ μ§μ  μ¬μ©μ μ§μ  μΈμ νλμ¨μ΄ κ°μ κΈ°λ₯μ κ²μ¬ νμ§ μμ΅λλ€ μ¬μ©μκ° μ§μ ν νλ‘κ·Έλ¨μ μ½μ΄λ€μ΄μ§ μμ΅λλ€ μ¬μ©μ νμΈ μ§λ¬Έμ νμ§ μκΈ° λλ²κΉ μ½λλ₯Ό μ¬μ©ν©λλ€ %s μ€ν μ€ν¨
 κ·Έλ κ·Έλ λλ―Έ κ·Έλ (Xorg) μ°Έμ΄λ©΄, gnome-sessionμμ λμ²΄ λͺ¨λλ‘ μλ μ§μνλ κ²½μ° λ‘κ·ΈμΈν λ€μ κ²½κ³  λν μμλ₯Ό νμν©λλ€. μ°Έμ΄λ©΄, μΈμμ λλΌ λ μ¬μ©μμκ² λ¬Όμ΄λ΄λλ€. μ°Έμ΄λ©΄, gnome-sessionμμ μλμΌλ‘ μΈμμ μ μ₯ν©λλ€. κΈμ§νλ νλ‘κ·Έλ¨μ΄ μμ΄λ λ¬΄μ λ‘κ·Έμμ λ‘κ·Έμμ νμΈ μλ΅ μμ μ, λ­κ° λ¬Έμ κ° μκ²Όμ΅λλ€. κΈ°λ³Έ μλ μμ λλ ν λ¦¬ λμ  μ¬μ©ν©λλ€ μ€νν  μΈμμ μ§μ  μ§μ νμ­μμ€ μ μ λκΈ° ν¨κ» μ¬μ©ν  μ μλ μ΅μμ κ°μ΄ μ¬μ©νμ΅λλ€ νλ‘κ·Έλ¨μ μ νν νλμ μΈμκ° νμν©λλ€ λ€μ μμ μ§κΈ μΈμμ λλ΄λ μ€μ΄λ―λ‘ μ ν΄λΌμ΄μΈνΈ μ°κ²°μ κ±°λΆν©λλ€.
 κΈ°μ΅ν νλ‘κ·Έλ¨ μΈμ μ΄λ¦ λ°κΎΈκΈ°(_M) dbus.serviceκ° μ€ν μ€μ΄λ©΄ λ€μ μμ ExecStopPostμμ μ€ν, μλΉμ€ μ€ν¨ν  λ gnome-session-failed.targetμ μμν©λλ€ systemd μλΉμ€λ‘ μ€ν <μΈμ_μ΄λ¦> μΈμ μ μ₯ μ΄ μΈμ μ μ₯ μΈμ %d β/βκ° λ€μ΄κ° μΈμ μ΄λ¦μ νμ©νμ§ μμ΅λλ€ β.βμΌλ‘ μμνλ μΈμ μ΄λ¦μ νμ©νμ§ μμ΅λλ€ β.βμΌλ‘ μμνκ±°λ β/βκ° λ€μ΄κ° μΈμ μ΄λ¦μ νμ©νμ§ μμ΅λλ€ μ¬μ©ν  μΈμ νμ₯ κ²½κ³  νμ νμ€νΈ μ©λλ‘ κ³ λ λνμ°½μ νμν©λλ€ λμ²΄ λͺ¨λ κ²½κ³  νμ gnome-sessionμ μκ·Έλ μ΄κΈ°ν μλ£ gnome-session-shutdown.target μμ νμ€ μλ ₯μμ EOF λλ ν λ°μ΄νΈλ₯Ό λ°μΌλ©΄ gnome-session-shutdown.targetμ μμν©λλ€ μ΄ ν­λͺ©μΌλ‘ μ μ₯ν μΈμμ μ§μ ν©λλ€ μ΄ νλ‘κ·Έλ¨μ΄ λ‘κ·Έμμμ λ§κ³  μμ΅λλ€. μ΄ μΈμμ μ¬μ©νλ©΄ κ·Έλμ λ‘κ·ΈμΈν©λλ€ λ΄μ₯ μΈμ κ΄λ¦¬ κΈ°λ₯ μ¬μ© (systemd κΈ°λ° μΈμ μλ) systemd μΈμ κ΄λ¦¬ μ¬μ© μ΄ νλ‘κ·Έλ¨μ λ²μ  μ°Έμ΄λ©΄, gnome-sessionμμ μλ μ μ₯μ κ»λλΌλ λ‘κ·Έμμν  λ λ€μ μΈμμ μλμΌλ‘ μ μ₯ν©λλ€. κ³μ(_C) λ‘κ·Έμμ(_L) λ‘κ·Έμμ(_L) μ μΈμ(_N) μΈμ μ κ±°(_R) 