��    D      <  a   \      �     �  p        q  T   �  i   �  \   I	  '   �	     �	     �	  (   �	  )   
     <
     C
  #   R
  '   v
  $   �
     �
     �
     �
     �
       q     G   �  >   �           )     1     ?  !   N  '   p  %   �  	   �  '   �  #   �       P        l     �  %   �  M   �          "     /     =  
   O  ;   Z  3   �  Q   �          +  &   B     i  +   �  #   �  P   �  *   $      O      p  B   �     �     �  p     	   �     �     �     �     �  �  �  %   �  �  �     �  i   �  k   \  b   �  '   +     S     g  =   �  0   �     �       /     3   H  &   |      �     �     �     �     �  �      Z   �  <   �  #        B     T     r  %     4   �  1   �       3     &   I     p  N   |     �     �  &   �  _        ~     �     �     �     �  D   �  <   )  a   f     �  (   �  ,     '   3  :   [  %   �  Y   �  ?      .   V   -   �   U   �   ,   	!     6!  �   S!  
   �!     �!     "     !"     0"         !                    #       *   9   6   4           0   )              8       (      '   $   %           >                     	          -          A   D   ,                                        B              ?   ;              :      
       +   .   3          /   5   1           2   =          @      "      C                7   &      <        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session-2.0
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2020-08-04 14:08+0300
Last-Translator: Florentina Mușat <florentina.musat.28@gmail.com>
Language-Team: Gnome Romanian Translation Team
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2);;
X-Generator: Poedit 2.4
X-Project-Style: gnome
  — Administratorul de sesiune GNOME %s [OPȚIUNE…] COMANDĂ

Execută COMANDA în timp ce se inhibă unele funcționalități ale sesiunii.

  -h, --help        Arată acest ajutor
  --version         Arată versiunea programului
  --app-id ID       ID-ul aplicației de utilizat
                    când se inhibă (opțional)
  --reason MOTIV   Motivul pentru inhibare (opțional)
  --inhibit ARG     Lucruri de inhibat, listă separată prin două puncte de:
                    deautentificare, comutare utilizator, suspendă, în așteptare, automontare
  --inhibit-only    Nu lansa COMANDA și așteaptă pentru totdeauna în schimb
  -l, --list        Listează inhibițiile existente, și ieși

Dacă nicio opțiune --inhibit option nu este specificată, în așteptare este presupusă.
 %s necesită un argument
 A intervenit o problemă și sistemul nu se poate recupera.
Ieșiți din sesiune și încercați din nou. A intervenit o problemă și sistemul nu se poate recupera. Toate extensiile au fost dezactivate preventiv. A intervenit o problemă și sistemul nu se poate recupera. Contactați un administrator de sistem O sesiune numită „%s” există deja DIRECTOR_DE_PORNIRE Permite ieșierea din sesiune Nu s-a putut realiza conectarea la administratorul de sesiuni Nu s-a putut creea socketul de ascultare ICE: %s Personalizat Sesiune personalizată Dezactivează verificarea accelerării hardware Nu încărca aplicațiile specificate de utilizator Nu solicita confirmarea utilizatorului Activează codul pentru depanare Eroare la executarea %s
 GNOME Machetă GNOME GNOME pe Xorg Dacă este activat, gnome-session va afișa un dialog de avertizare după autentificare dacă sesiunea de rezervă a fost inițiată. Dacă este activat, gnome-session va anunța utilizatorul înainte de terminarea sesiunii. Dacă este activat, gnome-session va salva sesiunea automat. Se ignoră orice inhibator existent Închide sesiunea Anunță ieșirea din sesiune Nu răspunde Oh, nu! Ceva nu a funcționat corect. Înlocuiește dosarele standard de pornire automată Selectați o sesiune personalizată pentru rulare Oprește Program apelat cu opțiuni care intră în conflict Programul necesită exact un parametru Repornește Se refuză conexiunea noului client deoarece sesiunea este în curs de oprire
 Aplicație memorată Redenu_mește sesiunea Reporneșe dbus.service dacă rulează Rulează de la ExecStopPost pentru a începe gnome-session-failed.target la eșecul serviciului Rulează ca serviciu systemd NUMELE_SESIUNII Salvează sesiuni Salvează această sesiune Sesiunea %d Nu este permis ca numele sesiunilor să conțină caracterul „/” Nu este permis ca numele sesiunilor să înceapă cu „.” Nu este permis ca numele sesiunilor să înceapă cu „.” sau să conțină caracterul „/” Sesiunea de utilizat Afișează avertizările pentru extensii Afișează dialogul de eroare pentru testare Arată avertizarea sesiunii de rezervă Inițializarea semnalului a fost făcută la gnome-session Începe gnome-session-shutdown.target Începe gnome-session-shutdown.target când se primește EOF sau un singur octet la stdin Această opțiune vă permite să selectați o sesiune salvată Acest program blochează ieșirea din sesiune. Această sesiune vă va autentifica în GNOME Utilizează administrarea de sesiune încorporată (față de cea bazată pe systemd) Utilizează administrarea de sesiune systemd Versiunea acestei aplicații Când este activată, gnome-session va salva sesiunea automat sesiunea următoare la deautentificare chiar și dacă salvarea automată este dezactivată. _Continuă _Ieșire din sesiune În_chide sesiunea Sesiune _nouă _Elimină sesiunea 