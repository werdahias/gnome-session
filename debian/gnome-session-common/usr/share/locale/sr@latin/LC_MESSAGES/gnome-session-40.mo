��    3      �  G   L      h     i  4  �     �  T   �  i   +  \   �  '   �          (  (   5  )   ^     �     �  #   �  '   �  $   �     	     %	     ;	     A	     M	      [	     |	     �	  !   �	  '   �	  %   �	  	   
  '   
     5
  P   <
     �
     �
     �
  
   �
  ;   �
  3     Q   <     �     �  &   �  *   �            '     H  	   d     n     w     �     �  5  �     �  �  �     �  U   �  m   H  Y   �  (        9     U  +   n  5   �     �     �  (   �  +        F  '   e  !   �     �     �     �     �  	   �       !     2   2  +   e  	   �  .   �     �  @   �          -     ?     L  .   \  -   �  J   �            %  ,   F  5   s  !   �  !   �     �       
     
        "     /                                    	   1      $   2                         *              0   -            "   %   &      3           !   /             #                   
          +                                         .   '   (             )   ,     — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg Ignoring any existing inhibitors Log out Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Version of this application _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=gnome-session&keywords=I18N+L10N&component=general
PO-Revision-Date: 2017-08-14 21:16+0200
Last-Translator: Miroslav Nikolić <miroslavnikolic@rocketmail.com>
Language-Team: srpski <gnome-sr@googlegroups.org>
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Project-Style: gnome
  — upravnik Gnomovom sesijom %s [OPCIJA...] NAREDBA

Izvršite NAREDBU uz sprečavanje nekih funkcionalnosti sesije.

  -h, --help        Prikazuje ovu pomoć
  --version         Prikazuje izdanje programa
  --app-id IB       IB programa za korišćenje prilikom
                    sprečavanja (izborno)
  --reason RAZLOG   Razlog sprečavanja (izborno)
  --inhibit ARG     Spisak stvari za sprečavanje, razdvojenih dvotačkama:
                    logout (odjavljivanje), switch-user (promena korisnika),
                    suspend (obustava), idle (mirovanje), automount (samoprikačinjanje)
  --inhibit-only    Ne pokreće NAREDBU i umesto toga čeka u nedogled

Ako nijedna opcija sprečavanja (--inhibit) nije navedena, podrazumeva se stanje mirovanja (idle).
 „%s“ zahteva argument
 Došlo je do problema i sistem ne može da se oporavi.
Odjavite se i pokušajte opet. Došlo je do problema i sistem ne može da se oporavi. Sva proširenja su isključena zarad predostrožnosti. Došlo je do problema i sistem ne može da se oporavi. Obratite se administratoru sistema Već postoji sesija pod nazivom „%s“ DIREKTORIJUM_SAMOPOKRETANJA Dozvoljava odjavljivanje Ne mogu da se povežem sa upravnikom sesije Ne mogu da napravim priključak ICE osluškivanja: %s Prilagođeno Prilagođena sesija Isključuje proveru hardverskog ubrzanja Ne učitava programe koje je zadao korisnik Ne traži potvrdu od korisnika Uključuje kod za ispravljanje grešaka Nisam uspeo da izvršim „%s“
 Gnom Gnomov lažnjak Gnom na Iks serveru Zanemarujem postojeće ometače Odjavi me Ne daje odziv O, ne!  Nešto je pošlo naopako! Zaobilazi uobičajene direktorijume samopokretanja Izaberite prilagođenu sesiju za pokretanje Isključi Program je pozvan sa nesaglasnim mogućnostima Ponovo pokreni Odbacujem vezu sa novim klijentom jer je u toku gašenje sesije
 Zapamćeni program Preimenuj _sesiju NAZIV_SESIJE Sesija „%d“ Nazivi sesija ne mogu da sadrže kosu crtu (/) Nazivi sesija ne mogu da počinju tačkom (.) Nazivi sesija ne mogu da počinju tačkom (.) ili da sadrže kosu crtu (/) Sesije koje će biti korišćene Prikazuje upozorenje proširenja Prikazuje prozorče neuspeha za isprobavanje Ova stavka vam dopušta da izaberete sačuvanu sesiju Ovaj program onemogućava odjavu. Ova sesija vas prijavljuje u Gnom Izdanje ovog programa _Nastavi _Odjavi me _Odjavi me _Nova sesija _Ukloni sesiju 