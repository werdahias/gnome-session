��    D      <  a   \      �     �  p        q  T   �  i   �  \   I	  '   �	     �	     �	  (   �	  )   
     <
     C
  #   R
  '   v
  $   �
     �
     �
     �
     �
       q     G   �  >   �           )     1     ?  !   N  '   p  %   �  	   �  '   �  #   �       P        l     �  %   �  M   �          "     /     =  
   O  ;   Z  3   �  Q   �          +  &   B     i  +   �  #   �  P   �  *   $      O      p  B   �     �     �  p     	   �     �     �     �     �  �  �  $   t  2  �     �  X   �     ?  f   �  )   &     P     k  -   y  /   �     �     �  7   �  7   5  &   m     �     �     �     �     �  {   �  W   q  C   �  -        ;     @     U     h  3     .   �     �  H   �  .   1     `  L   i     �     �  +   �  i     &   ~     �     �     �  
   �  8   �  =     Q   ]     �     �  -   �       :   5  $   p  L   �  8   �  )      !   E   A   g   '   �      �   W   �   	   F!     P!     V!     \!     j!         !                    #       *   9   6   4           0   )              8       (      '   $   %           >                     	          -          A   D   ,                                        B              ?   ;              :      
       +   .   3          /   5   1           2   =          @      "      C                7   &      <        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session 2.3.7
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2019-08-15 23:30+0200
Last-Translator: Robert Antoni Buj Gelonch <rbuj@fedoraproject.org>
Language-Team: Catalan <tradgnome@softcatala.org>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 2.2.1
  — el gestor de sessions del GNOME %s [OPCIÓ...] ORDRE

Executeu l'ORDRE mentre s'inhibeix alguna funcionalitat de la sessió.

  -h, --help        Mostra aquesta ajuda
  --version         Mostra la versió del programa
  --app-id ID       L'id d'aplicació que s'utilitzarà quan
                    s'inhibeixi (opcional)
  --reason RAÓ      La raó per la qual s'inhibeix (opcional)
  --inhibit ARG     Coses a inhibir, llista separada per dos punts:
                    «logout» (sortida), «switch-user» (canvi d'usuari),
                    «suspend» (suspendre), «idle» (inactiu) i
                    «automount» (muntatge automàtic)
  --inhibit-only    No executis l'ORDRE i espera per sempre
  -l, --list        Llista les inhibicions existents, i surt

Si no s'especifica cap opció a --inhibit, s'utilitzarà «idle» (inactiu).
 %s requereix un argument
 S'ha produït un problema i el sistema no es pot recuperar.
Sortiu i torneu-ho a provar. S'ha produït un problema i el sistema no es pot recuperar. Com a mètode de precaució s'han inhabilitat totes les extensions. S'ha produït un problema i el sistema no es pot recuperar. Contacteu amb un administrador del sistema Ja existeix una sessió amb el nom «%s» DIRECTORI_INICI_AUTOMÀTIC Permet sortir No s'ha pogut connectar al gestor de sessions No s'ha pogut crear el sòcol ICE d'escolta: %s Personalitzada Sessió personalitzada Inhabilita la comprovació d'acceleració del maquinari No carreguis les aplicacions especificades per l'usuari No demanis la confirmació de l'usuari Habilita el codi de depuració No s'ha pogut executar %s
 GNOME GNOME de prova GNOME damunt Xorg Si s'habilita, el gnome-session mostrarà un diàleg d'avís després d'iniciar la sessió si aquesta és en mode clàssic. Si s'habilita, el gnome-session preguntarà a l'usuari abans de finalitzar una sessió. Si s'habilita, el gnome-session desarà la sessió automàticament. S'està ignorant qualsevol inhibidor existent Surt Indicador de sortida No està responent Alguna cosa ha fallat. Ignora els directoris estàndard d'inici automàtic Seleccioneu quina sessió personalitzada voleu Atura El programa s'ha invocat amb opcions que entren en conflicte entre elles El programa necessita exactament un paràmetre Reinicia Es refusarà la connexió de client nova perquè la sessió s'està aturant
 Aplicació recordada _Canvia el nom de la sessió Reinicia dbus.service si està executant-se Executa des de ExecStopPost per a iniciar gnome-session-failed.target quan hi hagi una fallada del servei S'està executant systemd com a servei NOM_DE_SESSIÓ Desa les sessions Desa aquesta sessió Sessió %d Els noms de sessió no poden contenir el caràcter «/» Els noms de sessió no poden començar amb el caràcter «.» Els noms de sessió no poden començar amb els caràcters «.» ni contenir «/» Sessió que s'utilitzarà Mostra l'avís de l'extensió Mostra el diàleg de la balena per fer proves Mostra l'avís de mode clàssic S'ha completat el senyal d'inicialització a gnome-session Inicia gnome-session-shutdown.target Inicia gnome-session-shutdown.target quan es rep EOF o un únic byte a stdin Aquesta entrada us permet seleccionar una sessió desada Aquest programa està blocant la sortida. Aquesta sessió us entra al GNOME Usa la gestió de sessió integrada (en comptes de la de systemd) Usa la gestió de la sessió de systemd Versió d'aquesta aplicació Si s'habilita, el gnome-session desarà automàticament la propera sessió al registre. _Continua _Surt _Surt Sessió _nova _Suprimeix la sessió 