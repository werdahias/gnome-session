��    <      �  S   �      (     )  4  F     {  R   �  g   �  Z   O  '   �     �     �     �  	   	  	   	  	   	  (   )	  )   R	     |	     �	  '   �	  "   �	     �	     �	     	
     
     
      ,
     M
     U
  !   d
     �
  '   �
  %   �
  	   �
  '   �
       P        f     }     �  
   �  ;   �  3   �  Q        g     v  &   �     �  *   �      �        /   2     b  =   ~  	   �     �     �     �     �  (   �       �  %  <   �  L  .  7   {  �   �  (  �    �  p   �     S  V   a  2   �     �          $  �   @  j   �  $   9  :   ^  �   �  y   *  C   �  E   �     .     4     G  o   Y     �  8   �  ;        X     w  �   �     |  {   �  "     E  2  /   x   D   �      �      �   �   !  �   �!  �   +"  C   #  \   K#  t   �#  7   $  �   U$  Y   %  V   a%     �%  ;   8&  �   t&  '   '     C'     b'     �'  '   �'  p   �'  -   *(     $             3       0                  4                    -       	           7          1   )           #   *      (                 "                &   +      /         5   6            9   ;         <       2           '   !           ,             :   .   %       
                   8        - the GNOME session manager %s [OPTION...] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can't recover.
Please log out and try again. A problem has occurred and the system can't recover. All extensions have been disabled as a precaution. A problem has occurred and the system can't recover. Please contact a system administrator A session named ‘%s’ already exists AUTOSTART_DIR Additional startup _programs: Allow logout Browse… Co_mmand: Comm_ent: Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Do not load user-specified applications Don't prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Wayland Ignoring any existing inhibitors Log out Not responding Oh no!  Something has gone wrong. Options Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Session %d Session names are not allowed to contain ‘/’ characters Session names are not allowed to start with ‘.’ Session names are not allowed to start with ‘.’ or contain ‘/’ characters Session to use Show extension warning Show the fail whale dialog for testing Startup Programs This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME This session logs you into GNOME, using Wayland Version of this application _Automatically remember running applications when logging out _Continue _Log Out _Log out _Name: _New Session _Remember Currently Running Applications _Remove Session Project-Id-Version: as
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-session&keywords=I18N+L10N&component=general
PO-Revision-Date: 2014-01-06 17:30+0630
Last-Translator: Nilamdyuti Goswami <ngoswami@redhat.com>
Language-Team: Assamese <kde-i18n-doc@kde.org>
Language: as
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 1.5
 - GNOME অধিবেশন ব্যৱস্থাপক %s [OPTION...] COMMAND

অধিবেশনৰ কাৰ্য্যকৰীতাক বাধা দিওতে COMMAND এক্সিকিউট কৰক। 

  -h, --help        এই সহায় দেখুৱাওক
  --version         প্ৰগ্ৰামৰ সংস্কৰণ দেখুৱাওক
  --app-id ID       বাধা দিওতে ব্যৱহাৰ কৰিন লগিয়া
                    এপ্লিকেচন আইডি (বৈকল্পিক)
  --reason REASON   বাধা দিয়াৰ কাৰণ (বৈকল্পিক)
  --inhibit ARG     বাধা দিব লগিয়া বস্তু, কলন-পৃথকিত তালিকা:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    ইয়াৰ পৰিৱৰ্তে COMMAND লঞ্চ কৰি সদায়ৰ বাবে অপেক্ষা নকৰিব

যদি কোনো --inhibit বিকল্প ধাৰ্য্য কৰা হোৱা নাই, idle ধৰি লোৱা হয়।
 %s ৰ এটা তৰ্কৰ প্ৰয়োজন
 এটা সমস্যা দেখা দিছে আৰু চিস্টেমটো উদ্ধাৰ হব পৰা নাই।
অনুগ্ৰহ কৰি লগআউট কৰক আৰু পুনৰ চেষ্টা কৰক। এটা সমস্যা দেখা দিছে আৰু চিস্টেমটো উদ্ধাৰ হব পৰা নাই। সকলো সম্প্ৰসাৰন এটা সাৱধানতা হিচাপে অসামৰ্থবান কৰা হৈছে। এটা সমস্যা দেখা দিছে আৰু চিস্টেমটো উদ্ধাৰ হব পৰা নাই। অনুগ্ৰহ কৰি এজন চিস্টেম প্ৰশাসকক যোগাযোগ কৰক ‘%s’ নামৰ এটা অধিবেশন ইতিমধ্যে অস্তিত্ববান AUTOSTART_DIR অতিৰিক্ত আৰম্ভৰ প্ৰগ্ৰামসমূহ (_p): লগআউটৰ অনুমতি দিয়ক ব্ৰাউছ কৰক... কমান্ড (_m): মন্তব্য (_e): অধিবেশন ব্যৱস্থাপকৰ সৈতে সংযোগ স্থাপন কৰিবলৈ ব্যৰ্থ ICE অপেক্ষাৰ ছ'কেট নিৰ্মাণ কৰিবলৈ ব্যৰ্থ: %s স্বনিৰ্বাচিত স্বনিৰ্বাচিত অধিবেশন ব্যৱহাৰকাৰী দ্বাৰা নিৰ্ধাৰিত এপ্লিকেচনসমূহ ল'ড নকৰিব ব্যৱহাৰকাৰী নিশ্চিতকৰণৰ কাৰণে প্ৰমপ্ট নকৰিব ক'ড ডিবাগিং সামৰ্থবান কৰক %s এক্সিকিউট কৰিবলে ব্যৰ্থ
 GNOME GNOME ডামি Wayland ত GNOME কোনো উপস্থিত প্ৰতিৰোধ অগ্ৰাহ্য কৰা হৈ আছে লগআউট কৰক কোনো প্ৰতিক্ৰিয়া নাই ইচচ! কিবা এটা ভুল হৈছে। বিকল্পসমূহ প্ৰামাণিক স্বআৰম্ভ ডাইৰেকটৰিসমূহ অভাৰৰাইড কৰক অনুগ্ৰহ কৰি চলাবলে এটা স্বনিৰ্বাচিত অধিবেশন বাছক বন্ধ কৰক দ্বন্দ্বযুক্ত বিকল্প সহ প্ৰগ্ৰামক কল কৰা হৈছে পুনৰাম্ভ কৰক চিস্টেম বৰ্তমানে বন্ধ কৰাৰ প্ৰচেষ্টা কৰা হৈছে আৰু এই কাৰণে নতুন গ্ৰাহকৰ সৈতে সংযোগ স্থাপনৰ অনুৰোধ প্ৰত্যাখ্যান কৰা হৈছে।
 মনত ৰখা এপ্লিকেচন অধিবেশন পুনৰ নামকৰণ কৰক (_m) SESSION_NAME অধিবেশন %d অধিবেশন নামসমূহ ‘/’ আখৰসমূহ অন্তৰ্ভুক্ত কৰাৰ অনুমতি নাই অধিবেশন নামসমূহ ‘.’ ৰ সৈতে আৰম্ভ হোৱাৰ অনুমতি নাই অধিবেশন নামসমূহ ‘.’ ৰ সৈতে আৰম্ভ হোৱাৰ অথবা ‘/’ আখৰসমূহ অন্তৰ্ভুক্ত কৰাৰ অনুমতি নাই ব্যৱহাৰ হ'ব লগিয়া অধিবেশন সম্প্ৰসাৰনৰ সতৰ্কবাৰ্তা দেখুৱাওক পৰিক্ষণৰ বাবে ব্যৰ্থ হোৱেইল ডাইলগ দেখুৱাওক আৰম্ভৰ প্ৰগ্ৰামসমূহ এই প্ৰৱিষ্টিয়ে আপোনাক এটা সংৰক্ষিত অধিবেশন নিৰ্বাচন কৰাত সহায় কৰে এই প্ৰগ্ৰামে লগআউট অবৰোধ কৰি আছে। এই অধিবেশনে আপোনাক GNOME ত লগিন কৰায় এই অধিবেশনে আপোনাক Wayland ব্যৱহাৰ কৰি, GNOME ত লগিন কৰায় এই এপ্লিকেচনৰ সংস্কৰণ লগআউট কৰাৰ সময়ত চলি থকা এপ্লিকেচনসমূহ স্বচালিতভাৱে মনত ৰাখক (_A) অব্যাহত ৰাখক (_C) লগআউট কৰক (_L) লগআউট কৰক (_L) নাম (_N): নতুন অধিবেশন (_N) বৰ্তমানে চলি থকা এপ্লিকেচনসমূহ মনত ৰাখক (_R) অধিবেশন আতৰাওক (_R) 