��    3      �  G   L      h     i  4  �     �  T   �  i   +  \   �  '   �          (  (   5  )   ^     �     �  #   �  '   �  $   �     	     %	     ;	     A	     M	      [	     |	     �	  !   �	  '   �	  %   �	  	   
  '   
     5
  P   <
     �
     �
     �
  
   �
  ;   �
  3     Q   <     �     �  &   �  *   �            '     H  	   d     n     w     �     �  �  �  $   g  �  �     ~  W   �     �  f   p  )   �            -   )  /   W     �     �  7   �  7   �  &        D     c     ~     �     �  -   �     �     �     �  3   �  .   0     _  H   e     �  L   �               6  
   E  9   P  =   �  R   �          5  -   S  8   �  %   �     �        	        $     (     ,     :                                    	   1      $   2                         *              0   -            "   %   &      3           !   /             #                   
          +                                         .   '   (             )   ,     — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg Ignoring any existing inhibitors Log out Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Version of this application _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session 2.3.7
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=gnome-session&keywords=I18N+L10N&component=general
PO-Revision-Date: 2015-03-07 00:41+0200
Last-Translator: Xavi Ivars <xavi.ivars@gmail.com>
Language-Team: Catalan <tradgnome@softcatala.org>
Language: ca-valencia
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bits
Plural-Forms: nplurals=2; plural=n != 1;
  — el gestor de sessions del GNOME %s [OPCIÓ...] ORDRE

Executeu l'ORDRE mentre s'inhibeix alguna funcionalitat de la sessió.

  -h, --help        Mostra esta ajuda
  --version         Mostra la versió del programa
  --app-id ID       L'id d'aplicació que s'utilitzarà quan
                    s'inhibisca (opcional)
  --reason RAÓ      La raó per la qual s'inhibeix (opcional)
  --inhibit ARG     Coses a inhibir, llista separada per dos punts:
                    «logout» (eixida), «switch-user» (canvi d'usuari),
                    «suspend» (suspendre), «idle» (inactiu) i
                    «automount» (muntatge automàtic)
  --inhibit-only    No executes l'ORDRE i espera per sempre

Si no s'especifica cap opció a --inhibit, s'utilitzarà «idle» (inactiu).
 %s requereix un argument
 S'ha produït un problema i el sistema no es pot recuperar.
Eixiu i torneu-ho a provar. S'ha produït un problema i el sistema no es pot recuperar. Com a mètode de precaució s'han inhabilitat totes les extensions. S'ha produït un problema i el sistema no es pot recuperar. Contacteu amb un administrador del sistema Ja existeix una sessió amb el nom «%s» DIRECTORI_INICI_AUTOMÀTIC Permet eixir No s'ha pogut connectar al gestor de sessions No s'ha pogut crear el sòcol ICE d'escolta: %s Personalitzada Sessió personalitzada Inhabilita la comprovació d'acceleració del maquinari No carreguis les aplicacions especificades per l'usuari No demanes la confirmació de l'usuari Habilita el codi de depuració No s'ha pogut executar %s
 GNOME GNOME de prova GNOME en Xorg S'està ignorant qualsevol inhibidor existent Ix No està responent Alguna cosa ha fallat. Ignora els directoris estàndard d'inici automàtic Seleccioneu quina sessió personalitzada voleu Atura El programa s'ha invocat amb opcions que entren en conflicte entre elles Reinicia Es refusarà la connexió de client nova perquè la sessió s'està aturant
 Aplicació recordada _Canvia el nom de la sessió NOM_DE_SESSIÓ Sessió %d Els noms de sessió no poden contindre el caràcter «/» Els noms de sessió no poden començar amb el caràcter «.» Els noms de sessió no poden començar amb els caràcters «.» ni contindre «/» Sessió que s'utilitzarà Mostra l'avís de l'extensió Mostra el diàleg de la balena per fer proves Esta entrada vos permet seleccionar una sessió guardada Este programa està blocant l'eixida. Esta sessió vos entra al GNOME Versió d'esta aplicació _Continua I_x I_x Sessió _nova _Suprimeix la sessió 