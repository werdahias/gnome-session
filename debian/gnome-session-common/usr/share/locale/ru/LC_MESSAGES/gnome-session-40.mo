��    C      4  Y   L      �     �  4  �       T     i   s  \   �  '   :	     b	     p	  (   }	  )   �	     �	     �	  #   �	  '   

  $   2
     W
     m
     �
     �
     �
  q   �
  G     >   ]      �     �     �     �  !   �  '     %   ,  	   R  '   \  #   �     �  P   �             %   '     M     h     u     �  
   �  ;   �  3   �  Q        b     q  &   �     �  +   �  #   �  P     *   j      �      �  B   �          9  p   U  	   �     �     �     �     �  �  �  *   �  �    '   �  �   �  �   �  �   j  =     %   W  4   }  Q   �  I        N     f  \   ~  Z   �  P   6  ,   �  *   �     �  #   �     	  �     �     `   �  U        f  I   �     �  /   �  Q     1   f     �  X   �  D         I   y   d   +   �   &   
!  G   1!  7   y!     �!     �!  &   �!     "  Q   ""  V   t"  �   �"  #   Q#  K   u#  ]   �#  @   $  Q   `$  0   �$  x   �$  ^   \%  O   �%  A   &  �   M&  @   �&  ,   '  �   @'     6(     L(     k(     �(     �(     +   7      >   C                0   %          5   6   #       8      =   4              *          2       <   ?                               1                 -   A   :          '             9   (              3       	              /          !   )   $         ,   .   ;   
         &           @            B   "        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: ru
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2019-11-11 22:01+0300
Last-Translator: Stas Solovey <whats_up@tut.by>
Language-Team: Russian <gnome-cyr@gnome.org>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 2.2.4
  — менеджер сеансов GNOME %s [ПАРАМЕТР…] КОМАНДА

Выполнить КОМАНДУ с ограничением некоторой функциональности сеанса.

  -h, --help        Показать эту справку
  --version         Показать версию программы
  --app-id ID       Идентификатор используемой программы
                     при ограничении (необязательно)
  --reason ПРИЧИНА   Причина для ограничения (необязательно)
  --inhibit АРГУМЕНТ Список отключаемых функций, разделённых запятыми:
                     logout, switch-user, suspend, idle, automount
  --inhibit-only     Не запускать КОМАНДУ и включить вечное ожидание

При использовании параметра --inhibit включается ожидание.
 %s требуется аргумент
 Произошла ошибка, и системе не удалось восстановиться.
Нужно завершить сеанс и войти снова. Произошла ошибка, и системе не удалось восстановиться. Все расширения были отключены в целях безопасности. Произошла ошибка, и системе не удалось восстановиться. Обратитесь к системному администратору Сеанс с именем «%s» уже существует КАТАЛОГ_АВТОЗАПУСКА Разрешить завершение сеанса Не удалось соединиться с менеджером сеансов Не удалось создать сокет, слушающий ICE: %s Другой сеанс Другой сеанс Отключить проверку наличия аппаратного ускорения Не загружать указанные пользователем приложения Не требовать подтверждения от пользователя Включить отладочный код Не удалось запустить %s
 GNOME Фиктивный сеанс GNOME GNOME на Xorg Если включено, gnome-session отобразит диалоговое окно с предупреждением после входа в систему, если сеанс был автоматически аварийно завершён. Если включено, gnome-session будет предупреждать пользователя перед завершением сеанса. Если включено, gnome-session автоматически сохранит сеанс. Игнорирование любых существующих препятствий Завершить сеанс Предупреждать перед завершением сеанса Не отвечает О, нет! Что-то пошло не так. Заместить стандартные каталоги автозапуска Выберите сеанс для запуска Выключить Программа вызвана с конфликтующими параметрами Программе нужен только один параметр Перезагрузить Новые клиентские подключения отклоняются, т. к. сеанс завершается
 Запомненное приложение П_ереименовать сеанс Перезапустить dbus.service, если он работает Запущен в качестве службы systemd НАЗВАНИЕ_СЕАНСА Сохранить сеанс Сохранить этот сеанс Сеанс %d Имена сеансов не могут содержать символы «/» Имена сеансов не могут начинаться с символа «.» Имена сеансов не могут начинаться с символа «.» или содержать символы «/» Использовать сеанс Показывать предупреждения от расширений Показать для отладки диалог с сообщением об ошибке Показать аварийное предупреждение Инициализация сигнала выполнена для gnome-session Запустить gnome-session-shutdown.target Запустить gnome-session-shutdown.target при получении EOF или одного байта на stdin Эта запись позволяет вам выбрать сохранённый сеанс Эта программа блокирует завершение сеанса. Этот сеанс позволяет вам войти в GNOME Используйте встроенное управление сессиями (вместо основанного на systemd) Использовать менеджер сеансов systemd Версия этого приложения Когда включено, gnome-session автоматически сохранит следующий сеанс при выходе из системы, даже если автоматическое сохранение отключено. _Продолжить _Завершить сеанс _Завершить сеанс _Создать сеанс _Удалить сеанс 