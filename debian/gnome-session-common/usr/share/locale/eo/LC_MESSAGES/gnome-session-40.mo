��    ;      �  O   �           	  4  (     ]  T   v  i   �  \   5  '   �     �     �  (   �  )   �     (	     /	  #   >	  '   b	  $   �	     �	     �	     �	     �	     �	  q   �	  G   m
  >   �
      �
               +  !   :  '   \  %   �  	   �  '   �     �  P   �     4     K     [     h     v  
   �  ;   �  3   �  Q        U     d  &   {     �  *   �      �           )  p   E  	   �     �     �     �     �  �  �  "   �  X  �     C  T   Z  o   �  V     #   v     �     �  )   �  4   �            ,   -  (   Z  %   �     �     �     �     �     �  m   �  F   g  :   �  ,   �               .     ;  -   S  *   �  	   �  )   �  
   �  A   �     -     D     U     b     t  	   �  0   �  -   �  H   �     @     N  #   k     �  3   �  #   �  #        &  v   B  	   �     �     �     �     �         9      *         %      :           4   8      ;               5            7   !   '      &              (   	          ,      #              -       .      3                0      $      +       )       2       1                        /   6               
              "        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session 2.3.6.2
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2018-10-16 23:10+0200
Last-Translator: Kristjan SCHMIDT <kristjan.schmidt@googlemail.com>
Language-Team: Esperanto <gnome-eo-list@gnome.org>
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Virtaal 0.7.1
X-Project-Style: gnome
  — la seancadministrilo de GNOME %s [OPCIO...] KOMANDO

Plenumi KOMANDO dum inhibiting some session functionality.

  -h, --help        Montri ĉi tiun helpon
  --version         Montri la version de la programo
  --app-id ID       La uzenda aplikaĵa ID
                    kiam malebligante (malnepra)
  --reason KIALO    La kialo por malebligi (malnepra)
  --inhibit ARG     Malebligendaj aferoj, dupunkto-apartigita listo de:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Ne lanĉi COMMAND kaj anstataŭe atendi por ĉiam

Se neniu --inhibit opcio estas specifita, oni antaŭsupozas idle.
 %s bezonas argumenton
 Problemo okazis kaj la sistemo ne eblas restaŭri.
Bonvolu adiaŭi kaj provi denove. Problemo okazis kaj la sistemo ne eblis retrovi. Ĉiuj kromprogramoj estis malŝaltitaj por garantii sekurecon. Problemo okazis kaj la sistemo ne eblis retrovi. Bonvolu kontakti sistemadministranton Seanco nomita “%s” jam ekzistas AUTOSTART_DIR Permesi adiaŭon Ne eblis konekti al la seancoadministrilo Ne eblis krei ICE-aŭskultantan kontaktoskatolon: %s Propra Propra seanco Malebligi kontrolon de aparatara plirapidigo Ne ŝargi propre specifitajn aplikaĵojn Ne informpeti la uzanton por konfirmo Ŝalti sencimigan kodon Malsukcesis ruli %s
 GNOME GNOME lokokupo GNOME je Xorg Se ŝaltita, gnome-session montros avertan dialogon antaŭ ol salutado se la seanco aŭtomate retropaŝiĝis. Se ŝaltita, gnome-session demandos la uzanton antaŭ ol fini seancon. Se ŝaltita, gnome-session konservos la seancon aŭtomate. Ignori ĉiujn ajn ekzistantajn malebligilojn Adiaŭi Adiaŭ-invitilo Ne respondas Ho ne!  Io malsukcesis. Transiri normajn aŭtomatajn startdosierujojn Bonvolu elekti proporan seancon por lanĉi Malŝalti Programo vokata kun konfliktaj parametroj Restartigi Rifuzas novan klientkonekton ĉar la seanco estas nun elsalutata
 Memorigitaj aplikaĵoj Alino_mi seancon SESSION_NAME Konservi seancojn Konservi ĉi tiun seancon Seanco %d Seancaj nomoj ne permesas enhavi “/” signojn Seancaj nomoj ne permesas komenci kun “.” Seancaj nomoj ne permesas komenci kun “.” aŭ enhavi “/” signojn Uzenda seanco Montri kromproagaman averton Montri la paneo-dialogo por testado Montri retropaŝan averton Ĉi kampo permesas al vi elekti konservitan seancon Ĉi tiu programaro blokas adiaŭon. Ĉi tiu seanco salutas vin en GNOME Versio de ĉi tiu aplikaĵo Se ŝaltita, gnome-session konservos la seancon aŭtomate kiam vi adiaŭas, eĉ se aŭto-konservado estas malŝaltita. _Daŭrigi A_diaŭi A_diaŭi _Nova seanco _Forigi seancon 