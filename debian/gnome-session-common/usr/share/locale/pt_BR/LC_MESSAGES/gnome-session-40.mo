��    D      <  a   \      �     �  p        q  T   �  i   �  \   I	  '   �	     �	     �	  (   �	  )   
     <
     C
  #   R
  '   v
  $   �
     �
     �
     �
     �
       q     G   �  >   �           )     1     ?  !   N  '   p  %   �  	   �  '   �  #   �       P        l     �  %   �  M   �          "     /     =  
   O  ;   Z  3   �  Q   �          +  &   B     i  +   �  #   �  P   �  *   $      O      p  B   �     �     �  p     	   �     �     �     �     �  �  �  %   �  �  �     �  j   �  q     w   v  (   �           %  6   F  4   }     �     �  /   �  7     $   =      b     �     �     �     �  �   �  W   V  D   �  &   �          +     @  #   Q  8   u  A   �     �  *   �  *   $  	   O  Z   Y     �     �  0   �  U     #   e     �     �     �  
   �  D   �  >   
  ]   I     �     �  5   �       1   '  &   Y  S   �  9   �  9         H   H   e   *   �      �   �   �   
   �!     �!     �!     �!     �!         !                    #       *   9   6   4           0   )              8       (      '   $   %           >                     	          -          A   D   ,                                        B              ?   ;              :      
       +   .   3          /   5   1           2   =          @      "      C                7   &      <        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2020-08-03 11:04-0300
Last-Translator: Rafael Fontenelle <rafaelff@gnome.org>
Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1)
X-Generator: Gtranslator 3.36.0
X-Project-Style: gnome
  — gerenciador de sessões do GNOME %s [OPÇÃO…] COMANDO

Executa o COMANDO enquanto inibe algumas funcionalidades da sessão.

  -h, --help        Mostra esta ajuda
  --version         Mostra a versão do programa
  --app-id ID       O id do aplicativo a ser usado
                    quando inibir (opcional)
  --reason MOTIVO   O motivo para inibir (opcional)
  --inhibit ARG     Lista de elementos a serem inibidos, separados por dois
                    pontos:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Não executa o COMANDO e no lugar espera indefinidamente
  -l, --list        Lista as inibições existentes e sai

Se nenhuma opção --inhibit for especificada, assume-se como ocioso (idle).
 %s requer um argumento
 Ocorreu um problema e o sistema não pôde ser recuperado.
Por favor, encerre a sessão e tente novamente. Ocorreu um problema e o sistema não pôde ser recuperado. Todas as extensões foram desativadas por precaução. Ocorreu um problema e o sistema não pôde ser recuperado. Por favor, entre em contato com um administrador de sistemas Uma sessão com nome “%s” já existe AUTOSTART_DIR Permitir encerramento de sessão Não foi possível conectar ao gerenciador de sessões Não foi possível criar o soquete de escuta ICE: %s Personalizar Sessão personalizada Desabilita checagem de aceleração de hardware Não carrega os aplicativos especificados pelo usuário Não pedir confirmação do usuário Habilitar código de depuração Ocorreu falha ao executar %s
 GNOME GNOME experimental GNOME sobre Xorg Se habilitado, o gnome-session vai exibir um diálogo de aviso após a autenticação se a sessão entrou em modo reserva automaticamente. Se habilitado, o gnome-session vai perguntar ao usuário antes de terminar uma sessão. Se habilitado, o gnome-session vai salvar a sessão automaticamente. Ignorar qualquer um inibidor existente Encerrar sessão Pergunta ao encerrar Não respondendo Oh não! Alguma coisa está errada. Substitui os diretórios padrões de início automático Por gentileza selecione uma sessão personalizada a ser executada Desligar Programa chamado com opções conflitantes O programa requer exatamente um parâmetro Reiniciar Recusando a nova conexão do cliente porque a sessão está sendo desligada neste momento
 Lembrar dos aplicativos Reno_mear sessão Reinicia o dbus.service se estiver em execução Executa de ExecStopPost para iniciar gnome-session-failed.target ao falhar o serviço Executa como um serviço do systemd SESSION_NAME Salvar sessões Salvar essa sessão Sessão %d Não são permitidos a nomes de sessões conterem caracteres “/” Não são permitidos a nomes de sessões iniciarem com “.” Não são permitidos a nomes de sessões iniciarem com “.” ou conterem caracteres “/” Sessões para usar Mostrar alerta de extensão Mostra o diálogo da “falha da baleia” para teste Mostrar o aviso de reserva Inicialização de sinal feita para gnome-session Inicia o gnome-session-shutdown.target Inicia o gnome-session-shutdown.target ao receber um EOF ou um byte único no stdin Esta entrada permite a você selecionar uma sessão salva Este programa está bloqueando o encerramento da sessão. Essa sessão o leva ao GNOME Usa o gerenciador de sessões embutido (ao invés do baseado em systemd) Usa o gerenciamento de sessões do systemd Versão deste aplicativo Quando habilitado, o gnome-session vai salvar automaticamente a próxima sessão quando de seu encerramento mesmo se salvamento automático estiver desabilitado. _Continuar _Encerrar sessão _Encerrar sessão _Nova sessão _Remover sessão 