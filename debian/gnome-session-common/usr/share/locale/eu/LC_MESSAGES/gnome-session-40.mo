��    D      <  a   \      �     �  p        q  T   �  i   �  \   I	  '   �	     �	     �	  (   �	  )   
     <
     C
  #   R
  '   v
  $   �
     �
     �
     �
     �
       q     G   �  >   �           )     1     ?  !   N  '   p  %   �  	   �  '   �  #   �       P        l     �  %   �  M   �          "     /     =  
   O  ;   Z  3   �  Q   �          +  &   B     i  +   �  #   �  P   �  *   $      O      p  B   �     �     �  p     	   �     �     �     �     �  �  �     [    {     �  X   �  f     s   z  *   �           :  -   P  -   ~     �     �  4   �  1     &   8     _     t     �     �     �  u   �  V   /  ?   �  #   �     �     �            =   8  )   v     �  +   �  &   �     �  B        J     b  5   y  b   �  !        4     C     P     _  0   g  4   �  R   �           2  4   N     �  9   �  (   �  Z   �  5   Y  0   �     �  W   �  !   8      Z   �   s   	   �      !     !     #!     0!         !                    #       *   9   6   4           0   )              8       (      '   $   %           >                     	          -          A   D   ,                                        B              ?   ;              :      
       +   .   3          /   5   1           2   =          @      "      C                7   &      <        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: eu
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2020-08-23 10:00+0100
Last-Translator: Asier Sarasua Garmendia <asiersarasua@ni.eus>
Language-Team: Basque <librezale@librezale.eus>
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Project-Style: gnome
  — GNOMEren saio-kudeatzailea %s [AUKERA…] KOMANDOA

Exekutatu KOMANDOA saioaren beste funtzionalitate batzuk galarazten diren bitartean.

  -h, --help        Erakutsi laguntza hau
  --version         Erakutsi programaren bertsioa
  --app-id ID      Erabiliko den aplikazioaren IDa 
                        galaraztean (aukerakoa)
  --reason ZERGATIA Galarazteko zergatia (aukerakoa)
  --inhibit ARG     Galarazteko gauzak, komaz bereiztutako zerrenda:
                    logout (saio-amaiera), switch-user (erabiltzailez aldatzea),
                    suspend (esekitzea), idle (inaktiboa),
                    automount (muntai automatikoa)
  --inhibit-only    Ez abiarazi KOMANDOA eta itxaron beti
  -l, --list        Zerrendatu galarazpenak eta irten

Ez bada --inhibit aukera zehazten, aktibitate ez dagoela ulertzen da.
 %s(e)k argumentu bat behar du
 Arazo bat gertatu da eta sistemak ezin du berreskuratu.
Amaitu saioa eta saiatu berriro. Arazo bat gertatu da eta sistemak ezin du berreskuratu. Hedapen guztiak desgaitu egin dira badaezpada. Arazo bat gertatu da eta sistemak ezin du berreskuratu. Jar zaitez sistemaren administratzaile batekin harremanetan “%s“ izeneko saioa badago lehendik ere HASIERA_AUTOMATIKOKO_DIREKTORIOA Baimendu saio-amaiera Ezin izan da saio-kudeatzailearekin konektatu Ezin izan da entzuteko ICE socket-a sortu: %s Pertsonalizatua Saio pertsonalizatua Desgaitu hardware bidezko azelerazioaren egiaztaketa Ez kargatu erabiltzaileak zehaztutako aplikazioak Ez eskatu erabiltzailearen baieztapena Gaitu arazketa-kodea Huts egin du %s exekutatzean
 GNOME Probako GNOME GNOME Xorg gainean Gaituta badago, 'gnome-session'ek abisu bat bistaratuko du saioa hasi ondoren ordezko saioan sartu bada automatikoki. Gaituta badago, 'gnome-session'ek erabiltzaileari galdetuko dio saioa bukatu aurretik. Gaituta badago, gnome-session-ek saioa automatikoki gordeko du. Ez ikusi egin dauden inhibitzaileei Amaitu saioa Saio-amaierako gonbita Ez du erantzuten Zerbait gaizki irten da. Jaramonik ez egin hasiera automatikoko direktorio estandarrei Hautatu saio pertsonalizatua exekutatzeko Itzali Gatazkan dauden aukerekin deitu da programa Programak parametro bakar bat behar du Berrabiarazi Bezero berrien konexioa ukatzen, unean saioa itzaltzen ari delako
 Gogoratutako aplikazioa _Aldatu saioaren izena Berrabiarazi 'dbus.service' hura exekutatzen ari bada Exekutatu ExecStopPost-etik gnome-session-failed.target abiarazteko zerbitzuak huts egiten duenean systemd zerbitzu gisa exekutatzen SAIOAREN_IZENA Gorde saioak Gorde saio hau %d saio Saioen izenak ezin dute “/“ karakterea eduki Saioen izenak ezin dute “.“ karakterearekin hasi Saioen izenak ezin dute “.“ karakterearekin hasi, edo “/“ karakterea eduki Saioa erabiltzeko Erakutsi hedapenaren abisua Erakutsi errorearen elkarrizketa-koadroa probetarako Erakutsi ordezko abisua Seinalearen hasieratzea egin da 'gnome-session' programan Abiarazi 'gnome-session-shutdown.target' Abiarazi 'gnome-session-shutdown.target' EOF jasotzean edo byte bakarra jasotzean stdin-en Sarrera honek gordetako saio bat hautatzea uzten dizu Programa honek saioa amaitzea blokeatzen ari da. Saio honek GNOMEn sartuko zaitu Erabili integratutako saio-kudeatzailea (systemd sisteman oinarritutakoa erabili ordez) Erabili systemd saio-kudeatzailea Aplikazio honen bertsioa Gaituta badago, 'gnome-session'ek hurrengo saioa automatikoki gordeko du saioa amaitzean, nahiz eta gordetze automatikoa desgaituta egon. _Jarraitu Amaitu _saioa _Amaitu saioa Saio _berria _Kendu saioa 