��    ;      �  O   �           	  4  (     ]  T   v  i   �  \   5  '   �     �     �  (   �  )   �     (	     /	  #   >	  '   b	  $   �	     �	     �	     �	     �	     �	  q   �	  G   m
  >   �
      �
               +  !   :  '   \  %   �  	   �  '   �     �  P   �     4     K     [     h     v  
   �  ;   �  3   �  Q        U     d  &   {     �  *   �      �           )  p   E  	   �     �     �     �     �  �  �     �  ~  �     ?  f   V  �   �  e   B  $   �     �     �  $   �  +        D     Q  -   b  ,   �  $   �     �     �               &  `   4  J   �  1   �       	   0     :     S  0   _  7   �  $   �     �  )   �  
   #  _   .     �     �     �     �     �     �  '   �        9   )     c  #   y  -   �     �  9   �  $     %   >     d  }   |     �  	     
     	        !         9      *         %      :           4   8      ;               5            7   !   '      &              (   	          ,      #              -       .      3                0      $      +       )       2       1                        /   6               
              "        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2019-02-21 08:50+0000
Last-Translator: Sveinn í Felli <sv1@fellsnet.is>
Language-Team: Icelandic <translation-team-is@lists.sourceforge.net>
Language: is
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Lokalize 2.0
  — GNOME setustýringin %s [VALKOSTUR…] SKIPUN

Keyrir SKIPUN en kemur í veg fyrir einhverja eiginleika setu.

  -h, --help        Birtir þessa hjálp
  --version         Birtir útgáfuupplýsingar
  --app-id ID       Auðkenni forritsins sem á að nota
                    við hindrun - inhibit (valkvætt)
  --reason REASON   Ástæða fyrir hindrunum (valkvætt)
  --inhibit ARG     Hvað á að hindra, listi með kommuaðgreinum:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Ekki keyra SKIPUN og þess í stað bíða til eilífðar

Ef engin --inhibit kostur er tilgreindur, ganga út frá 'idle' sem vísu.
 %s krefst færibreytu
 Vandamál kom upp og kerfið nær ekki að vinna sig út úr því.
Skráðu þig út og reyndu aftur. Vandamál kom upp og kerfið nær ekki að vinna sig út úr því. Allar viðbætur hafa verið gerðar óvirkar í varúðarskyni. Vandamál kom upp og kerfið nær ekki að vinna sig út úr því. Hafðu samband við kerfisstjóra Seta með heitinu '%s' er þegar til AUTOSTART_DIR Leyfa að skrá sig út Ekki tókst að tengjast setustjóra Gat ekki búið til ICE hlustunar tengi: %s Sérsniðið Sérsniðin seta Gera prófun á vélbúnaðarhröðun óvirka Ekki hlaða inn forritum völdum að notanda Ekki spyrja notanda um staðfestingu Virkja aflúsunarham Mistókst að keyra %s
 GNOME GNOME sýnishorn GNOME á Xorg Ef virkt mun gnome-setan birta aðvörun eftir innskráningu ef sjálfkrafa var notuð varaseta. Ef virkt mun gnome-setan spyrja um staðfestingu áður en setu er lokið. Ef virkt mun gnome-setan vista setuna sjálkrafa. Hunsa allt sem gæti truflað Skrá út Staðfesta útskráningu Svarar ekki Úbbs! Eitthvað hefur farið úrskeiðis núna. Ekki nota sjálfgefna möppu fyrir sjálfvirka ræsingu Veldu sérsniðna setu til að keyra Slökkva á Kallað á forrit með röngum stillingum Endurræsa Leyfi biðlara ekki að tengjast vegna þess að það er verið að slökkva á þessari setu
 Munað forrit Endur_nefna setu SESSION_NAME Vista setur Vista þessa setu Seta %d Heiti setu má ekki innihalda '/' stafi Heiti setu má ekki byrja á '.' Heiti setu má ekki byrja á '.' eða innihalda '/' stafi Setan sem á að nota Birta aðvörun vegna skráaendinga Birta 'bilaða bilunargluggann' til prófunar Birta varaaðvörun Þessi færsla gerir þér kleift að velja vistaða setu Þetta forrit hindrar útskráningu. Þessi seta skráir þig inn í GNOME Útgáfa þessa forrits Ef virkt mun gnome-setan vista næstu setu sjálkrafa við útskráningu jafnvel þótt sjálfvirk vistun á setu sé óvirk. _Áfram _Útskrá S_krá út _Ný seta Fja_rlægja setu 