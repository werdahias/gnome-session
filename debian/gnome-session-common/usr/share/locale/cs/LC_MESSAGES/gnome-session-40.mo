��    D      <  a   \      �     �  p        q  T   �  i   �  \   I	  '   �	     �	     �	  (   �	  )   
     <
     C
  #   R
  '   v
  $   �
     �
     �
     �
     �
       q     G   �  >   �           )     1     ?  !   N  '   p  %   �  	   �  '   �  #   �       P        l     �  %   �  M   �          "     /     =  
   O  ;   Z  3   �  Q   �          +  &   B     i  +   �  #   �  P   �  *   $      O      p  B   �     �     �  p     	   �     �     �     �     �  �  �     �  �  �     �  _   �  m      ^   n  (   �     �     
  $      ,   E     r     {  (   �  (   �  %   �               4     :     F  �   T  W   �  B   ;  ,   ~     �     �     �     �  :   �  7   0     h  '   p  )   �     �  R   �     !     8  '   N  Y   v     �     �     �       
   "  -   -  /   [  K   �     �  !   �  6     ,   P  8   }  %   �  l   �  1   I  #   {  &   �  I   �  "         3   �   H      �      �      �      �      !         !                    #       *   9   6   4           0   )              8       (      '   $   %           >                     	          -          A   D   ,                                        B              ?   ;              :      
       +   .   3          /   5   1           2   =          @      "      C                7   &      <        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session master
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2020-08-09 17:59+0200
Last-Translator: Marek Černocký <marek@manet.cz>
Language-Team: čeština <gnome-cs-list@gnome.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Poedit 2.2.3
X-Project-Style: gnome
  – správce sezení GNOME %s [VOLBA…] PŘÍKAZ

Spustí PŘÍKAZ a zablokuje určitou vlastnost sezení.

  -h, --help        Zobrazit tuto nápovědu
  --version         Zobrazit verzi programu
  --app-id ID       ID aplikace, které se má použít při zablokování
                    (volitelné)
  --reason DŮVOD    Důvod zablokování (volitelné)
  --inhibit ARG     Vlastnosti k zablokování, seznam z následujících možností
                    oddělených dvojtečkou:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Nespustit PŘÍKAZ a namísto toho čekat donekonečna
  -l, --list        Vypsat současná blokování a skončit

Pokud není zadána volba --inhibit, použije se idle.
 %s vyžaduje argument
 Došlo ke komplikacím a systém se nepodařilo obnovit.
Odhlaste se prosím a zkuste to znovu. Došlo ke komplikacím a systém se nepodařilo obnovit. Všechna rozšíření byla preventivně zakázána. Došlo ke komplikacím a systém se nepodařilo obnovit. Kontaktujte prosím správce systému Sezení s názvem „%s“ již existuje ADR_AUTOSPUŠTĚNÍ Umožnit odhlášení Nelze se spojit se správcem sezení Nelze vytvořit socket naslouchání ICE: %s Vlastní Vlastní sezení Zakázat kontrolu hardwarové akcelerace Nenahrávat uživatelem zadané aplikace Nevyžadovat potvrzení od uživatele Zapnout ladicí kód Nepodařilo se spustit %s
 GNOME GNOME dummy GNOME na Xorg Když je zapnuto, gnome-session zobrazí po přihlášení varovné dialogové okno v situaci, kdy se automaticky použije záložní sezení. Když je zapnuto, gnome-session se bude před ukončením sezení dotazovat uživatele. Když je zapnuto, bude gnome-session automaticky ukládat sezení. Ignorovat jakékoliv existující inhibitory Odhlásit se Dotaz na odhlášení Neodpovídá Jejda! Něco se pokazilo. Přepsat standardní adresáře automatického spuštění Vyberte prosím vlastní sezení, které chcete spustit Vypnout Program volán s kolidujícími volbami Program potřebuje právě jeden parametr Restartovat Odmítnutí spojení nového klienta, jelikož právě probíhá vypnutí sezení
 Zapamatovaná aplikace Přej_menovat sezení Restartovat dbus.service, pokud běží Běžet od ExecStopPost do spuštění gnome-session-failed.target při selhání služby Běží jako služba systemd NÁZEV_SEZENÍ Ukládat sezení Uložit toto sezení Sezení %d Názvy sezení nemohou obsahovat znak „/“ Názvy sezení nemohou začínat znakem „.“ Názvy sezení nemohou začínat znakem „.“ nebo obsahovat znak „/“ Sezení, které se má použít Zobrazit varování rozšíření Zobrazit dialogové okno černé barvy pro testování Zobrazit varování při záložním sezení Dokončena signálová inicializace vůči gnome-session Spustit gnome-session-shutdown.target Spustit gnome-session-shutdown.target při zachycení signálu EOF nebo jednoho bajtu na standardním vstupu Tato položka umožňuje vybrat uložené sezení Tento program brání odhlášení. Toto sezení vás přihlásí do GNOME Používat vestavěnou správu sezení (místo té založené na systemd) Používat správu sezení systemd Verze této aplikace Když je zapnuto, gnome-session při odhlášení automaticky uloží příští sezení, i když je vypnuté automatické ukládání. _Pokračovat _Odhlásit se Odh_lásit se _Nové sezení Odst_ranit sezení 