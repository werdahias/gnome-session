��    D      <  a   \      �     �  p        q  T   �  i   �  \   I	  '   �	     �	     �	  (   �	  )   
     <
     C
  #   R
  '   v
  $   �
     �
     �
     �
     �
       q     G   �  >   �           )     1     ?  !   N  '   p  %   �  	   �  '   �  #   �       P        l     �  %   �  M   �          "     /     =  
   O  ;   Z  3   �  Q   �          +  &   B     i  +   �  #   �  P   �  *   $      O      p  B   �     �     �  p     	   �     �     �     �     �  )  �  5   �  �    )   �  �   �  �   �  �   B  A   �  3   $  )   X  M   �  X   �     )  #   @  G   d  N   �  6   �  @   2  3   s     �     �  #   �  �   �  �   �  p   F  8   �     �              5   6   ]   l   N   �      !  U   (!  D   ~!     �!  q   �!  !   Q"      s"  E   �"  �   �"  4   e#     �#     �#  "   �#     �#  O   $  J   W$  y   �$  6   %  8   S%  P   �%  7   �%  O   &  ,   e&  �   �&  _   A'  <   �'  ;   �'  �   (  B   �(  &   �(  �   )     �)     �)     *      *     7*         !                    #       *   9   6   4           0   )              8       (      '   $   %           >                     	          -          A   D   ,                                        B              ?   ;              :      
       +   .   3          /   5   1           2   =          @      "      C                7   &      <        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2020-08-18 23:06+0200
Last-Translator: Марко М. Костић <marko.m.kostic@gmail.com>
Language-Team: Serbian <gnome-sr@googlegroups.org>
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2
X-Project-Style: gnome
X-Generator: Gtranslator 3.36.0
  — управник Гномовом сесијом %s [ОПЦИЈА...] НАРЕДБА

Извршите НАРЕДБУ уз спречавање неких функционалности сесије.

  -h, --help        Приказује ову помоћ
  --version         Приказује издање програма
  --app-id ИБ       ИБ програма за коришћење приликом
                    спречавања (изборно)
  --reason РАЗЛОГ   Разлог спречавања (изборно)
  --inhibit АРГ     Списак ствари за спречавање, раздвојених двотачкама:
                    logout (одјављивање), switch-user (промена корисника),
                    suspend (обустава), idle (мировање), automount (самоприкачињање)
  --inhibit-only    Не покреће НАРЕДБУ и уместо тога чека у недоглед
  -l, --list        Приказује тренутна спречавања и излази

Ако ниједна опција спречавања (--inhibit) није наведена, подразумева се стање мировања (idle).
 „%s“ захтева аргумент
 Дошло је до проблема и систем не може да се опорави.
Одјавите се и покушајте опет. Дошло је до проблема и систем не може да се опорави. Сва проширења су искључена зарад предострожности. Дошло је до проблема и систем не може да се опорави. Обратите се администратору система Већ постоји сесија под називом „%s“ ДИРЕКТОРИЈУМ_САМОПОКРЕТАЊА Дозвољава одјављивање Не могу да се повежем са управником сесије Не могу да направим прикључак ИЦЕ ослушкивања: %s Прилагођено Прилагођена сесија Искључује проверу хардверског убрзања Не учитава програме које је задао корисник Не тражи потврду од корисника Укључује код за исправљање грешака Нисам успео да извршим „%s“
 Гном Гномов лажњак Гном на Икс серверу Ако је омогућено, гномова сесија ће приказати прозорче са упозорењем након пријаве ако је сесија одступна. Ако је омогућено, гномова сесија ће питати корисника да ли жели да оконча сесију. Ако је омогућено, гномова сесија ће самостално чувати сесију. Занемарујем постојеће ометаче Одјави ме Упит за одјаву Не даје одзив О, не!  Нешто је пошло наопако! Заобилази уобичајене директоријуме самопокретања Изаберите прилагођену сесију за покретање Искључи Програм је позван са несагласним могућностима Програму треба тачно један параметар Поново покрени Одбацујем везу са новим клијентом јер је у току гашење сесије
 Запамћени програм Преименуј _сесију Поново покрени dbus.service ако је покренут Покретање из ExecStopPost-а зарад покретања gnome-session-failed.target при неуспешној услузи Покрени као систем-де услугу НАЗИВ_СЕСИЈЕ Сачувај сесије Сачувај ову сесију Сесија „%d“ Називи сесија не могу да садрже косу црту (/) Називи сесија не могу да почињу тачком (.) Називи сесија не могу да почињу тачком (.) или да садрже косу црту (/) Сесије које ће бити коришћене Приказује упозорење проширења Приказује прозорче неуспеха за испробавање Прикажи упозорење о одступању Иницијализација сигнала урађено за gnome-session Покрени gnome-session-shutdown.target Покрени gnome-session-shutdown.target након примања EOF сигнала или примања једног бајта на стандардном улазу Ова ставка вам допушта да изаберете сачувану сесију Овај програм онемогућава одјаву. Ова сесија вас пријављује у Гном Користи уграђено управљање сесије (уместо оне засноване на систем-деу) Користи систем-де управљање сесијом Издање овог програма Када је омогућено, гномова сесија ће самостално сачувати следећу сесију при одјави чак иако је самостално чување искључено. _Настави _Одјави ме _Одјави ме _Нова сесија _Уклони сесију 