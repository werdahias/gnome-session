��    @        Y         �     �  p  �       T   *  i     \   �  '   F	     n	     |	  (   �	  )   �	     �	     �	  #   �	  '   
  $   >
     c
     y
     �
     �
     �
  >   �
      �
            !   &  '   H  %   p  	   �  '   �  #   �     �  P   �     D     [  %   k  M   �     �     �            
   '  ;   2  3   n  Q   �     �       &        A  +   [  #   �  P   �  *   �      '      H  B   i     �     �  	   �     �     �            �        �  w  �     Q  ^   h  n   �  f   6  +   �     �     �  #   �  -        :     G  )   Y  $   �  '   �     �     �          	       8   $     ]     {     �     �  *   �  *   �       1   	  *   ;     f  A   u     �     �  ,   �  W        `     }     �     �     �  &   �  "   �  ?   �     <     Q     l      �  .   �  #   �  R   �  /   N  #   ~     �  9   �     �       	   4     >     G     P     Y     )   5           @                 .   "          &   4           6      ;   2              (          0   %   :   <                               /                  +   >              $             7                 1   9   	             -             '   !         *   ,   3   
         #           =            ?      8     — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session 3.40.x
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2021-03-13 17:40+0100
Last-Translator: Kjartan Maraas <kmaraas@gnome.org>
Language-Team: Norwegian bokmål <i18n-nb@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 – GNOME økthåndterer %s [FLAGG …] KOMMANDO

Kjør KOMMANDO mens noe av øktens funksjonalitet holdes tilbake.

  -h, --help        Vis denne hjelpen
  --version         Vis versjon for programmet
  --app-id ID       Applikasjons-ID som skal brukes
                    ved hindring (valgfri)
  --reason ÅRSAK    Årsak for hindring
  --inhibit ARG     Ting som skal hindres. Kolonseparert liste med:
                    logout, switch-user, suspend, idle og automount
  --inhibit-only    Ikke start KOMMANDO og vent for alltid i stedet
  -l, --list        Vis eksisterende hindringer og avslutt
  
Hvis ingen flagg gis til --inhibit vil idle brukes.
 %s krever et argument
 Et problem har oppstått og systemet kan ikke gjenopprettes.
Vennligst logg ut og prøv igjen. Et problem har oppstått og systemet kan ikke gjenopprettes. Alle utvidelser er slått av som følge av dette. Et problem har oppstått og systemet kan ikke gjenopprettes. Vennligst kontakt en systemadmininstrator En økt med navn «%s» eksisterer allerede AUTOSTART_DIR Tillat utlogging Kunne ikke koble til økthåndterer Kunne ikke opprette plugg for ICE-lytting: %s Egendefinert Egendefinert økt Slå av sjekk for maskinvareaksellerasjon Ikke last brukerdefinerte programmer Ikke spør etter bekreftelse fra bruker Aktiver feilsøkingskode Klarte ikke å kjøre %s
 GNOME GNOME dummy GNOME på Xorg Hvis aktivert vil gnome-session lagre økten automatisk. Overser eventuelle hindringer Logg ut Svarer ikke Å nei! Noe har gått galt. Overstyr forvalgte kataloger for autostart Velg en egendefinert økt som skal kjøres Slå av Programmet ble kalt med motstridende alternativer Programmet trenger kun eksakt en parameter Start på nytt Nekter ny klient tilgang fordi økten er i ferd med å avsluttes
 Husket program E_ndre navn på økt Start dbus.service på nytt hvis den kjører Kjør fra ExecStopPost for å starte gnome-session-failed.target ved feil på tjenesten Kjører som systemd tjeneste SESSION_NAME Lagre økter Lagre denne økten Økt %d Øktnavn kan ikke inneholde «/»-tegn Øktnavn kan ikke starte med «.» Øktnavn kan ikke starte med «.» eller inneholde tegnet «/» Økt som skal brukes Vis advarsel om utvidelser Vis feildialog for testing Vis advarsel for reserveløsning Signaliser initiering ferdig til gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target når EOF eller en enkelt byte mottas på stdin Denne oppføringen lar deg velge en lagret økt Dette programmet hindrer utlogging. Denne økten logger inn i GNOME Bruk innebygget økthåndterer (i stedet for systemd sin) Bruk systemd økthåndterer Versjon av dette programmet _Fortsett _Logg ut _Logg ut _Ny økt Fje_rn økt 