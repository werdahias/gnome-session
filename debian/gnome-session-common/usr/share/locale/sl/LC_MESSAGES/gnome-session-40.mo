��    D      <  a   \      �     �  p        q  T   �  i   �  \   I	  '   �	     �	     �	  (   �	  )   
     <
     C
  #   R
  '   v
  $   �
     �
     �
     �
     �
       q     G   �  >   �           )     1     ?  !   N  '   p  %   �  	   �  '   �  #   �       P        l     �  %   �  M   �          "     /     =  
   O  ;   Z  3   �  Q   �          +  &   B     i  +   �  #   �  P   �  *   $      O      p  B   �     �     �  p     	   �     �     �     �     �    �     �  �  �     �  [   �  �   �  W   �      �     �       +     .   E     t     |      �  ,   �      �     �          1     7     F  W   \  A   �  6   �  '   -     U     \  	   p  $   z  +   �     �  	   �  4   �  $   )     N  E   \     �     �  9   �  O         P     m     v     �     �  1   �  .   �  W   �     O     _  0   }  &   �  #   �  *   �  s   $  &   �     �  &   �  <     !   C     e  p   y  	   �     �     �  
                   !                    #       *   9   6   4           0   )              8       (      '   $   %           >                     	          -          A   D   ,                                        B              ?   ;              :      
       +   .   3          /   5   1           2   =          @      "      C                7   &      <        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session master
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2020-08-19 21:46+0200
Last-Translator: Matej Urbančič <mateju@svn.gnome.org>
Language-Team: Slovenian GNOME Translation Team <gnome-si@googlegroups.com>
Language: sl_SI
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n%100==4 ? 3 : 0);
X-Poedit-SourceCharset: utf-8
X-Generator: Poedit 2.3
 – upravljalnik seje GNOME %s [MOŽNOST...] UKAZ

Izvajanje ukaza z onemogočenjem nekaterih zmožnosti seje.

  -h, --help        Pokaže to pomoč
  --version         Pokaže različico programa
  --app-id ID       ID programa za uporabo
                    pri onemogočanju (izbirno)
  --reason RAZLOG   Razlog za onemogočanje (izbirno)
  --inhibit ARG     Možnosti za onemogočanje, ločene z vejico:
                    logout (odjava), switch-user (preklop uporabnika)
  --inhibit-only    Ne zaženi ukaza, ampak čakaj v nedogled
  -l, --list        Izpiše seznam obstoječih zavrnitev

Kadar ni izbrane možnosti --inhibit, je uporabljen argument idle (nedejavno).
 %s zahteva določen argument
 Prišlo je do napake, ki je ni mogoče samodejno odpraviti.
Odjavite se in poskusite znova. Prišlo je do napake, ki je ni mogoče samodejno odpraviti. Priporočljivo je onemogočiti vse razširitve, dokler se napaka ne razreši. Prišlo je do napake, ki je ni mogoče samodejno odpraviti. Stopite v stik s skrbnikom. Seja z imenom »%s« že obstaja ZAČETNA_MAPA Dovoli odjavo Ni se mogoče povezati z upravljalnikom sej Ni mogoče ustvariti vtiča za prisluh ICE: %s Po meri Seja po meri Onemogoči strojno pospeševanje Ne naloži uporabniško določenih programov Ne zahtevaj potrditve uporabnika Omogoči razhroščevanje kode Napaka med izvajanjem %s
 GNOME Poskusni GNOME GNOME na sistemu Xorg Izbrana možnost omogoči prikaz opozorila po prijavi, če je seja samodejno povrnjena. Izbrana možnost omogoči prikaz vnosnega polja pred koncem seje. Izbrana možnost omogoči samodejno shranjevanje seje. Prezri obstoječe oviralnike dejavnosti Odjava Vnosno polje odjave Ni odziva Prišlo je do nepričakovane napake. Preskoči običajne mape samodejnega zagona Izberite sejo po meri za zagon Izključi Program zagnan z nasprotujočimi si možnostmi ukaza Program zahteva natanko en parameter Ponovni zagon Povezave z odjemalci bodo zavrnjene, ker se trenutna seja izklaplja.
 Zapomnjeni programi Pre_imenuj sejo Ponovno zaženi storitev dbus.service, če je že zagnana Zaženi iz ExecStopPost za zagon gnome-session-failed.target ob spodleteli seji Zagnano kot storitev systemd IME_SEJE Shrani seje Shrani sejo Seja %d Imena seje ne smejo vsebovati poševnice » / «. Imena seje se ne smejo začeti s piko » . «. Imena seje se ne smejo začeti s piko » . « in ne smejo vsebovati poševnice » / «. Seja za uporabo Pokaži opozorila razširitev Pokaži pogovorno okno napak med preizkušanjem. Pokaži opozorila programske povrnitve Začenjanje signala za gnome-sesion Zaženi ukaz gnome-session-shutdown.target Zaženi ukaz gnome-session-shutdown.target pri prejetju ukaza konca datoteke EOF oziroma enobajtni standardni dovod Možnost omogoča izbor shranjene seje Program zavira postopek odjave. Seja omogoča prijavo v namizje GNOME. Uporabi vgrajen upravljalnik seje (namesto storitev systemd) Uporabi upravljalnik seje systemd Različica programa Izbrana možnost omogoči samodejno shranjevanje seje ob odjavi, neglede na nastavitev samodejnega shranjevanja. N_adaljuj _Odjava _Odjavi _Nova seja _Odstrani sejo 