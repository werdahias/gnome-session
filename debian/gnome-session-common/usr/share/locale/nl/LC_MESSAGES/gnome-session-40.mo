��    D      <  a   \      �     �  p        q  T   �  i   �  \   I	  '   �	     �	     �	  (   �	  )   
     <
     C
  #   R
  '   v
  $   �
     �
     �
     �
     �
       q     G   �  >   �           )     1     ?  !   N  '   p  %   �  	   �  '   �  #   �       P        l     �  %   �  M   �          "     /     =  
   O  ;   Z  3   �  Q   �          +  &   B     i  +   �  #   �  P   �  *   $      O      p  B   �     �     �  p     	   �     �     �     �     �  �  �     Z  �  v  "   X  {   {  �   �  u   x  ,   �          )  .   ;  -   j  	   �     �  )   �  <   �  %        A     \     w     }     �  �   �  X     E   s  .   �     �     �     �       ,   (  )   U       0   �  )   �     �  M   �     M     d  '   v  S   �     �               ,  	   @  '   J  /   r  C   �     �  &   �  $   !     F  9   b  #   �  [   �  /     (   L  $   u  I   �     �        �      	   �   	   �   	   �      �      �          !                    #       *   9   6   4           0   )              8       (      '   $   %           >                     	          -          A   D   ,                                        B              ?   ;              :      
       +   .   3          /   5   1           2   =          @      "      C                7   &      <        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2020-08-25 17:26+0200
Last-Translator: Nathan Follens <nthn@unseen.is>
Language-Team: Dutch <gnome-nl-list@gnome.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.4.1
  — het Gnome-sessiebeheer %s [OPTIE…] OPDRACHT

Functionaliteit van de sessie uitschakelen tijdens het uitvoeren
van de opgegeven OPDRACHT.

  -h, --help        Deze uitleg tonen
  --version         Programmaversie tonen
  --app-id ID       Te gebruiken toepassings-id (optioneel)
  --reason REDEN    Reden voor voor uitschakelen sessiefuncties (optioneel)
  --inhibit ARG     Uit te schakelen functionaliteit; dit is een met dubbele
                    punten gescheiden opsomming van:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    OPDRACHT niet uitvoeren, maar alleen wachten
  -l, --list        Lijst de bestaande inhibities op, en sluit af

Als ‘--inhibit’ niet opgegeven wordt, wordt alleen ‘idle’ gebruikt.
 %s heeft een argumentwaarde nodig
 Er is een probleem opgetreden dat niet door het systeem kan worden hersteld.
Probeer af te melden en opnieuw aan te melden. Er is een probleem opgetreden dat niet door het systeem kan worden hersteld. Alle uitbreidingen zijn uit voorzorg uitgeschakeld. Er is een probleem opgetreden dat niet door het systeem kan worden hersteld. Neem contact op met een systeembeheerder Er bestaat reeds een sessie genaamd ‘%s’ AUTOSTART_MAP Afmelden toestaan Kan geen verbinding maken met het sessiebeheer Kon geen luister-socket voor ICE aanmaken: %s Aangepast Aangepaste sessie Hardwareversnellingscontrole uitschakelen Toepassingen die door de gebruiker opgegeven zijn niet laden Geen bevestiging van gebruiker vragen Debugging-code inschakelen Fout bij uitvoeren van %s
 Gnome Gnome dummy Gnome op Xorg Indien ingeschakeld, zal gnome-session een waarschuwingsdialoog tonen na het aanmelden als de sessie automatisch is teruggevallen. Indien ingeschakeld, zal gnome-session de gebruiker vragen om een sessie te beëindigen. Indien ingeschakeld, zal gnome-session de sessie automatisch opslaan. Actieve verhindering door toepassingen negeren Afmelden Afmeldprompt Reageert niet Oeps! Er is iets misgegaan. Map voor automatisch te starten toepassingen Selecteer de te starten aangepaste sessie Computer uitzetten Toepassing aangeroepen met conflicterende opties Programma vereist precies één parameter Opnieuw opstarten Nieuwe verbinding wordt geweigerd omdat de sessie momenteel wordt afgesloten
 Onthouden toepassingen Sessie _hernoemen Herstart dbus.service indien het draait Uitvoeren van ExecStopPost om gnome-session-failed.target te starten bij dienstfout Draait als systemd-dienst NAAM_VAN_SESSIE Sessies opslaan Deze sessie opslaan Sessie %d Sessienamen mogen geen ‘/’ bevatten Sessienamen mogen niet met een ‘.’ beginnen Sessienamen mogen niet met een ‘.’ beginnen of ‘/’ bevatten Te gebruiken sessie Waarschuwingen van uitbreidingen tonen Het misvis-venster tonen voor testen Fallback-waarschuwing tonen Voltooiing van initialisering doorgeven aan gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target wanneer EOF of een enkele byte ontvangen wordt op stdin Hiermee kunt u een opgeslagen sessie selecteren Deze toepassing verhindert het afmelden. Deze sessie laat u in Gnome inloggen Ingebouwd sessiebeheer gebruiken (in plaats van dat gebaseerd op systemd) Systemd-sessiebeheer gebruiken Versie van deze toepassing Indien ingeschakeld, zal gnome-session de volgende sessie automatisch opslaan bij het afmelden, zelfs wanneer automatisch opslaan is uitgeschakeld. _Doorgaan _Afmelden _Afmelden _Nieuwe sessie Sessie _verwijderen 