Þ    D      <  a   \      à     á  p        q  T     i   ß  \   I	  '   ¦	     Î	     Ü	  (   é	  )   
     <
     C
  #   R
  '   v
  $   
     Ã
     Ù
     ï
     õ
       q     G     >   É           )     1     ?  !   N  '   p  %     	   ¾  '   È  #   ð       P        l       %     M   ¹          "     /     =  
   O  ;   Z  3     Q   Ê          +  &   B     i  +     #   ¯  P   Ó  *   $      O      p  B        Ô     ó  p     	                       ©  Ä  ¹     ~  p         r   '          "  ,   ¥     Ò     à  2   ö  *   )     T  "   j  #     '   ±  )   Ù               /     8     Q  q   k  G   Ý  >   %  1   d  
        ¡     ¯     Á  '   á  =   	  
   G  :   R  #        ±  a   Å  $   '     L  %   j  M        Þ     ù               &  A   4  >   v  b   µ           '  &   H     o  +     #   µ  P   Ù  F   *  -   q  E     B   å     (      G   p   c      Ô      â      î      ú      !         !                    #       *   9   6   4           0   )              8       (      '   $   %           >                     	          -          A   D   ,                                        B              ?   ;              :      
       +   .   3          /   5   1           2   =          @      "      C                7   &      <        â the GNOME session manager %s [OPTIONâ¦] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system canât recover.
Please log out and try again. A problem has occurred and the system canât recover. All extensions have been disabled as a precaution. A problem has occurred and the system canât recover. Please contact a system administrator A session named â%sâ already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Donât prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain â/â characters Session names are not allowed to start with â.â Session names are not allowed to start with â.â or contain â/â characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session.HEAD.he
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2020-09-28 20:21+0300
Last-Translator: Yosef Or Boczko <yoseforb@gmail.com>
Language-Team: Hebrew <yoseforb@gmail.com>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n>2||n==0) ? 1 : 2
X-Generator: Gtranslator 3.36.0
  â the GNOME session manager %s [OPTIONâ¦] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 ×××¨×¢× ×©×××× ××××¢×¨××ª ××× × ××××× ×××©×ª×§× ××× ×.
× × ××¦××ª ××× ×¡××ª ×©××. ×××¨×¢× ×©×××× ××××¢×¨××ª ××× × ××××× ×××©×ª×§× ××× ×. ×××ª×¨ ×××××× ×××©××ª× ×× ×××¨××××ª. ×××¨×¢× ×©×××× ××××¢×¨××ª ××× × ××××× ×××©×ª×§× ××× ×. × × ×××¦××¨ ×§×©×¨ ×¢× ×× ×× ×××¢×¨××ª. ×××¨ ×§××××ª ××¤×¢×× ××©× â%sâ AUTOSTART_DIR ×××¤×©×¨ ××¦××× ×× × ××ª× ×××ª×××¨ ××× ×× ×××¤×¢×× ×× × ××ª× ×××¦××¨ ×©×§×¢ ICE: â%s ××ª××× ×××©××ª ××¤×¢×× ×××ª×××ª ×××©××ª Disable hardware acceleration check Do not load user-specified applications ××× ×××§×© ×××©××¨ ××××©×ª××© Enable debugging code Failed to execute %s
 âGNOME â××××× ×©× GNOME âGNOME ×¢× ××× Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. ×ª×× ××ª×¢××××ª ×××¢×§××× ×§××××× ××¦××× Logout prompt ××× ×ª×××× ××××!  ××©×× ××©×ª××©. Override standard autostart directories × × ×××××¨ ××¤×¢×× ×××ª×××ª ×××©××ª ×××¨×¦× ××××× ××ª×× ××ª × ×§×¨×× ×¢× ××¤×©×¨××××ª ×¡××ª×¨××ª Program needs exactly one parameter ××¤×¢×× ××××© ××¡×¨× ××××××¨ ××§×× ×××© ××××× ×©×××¤×¢×× × ××¦××ª ××××× ×××××
 ×××©×× ××©×××¨ ×××××¨×× _×©×× ×× ×©× ×××¤×¢×× Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session ××¤×¢×× %d ××¡××¨ ×©×©×××ª ×××¤×¢×××ª ××××× ××ª ××ª× â/â ××¡××¨ ×©×©×××ª ×××¤×¢×××ª ××ª×××× ××ª× â.â ××¡××¨ ×©×©×××ª ×××¤×¢×××ª ××ª×××× ××ª× â.â ×× ××××× ××ª ××ª× â/â Session to use ××¦××ª ××××¨×ª ××¨××××ª Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin ×¨×©××× ×× ×××¤×©×¨×ª ×× ×××××¨ ×××¤×¢×× ×©× ×©××¨× ×ª×× ××ª ×× ××× ×¢×ª ××ª ×××¦×××. ××¤×¢×× ×××ª ××××¨×ª ×××ª× ××©×××× ××¢×××× GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. ×××_×©×× _× ××ª××§ ×_×¦××× ××¤×¢×× _×××©× ×_×¡×¨×ª ××¤×¢×× 