Þ    X      Ü                  4  ¶     ë	  R   
  g   W
  Z   ¿
  '        B     P     d       5     	   Å  7   Ï  1     	   9  	   C  (   M  )   v           À     Ç  %   Ö  '   ü  "   $     G     \     r     z       !        ·     ½     É     Ú     Ý      â                    "     8  !   G     i  '   q  %     	   ¿     É  '   Ñ     ù  P         Q     h     x       
          ;   »  3   ÷  Q   +     }          £  &   Ã  +   ê          4     @      U     v  #         «  *   Ì      ÷        /   9  &   i          ¯  =   Ë  	   	               %     ,  (   9     b    r     ú  *       D  C   U  W     ?   ñ  &   1     X     f  /        ¯  -   ¼  	   ê  5   ô  0   *     [     g     s  "        µ     Î     Õ  *   è  *        >     T     m  	   }            %        Å     Ë     ×     é     ì  !   ó               )     6     R  $   _       !     '   ­     Õ     â  !   é       L        e     {          ¥     ²     Â  /   Þ  -     D   <               ³  '   Ò  $   ú          ;     K     ^     }  !        ²  -   Ë  !   ù  $     5   @  )   v            Á  7   ×  
      
      
   %      0      <   %   S      y      (   '          2      T   Q                     B   R       #      1   6         
   >   L   8          +      H   5   E              S   C   9   ,   G       0   J          -   !                 M                  A       P      K          3       :   N          I           =              	       D   .      F       U   ;                          $       4       "   7       W   ?   %          O   X             /   @           V                           &   )   *      <     - the GNOME session manager %s [OPTION...] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can't recover.
Please log out and try again. A problem has occurred and the system can't recover. All extensions have been disabled as a precaution. A problem has occurred and the system can't recover. Please contact a system administrator A session named â%sâ already exists AUTOSTART_DIR Add Startup Program Additional startup _programs: Allow logout Application does not accept documents on command line Browseâ¦ Can't pass document URIs to a 'Type=Link' desktop entry Choose what applications to start when you log in Co_mmand: Comm_ent: Could not connect to the session manager Could not create ICE listening socket: %s Could not display help document Custom Custom Session Disable connection to session manager Do not load user-specified applications Don't prompt for user confirmation Edit Startup Program Enable debugging code Enabled FILE Failed to execute %s
 File is not a valid .desktop file GNOME GNOME dummy GNOME on Wayland ID Icon Ignoring any existing inhibitors Log out No description No name Not a launchable item Not responding Oh no!  Something has gone wrong. Options Override standard autostart directories Please select a custom session to run Power off Program Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Select Command Session %d Session management options: Session names are not allowed to contain â/â characters Session names are not allowed to start with â.â Session names are not allowed to start with â.â or contain â/â characters Session to use Show extension warning Show session management options Show the fail whale dialog for testing Specify file containing saved configuration Specify session management ID Starting %s Startup Applications Startup Applications Preferences Startup Programs The startup command cannot be empty The startup command is not valid This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME This session logs you into GNOME, using Wayland Unrecognized desktop file Version '%s' Unrecognized launch option: %d Version of this application _Automatically remember running applications when logging out _Continue _Log Out _Log out _Name: _New Session _Remember Currently Running Applications _Remove Session Project-Id-Version: gnome-session 3.1.92
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-09-17 19:40+0800
Last-Translator: Chao-Hsiung Liao <j_h_liau@yahoo.com.tw>
Language-Team: Chinese (Hong Kong) <community@linuxhall.org>
Language: zh_HK
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 1.5.5
  - GNOME ä½æ¥­éæ®µç®¡çå¡ %s [OPTION...] COMMAND

æå¶æäºä½æ¥­éæ®µåè½æå·è¡ COMMAND ã

  -h, --help        é¡¯ç¤ºéåæ±å©æä»¶
  --version         é¡¯ç¤ºç¨å¼çæ¬
  --app-id ID       æå¶æè¦ä½¿ç¨ç
                    æç¨ç¨å¼ id (é¸ææ§)
  --reason REASON   æå¶çåå  (é¸ææ§)
  --inhibit ARG     è¦æå¶çæ±è¥¿ï¼ä»¥åèåéçæ¸å®ï¼
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    ä¸è¦å·è¡ COMMAND èä»¥ç­å¾ä¾åä»£

å¦ææ²ææå® --inhibit é¸é ï¼é è¨­çº idleã
 %s éè¦å¼æ¸
 æåé¡ç¢çä¸ç³»çµ±ä¸è½æ¢å¾©ã
è«ç»åºå¾åè©¦ä¸æ¬¡ã æåé¡ç¢çä¸ç³»çµ±ä¸è½æ¢å¾©ãçºäºé é²å·²ååç¨ææçæ´ååè½ã æåé¡ç¢çä¸ç³»çµ±ä¸è½æ¢å¾©ãè«è¯çµ¡ç³»çµ±ç®¡çè å·²å­å¨åçºã%sãçä½æ¥­éæ®µ AUTOSTART_DIR æ°å¢åå§ååç¨å¼ åå§ååæé¡å¤å·è¡ä»¥ä¸çç¨å¼(_P): åè¨±ç»åº æç¨ç¨å¼ä¸æ¥åä»¥å½ä»¤åéåæä»¶ çè¦½â¦ ä¸è½å³éæä»¶ URI è³ãType=Linkãæ¡é¢é ç® é¸æå¨ä½ ç»å¥æè¦åååªäºæç¨ç¨å¼ æä»¤(_M): è¨»è§£(_E): ç¡æ³é£æ¥ä½æ¥­éæ®µç¸½ç®¡ ç¡æ³å»ºç« ICE ç£è½ææ§½ï¼%s ç¡æ³é¡¯ç¤ºæ±å©æä»¶ èªé¸ èªé¸ä½æ¥­éæ®µ åç¨å°ä½æ¥­éæ®µç®¡çç¨å¼çé£ç· ä¸è¦è¼å¥ä½¿ç¨èæå®çæç¨ç¨å¼ ç¡éä½¿ç¨èç¢ºèª ä¿®æ¹åå§ååç¨å¼ åç¨é¤é¯ç¢¼ å·²åç¨ æªæ¡ ç¡æ³å·è¡ %s
 æªæ¡ä¸æ¯ææç .desktop æªæ¡ GNOME GNOME dummy GNOME æ¼ Wayland ID åç¤º å¿½ç¥ä»»ä½ç¾æçéå¶å ç´  ç»åº æ²ææè¿° æ²æåç¨± ä¸æ¯å¯ä»¥ååçé ç® æ²æåæ ç³ç³ï¼æäºå°æ¹åºåé¡äºã é¸é  è¦èæ¨æºçèªåååç®é è«é¸æè¦å·è¡çèªé¸ä½æ¥­éæ®µ ééé»æº ç¨å¼ ä»¥ç¸è¡çªçé¸é å¼å«ç¨å¼ éæ°éæ© å çºç®åæ­¤ä½æ¥­éæ®µå³å°ééé»è¦ï¼æçµæ°çå®¢æ¶ç«¯é£ç·
 è¨ä½çæç¨ç¨å¼ éæ°å½åä½æ¥­éæ®µ(_M) SESSION_NAME é¸ææä»¤ ä½æ¥­éæ®µ %d ä½æ¥­éæ®µç®¡çé¸é ï¼ ä½æ¥­éæ®µåç¨±ä¸åè¨±åå« â/âå­ç¬¦ ä½æ¥­éæ®µåç¨±ä¸åè¨±ä»¥ â.â éé ­ ä½æ¥­éæ®µåç¨±ä¸åè¨±ä»¥ â.â éé ­æåå« â/âå­ç¬¦ è¦ä½¿ç¨çä½æ¥­éæ®µ é¡¯ç¤ºæ´ååè½è­¦å é¡¯ç¤ºä½æ¥­éæ®µç®¡çé¸é  é¡¯ç¤ºå´éé¯èª¤å°è©±çä»¥ä¾æ¸¬è©¦ æå®å«æå·²å²å­çµæçæªæ¡ æå®ä½æ¥­éæ®µç®¡ç ID æºååå %s åå§ååç¨å¼ åå§ååç¨å¼åå¥½è¨­å® åå§ååç¨å¼ åå§ååæä»¤ä¸å¯ä»¥çç©º åå§ååæä»¤ç¡æ éåé ç®è®ä½ é¸æå²å­çä½æ¥­éæ®µ éåç¨å¼æ­£å¨é»æç»åºã éåä½æ¥­éæ®µè®ä½ ç»å¥ GNOME éåä½æ¥­éæ®µè®ä½ ç»å¥ GNOMEï¼ä½¿ç¨ Wayland ç¡æ³è¾¨è­çæ¡é¢æªæ¡çæ¬ã%sã ç¡æ³è¾¨è­çååé¸é ï¼%d éåç¨å¼ççæ¬ ç»åºæèªåè¨ä½éå¨å·è¡ä¸­çæç¨ç¨å¼(_A) ç¹¼çº(_C) ç»åº(_L) ç»åº(_L) åç¨±(_N): æ°å¢ä½æ¥­éæ®µ(_N) è¨ä½ç®åå·è¡çæç¨ç¨å¼(_R) ç§»é¤ä½æ¥­éæ®µ(_R) 