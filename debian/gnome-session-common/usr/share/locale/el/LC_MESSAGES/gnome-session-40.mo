��    D      <  a   \      �     �  p        q  T   �  i   �  \   I	  '   �	     �	     �	  (   �	  )   
     <
     C
  #   R
  '   v
  $   �
     �
     �
     �
     �
       q     G   �  >   �           )     1     ?  !   N  '   p  %   �  	   �  '   �  #   �       P        l     �  %   �  M   �          "     /     =  
   O  ;   Z  3   �  Q   �          +  &   B     i  +   �  #   �  P   �  *   $      O      p  B   �     �     �  p     	   �     �     �     �     �  �  �  @   �  �  �  +   _  �   �  �   P  �   6  D        [  0   i  U   �  Q   �     B  +   ]  O   �  i   �  J   C  @   �  .   �     �            �   +  �   !  �   �  K   E      �   +   �   #   �   ,   �   Z   #!  {   ~!     �!  W   "  U   i"     �"  �   �"  -   b#  *   �#  S   �#  �   $  2   �$     �$  '   �$  '   %     B%  ~   V%  l   �%  �   B&  "   �&  >   '  G   G'  Q   �'  Q   �'  5   3(  z   i(  �   �(  P   e)  ?   �)  �   �)  :   �*  4   �*    �*     ,     ,     3,     I,  $   b,         !                    #       *   9   6   4           0   )              8       (      '   $   %           >                     	          -          A   D   ,                                        B              ?   ;              :      
       +   .   3          /   5   1           2   =          @      "      C                7   &      <        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: el
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2020-09-30 23:04+0300
Last-Translator: Efstathios Iosifidis <eiosifidis@gnome.org>
Language-Team: Greek, Modern (1453-) <gnome-el-list@gnome.org>
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
X-Project-Style: gnome
  — ο διαχειριστής συνεδρίας του GNOME %s [OPTION...] COMMAND

Εκτέλεση COMMAND ενώ παρεμποδίζεται κάποια λειτουργικότητα συνεδρίας.

  -h, --help        Προβολή αυτής της βοήθειας
  --version         Προβολή έκδοσης προγράμματος
  --app-id ID       Το χρησιμοποιούμενο αναγνωριστικό της εφαρμογής
                    όταν εμποδίζεται (προαιρετικό)
  --reason REASON   Ο λόγος παρεμπόδισης (προαιρετικός)
  --inhibit ARG     Τα πράγματα για παρεμπόδιση, λίστα που χωρίζεται με : για τα
        :            αποσύνδεση, εναλλαγή χρήστη, αναστολή, αδράνεια, αυτόματη προσάρτηση
  --inhibit-only    Να μην ξεκινά η COMMAND και να περιμένετε για πάντα
  -l, --list        Εμφάνιση των υπαρχόντων παρεμποδίσεων, και έξοδος

Εάν δεν έχει οριστεί καμιά επιλογή --inhibit, υποτίθεται αδράνεια.
 Το %s απαιτεί ένα όρισμα
 Προέκυψε ένα πρόβλημα και το σύστημα δε μπορεί να ανακάμψει.
Παρακαλούμε αποσυνδεθείτε και δοκιμάστε ξανά. Προέκυψε ένα πρόβλημα και το σύστημα δε μπορεί να ανακάμψει. Έχουν απενεργοποιηθεί όλες οι επεκτάσεις για λόγους προφύλαξης. Προέκυψε ένα πρόβλημα και το σύστημα δε μπορεί να ανακάμψει. Παρακαλούμε επικοινωνήστε με τον διαχειριστή του συστήματος Υπάρχει ήδη μια συνεδρία με όνομα «%s» AUTOSTART_DIR Να επιτρέπεται αποσύνδεση Αδυναμία σύνδεσης με το διαχειριστή συνεδρίας Αδυναμία δημιουργίας υποδοχής ακρόασης ICE: %s Προσαρμοσμένο Προσαρμοσμένη συνεδρία Απενεργοποίηση ελέγχου επιτάχυνσης υλικού Να μην φορτώνονται εφαρμογές καθορισμένες από τον χρήστη Να μη ζητείται επιβεβαίωση από το χρήστη Ενεργοποίηση κώδικα αποφαλμάτωσης Αποτυχία εκτέλεσης του %s
 GNOME Εικονικό GNOME GNOME σε Xorg Αν ενεργοποιηθεί, η συνεδρία του gnome θα εμφανίσει έναν διάλογο προειδοποίησης μετά την είσοδο αν χρησιμοποιήθηκε η εφεδρική συνεδρία. Αν ενεργοποιηθεί, η συνεδρία του gnome θα ειδοποιεί το χρήστη πριν το τέλος της συνεδρίας. Αν ενεργοποιηθεί, η συνεδρία του gnome θα αποθηκεύει αυτόματα τη συνεδρία. Αγνοώντας όποιους υπάρχοντες αναστολείς Αποσύνδεση Ειδοποίηση αποσύνδεσης Δεν ανταποκρίνεται Ωχ όχι! Κάτι πήγε στραβά. Παράκαμψη τυπικών καταλόγων αυτόματης εκκίνησης Παρακαλούμε επιλέξτε μια προσαρμοσμένη συνεδρία για να εκτελεστεί Τερματισμός Το πρόγραμμα κλήθηκε με συγκρουόμενες επιλογές Το πρόγραμμα χρειάζεται ακριβώς μία παράμετρο Επανεκκίνηση Άρνηση σύνδεσης νέου πελάτη επειδή το σύστημα τερματίζεται αυτή τη στιγμή
 Απομνημόνευση εφαρμογών Με_τονομασία συνεδρίας Επανεκκίνηση της υπηρεσίας dbus εάν εκτελείται Εκτέλεση από το ExecStopPost για εκκίνηση του gnome-session-failed.target σε περίπτωση αποτυχίας υπηρεσίας Εκτελείται ως υπηρεσία systemd SESSION_NAME Αποθήκευση συνεδρίων Αποθήκευση συνεδρίας Συνεδρία %d Τα ονόματα της συνεδρίας δεν επιτρέπεται να περιέχουν χαρακτήρες «/» Τα ονόματα της συνεδρίας δεν επιτρέπεται να ξεκινούν με «.» Τα ονόματα της συνεδρίας δεν επιτρέπεται να ξεκινάνε με «.» ή να περιέχουν χαρακτήρες «/» Συνεδρία για χρήση Προβολή προειδοποίησης επέκτασης Εμφάνιση του διαλόγου fail whale για δοκιμή Προβολή προειδοποίησης εφεδρικής συνεδρίας Η αρχικοποίηση του σήματος έγινε στο gnome-session Εκκίνηση του gnome-session-shutdown.target Εκκίνηση του gnome-session-shutdown.target όταν λαμβάνετε EOF ή ένα μόνο byte στο stdin Αυτή η καταχώριση επιτρέπει την επιλογή μιας αποθηκευμένης συνεδρίας Αυτό το πρόγραμμα εμποδίζει την αποσύνδεση. Αυτή η συνεδρία σας συνδέει στο GNOME Χρήση της ενσωματωμένης διαχείρισης συνεδρίας (και όχι την βασισμένη στο systemd) Χρήση διαχείριση συνεδρίας systemd Η έκδοση αυτής της εφαρμογής Αν ενεργοποιηθεί, η συνεδρία του gnome θα αποθηκεύει αυτόματα την επόμενη συνεδρία στην έξοδο ακόμη και αν η αυτόματη αποθήκευση είναι απενεργοποιημένη. _Συνέχεια Απο_σύνδεση _Αποσύνδεση _Νέα συνεδρία _Αφαίρεση συνεδρίας 