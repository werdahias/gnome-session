��    D      <  a   \      �     �  p        q  T   �  i   �  \   I	  '   �	     �	     �	  (   �	  )   
     <
     C
  #   R
  '   v
  $   �
     �
     �
     �
     �
       q     G   �  >   �           )     1     ?  !   N  '   p  %   �  	   �  '   �  #   �       P        l     �  %   �  M   �          "     /     =  
   O  ;   Z  3   �  Q   �          +  &   B     i  +   �  #   �  P   �  *   $      O      p  B   �     �     �  p     	   �     �     �     �     �  �  �  !   j  �  �     "  ^   @     �  k     '   �     �     �  .   �  ,        .     <  "   R  0   u     �     �     �     �     �     �  |     L   �  @   �          6     ;     S      `  ;   �  8   �     �  &   �  .   #     R  F   ^     �     �  .   �  [      $   \     �     �     �  
   �  2   �  -   �  F        d     v  -   �     �  1   �  )   	  \   3  +   �  (   �  *   �  K     '   \     �  �   �  	   H      R      W      \      l          !                    #       *   9   6   4           0   )              8       (      '   $   %           >                     	          -          A   D   ,                                        B              ?   ;              :      
       +   .   3          /   5   1           2   =          @      "      C                7   &      <        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session 2.20.x
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2020-08-28 12:34+0200
Last-Translator: Fabio Tomat <f.t.public@gmail.com>
Language-Team: Friulian <massimo.furlani@libero.it>
Language: fur
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.4.1
  — Il gjestôr di session GNOME %s [OPZION...] COMANT

Eseguìs COMANT intant che si inibìs cualchi funzionalitât di session.

 -h, --help        Mostre chest jutori
  --version         Mostre version dal program
  --app-id ID       Il ID de aplicazion di doprâ
                    cuant che si inibìs (opzionâl)
  --reason REASON   La reson par inibî (opzionâl)
  --inhibit ARG     Liste, separade di virgulis, di elements di inibî:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    No sta eseguî COMANT, invezit spietâ par simpri
 -l, --list        Liste lis inibizions esistentis e jes

Se --inhibit nol à nissune opzion, al ven doprât idle.
 %s al scugne vê un argoment
 Al è vignût fûr un probleme e il sisteme nol pues comedâsi. 
Par plasê jes e torne prove. Al è vignût fûr un probleme e il sisteme nol pues comedâsi. Dutis lis estensions a son stadis disabilitadis par precauzion. Al è vignût fûr un probleme e il sisteme nol pues comedâsi. Par plasê clame l'aministradôr di sisteme Une session clamade “%s” e esist za AUTOSTART_DIR Permet di jessî Impussibil colegasi al ministradôr di session Impussibil creâ il socket di scolte ICE: %s Personalizade Session personalizade Gjave control acelerazion hardware Nol cjame lis aplicazions specificadis dal utent No sta domandâ conferme Abilite codiç di debug Falît a eseguî %s
 GNOME GNOME dummy GNOME su Xorg Se abilitât, gnome-sessions al mostrarà un dialic di avîs daspò l'acès se la session e je stade rapeçade in automatic. Se abilitât, gnome-session al domandarà conferme prin di finî la session. Se abilitât, gnome-session al salvarà la session in automatic. Ignore ogni inibidôr esistent _Jes Conferme fin da session Nol rispuint Orpo! Alc al è lât par stuart. No'l ten cont des directory standard di inviament automatic Par plasê selezione une session personalizade di inviâ Stude Program clamât cun opzions in conflit Il program al à bisugne di juste un parametri Torne invie Refudi le gnove conession client parcè che la session si sta studant
 Aplicazion memorizade Cambie Non ae Session Torne invie dbus.service se al è in esecuzion Eseguìs di ExecStopPost par fâ partî gnome-session-failed.target al faliment dal servizi In esecuzion come servizi di systemd NON_SESSION Salve sessions Salve cheste session Session %d I nons di session no puedin vê il caratar “/” I nons di session no puedin tacâ cun “.” I nons di session no puedin tacâ cun “.” o vê il caratar “/” Session di doprâ Mostre avîs di estension Mostre il dialic "fail whale" par fâ un test Mostre l'avîs di rapeç Inizializazion dal segnâl fate par gnome-session Fâs partî gnome-session-shutdown.target Fâs partî gnome-session-shutdown.target cuant che si ricêf EOF o un singul byte sul stdin Ti permet di selezionâ une session salvade Chest program al sta blocant la jessude. Cheste session ti fasarà jentrâ in GNOME Dopre la gjestion de session integrade (invezit che chel basât su systemd) Dopre la gjestion de session di systemd Version di cheste aplicazion Cuant che al è abilitât, gnome-session al salvarà in automatic la sucessive session tal moment che si jessarà, ancje se il salvament automatic al è disabilitât. _Indenant _Jes _Jes G_nove Sessions _Gjave sessions 