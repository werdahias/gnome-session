��    3      �  G   L      h     i  4  �     �  T   �  i   +  \   �  '   �          (  (   5  )   ^     �     �  #   �  '   �  $   �     	     %	     ;	     A	     M	      [	     |	     �	  !   �	  '   �	  %   �	  	   
  '   
     5
  P   <
     �
     �
     �
  
   �
  ;   �
  3     Q   <     �     �  &   �  *   �            '     H  	   d     n     w     �     �  Z  �  8   �  �  1  #     �   :  �   �  �   �  :   8  ;   s  (   �  c   �  X   <  
   �     �  Y   �  ^     !   o  B   �  0   �               &  A   4  
   v     �  %   �  �   �  H   A     �  Z   �     �  �     #   �  )   �     �       [     b   y  �   �  (   o  N   �  \   �  Q   D  >   �     �  /   �          7     C     O  #   d                                    	   1      $   2                         *              0   -            "   %   &      3           !   /             #                   
          +                                         .   '   (             )   ,     — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg Ignoring any existing inhibitors Log out Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Version of this application _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session master
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-09-09 09:15+0300
Last-Translator: Lyubomir Vasilev <lyubomirv@abv.bg>
Language-Team: Bulgarian <dict@fsa-bg.org>
Language: bg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
  — управление на сесиите на GNOME %s [ОПЦИЯ…] КОМАНДА

Изпълняване на КОМАНДАта с ограничена функционалност на сесията.

  -h, --help        Извеждане на тази помощ
  --version         Извеждане на версията на програмата
  --app-id ИД       ИДентификатор на програмата стартирана с ограничена
                    сесия (незадължително)
  --reason ПРИЧИНА  ПРИЧИНАта за ограничаване (незадължително)
  --inhibit АРГ     Какво да се ограничи, списък разделен с двоеточие.
                    Възможните стойности са logout (изход), switch-user (смяна
                    на потребител), suspend (приспиване), idle (бездействие),
                    automount (автоматично монтиране)
  --inhibit-only    Команда не се изпълнява, стартира се направо сесия

Ако не е указана опция „--inhibit“, за стандартна се ползва стойност „idle“.
 %s изисква аргумент
 Възникна проблем и системата не може да се справи.
Излезте от системата и опитайте отново. Възникна проблем и системата не може да се справи. За всеки случай всички разширения са изключени. Възникна проблем и системата не може да се справи. Свържете се със системен администратор Вече има сесия с това име — „%s“ ПАПКА_ЗА_АВТОМАТИЧНО_СТАРТИРАНЕ Разрешаване на изхода Не може да се изгради връзка към управлението на сесии Неуспешно създаване на гнездо на ICE за слушане: %s Други Друга сесия Изключване на проверката за хардуерно ускорение Да не се зареждат програми, зададени от потребителя Без потвърждаване Включване на изчистването на грешки Неуспешно изпълнение на %s
 GNOME GNOME — фиктивна GNOME с Xorg Изход въпреки блокиращите програми Изход Не отговаря О, не! Нещо се обърка. Използване на различни от стандартните папки за стартиране при влизане Изберете коя друга сесия да се стартира Изключване Програмата е стартирана с противоречащи си опции Рестартиране Отхвърляне на свързванията с нови клиенти, защото тази сесия в момента се затваря
 Запомнена програма Пре_именуване на сесия ИМЕ_НА_СЕСИЯ Сесия %d Имената на сесиите не могат да съдържат знака „/“ Имената на сесиите не могат за започват със знака „.“ Имената на сесиите не могат за започват със знака „.“ или да съдържат знака „/“ Коя сесия да се ползва Показване на предупреждение за разширение Показване на черния екран за грешка с тестови цели Оттук може да изберете сесия запазена от вас Тази програма блокира излизането. Влизане в GNOME Версията на тази програма _Продължаване _Изход _Изход _Нова сесия Из_триване на сесия 