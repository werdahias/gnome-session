��    D      <  a   \      �     �  p        q  T   �  i   �  \   I	  '   �	     �	     �	  (   �	  )   
     <
     C
  #   R
  '   v
  $   �
     �
     �
     �
     �
       q     G   �  >   �           )     1     ?  !   N  '   p  %   �  	   �  '   �  #   �       P        l     �  %   �  M   �          "     /     =  
   O  ;   Z  3   �  Q   �          +  &   B     i  +   �  #   �  P   �  *   $      O      p  B   �     �     �  p     	   �     �     �     �     �  �  �     �  �  �     c  g   z  {   �  j   ^  )   �     �       )     /   D  
   t       -   �  -   �  $   �          0     I     O     \  o   j  I   �  :   $  "   _     �     �     �      �  4   �  2        ;  0   D  '   u     �  ?   �     �     �  -     \   ?     �     �     �     �  	   �  ;   �  :   ;  O   v     �  &   �  $     #   &  2   J  %   }  N   �  2   �  "   %  %   H  D   n  $   �     �  �   �  
   l      w      �      �      �          !                    #       *   9   6   4           0   )              8       (      '   $   %           >                     	          -          A   D   ,                                        B              ?   ;              :      
       +   .   3          /   5   1           2   =          @      "      C                7   &      <        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: lv
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2020-09-12 12:02+0300
Last-Translator: Rūdolfs Mazurs <rudolfs.mazurs@gmail.com>
Language-Team: Latvian <lata-l10n@googlegroups.com>
Language: lv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2);
X-Generator: Lokalize 19.12.3
X-Launchpad-Export-Date: 2009-02-19 16:24+0000
  — GNOME sesiju pārvaldnieks %s [OPCIJA…] KOMANDA

Izpilda KOMANDU kamēr aiztur dažas sesijas funkcijas.

  -h, --help        Rādīt šo palīdzību
  --version         Rādīt programmas versiju
  --app-id ID       Lietotnes id, ko izmantot
                    viecot aizturēšanu (neobligāti)
  --reason IEMESLS  Aizturēšanas iemesls (neobligāti)
  --inhibit PARAM   Lietas, ko aizturēt, ar kolu atdalīts saraksts:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Nepalaist KOMANDU un tā vietā gaidīt bezgalīgi
  -l, --list        Uzskaitīt esošās aiztures un iziet

Ja nav norādīta opcija --inhibit, tiek pieņemts idle.
 %s pieprasa parametru
 Gadījās problēma, un sistēma no tās nevar atgūties.
Lūdzu, izrakstieties un mēģiniet vēlreiz. Gadījās problēma, un sistēma no tās nevar atgūties. Kā piesardzības pasākums, visi paplašinājumi ir deaktivēti. Gadījās problēma, un sistēma no tās nevar atgūties. Lūdzu, sazinieties ar sistēmas administratoru. Sesija ar nosaukumu “%s” jau eksistē AUTOSTARTA_DIR Atļaut izrakstīšanos Nevar savienoties ar sesiju pārvaldnieku Nevar izveidot ICE klausīšanās ligzdu — %s Pielāgota Pielāgota sesija Izslēgt aparatūras paātrinājuma pārbaudi Neielādēt lietotāja norādītās lietotnes Neprasīt lietotāja apstiprinājumu Aktivēt atkļūdošanas kodu Neizdevās izpildīt %s
 GNOME GNOME makets GNOME ar Xorg Ja ieslēgts, gnome-session pēc ierakstīšanās rādīs brīdinājumu, ja sesija ir automātiski atkāpusies. Ja ieslēgts, gnome-session vaicās lietotājam pirms sesijas beigšanas. Ja ieslēgts, gnome-session automātiski saglabās sesiju. Ignorējot jebkādus šķēršļus Izrakstīties Izrakstīšanās uzvedne Neatbild Ak nē! Kaut kas nogāja greizi. Aizstāt standarta automātiskā starta direktorijas Lūdzu, izvēlieties pielāgotu sesiju, ko palaist Izslēgt Programma izsaukta ar konfliktējošām opcijām Programmai vajag tieši vienu parametru Pārstartēt Atsaka jauniem klientu savienojumiem, jo sesija tiek izslēgta
 Iegaumētā lietotne _Pārsaukt sesiju Pārstartēt dbus.service, ja tas ir palaists Palaidiet no ExecStopPost, lai startētu gnome-session-failed.target, kad ir servisa kļūme Darbina kā systemd pakalpojumu SESIJAS_NOSAUKUMS Saglabāt sesijas Saglabāt šo sesiju Sesija %d Sesijas nosaukums nedrīkst vai saturēt rakstzīmi “/” Sesijas nosaukums nedrīkst sākties ar rakstzīmi “.” Sesijas nosaukums nedrīkst sākties ar rakstzīmi “.” vai saturēt “/” Sesija, ko izmantot Rādīt paplašinājumu brīdinājumus Rādīt neveiksmju vali testēšanai Rādīt atkāpšanās brīdinājumu Signāla inicializācija tika veikta gnome-session Palaist gnome-session-shutdown.target Palaist gnome-session-shutdown.target, kad saņem EOF vai vienu baitu no stdin Šis ieraksts ļauj izvēlēties saglabātu sesiju Programma bloķē izrakstīšanos. Šī sesija ieraksta jūs GNOME vidē Izmantot iebūvēto sesiju pārvaldību (nevis systemd pārbaldību) Izmantot systemd sesiju pārvaldību Lietotnes versija Kad ieslēgts, gnome-session izrakstoties automātiski saglabās nākamo sesiju, pat ja automātiskā saglabāšana ir izslēgta. _Turpināt _Izrakstīties _Izrakstīties Jau_na sesija _Izņemt sesiju 