��    <      �  S   �      (     )  4  F     {  R   �  g   �  Z   O  '   �     �     �     �  	   	  	   	  	   	  (   )	  )   R	     |	     �	  '   �	  "   �	     �	     �	     	
     
     
      ,
     M
     U
  !   d
     �
  '   �
  %   �
  	   �
  '   �
       P        f     }     �  
   �  ;   �  3   �  Q        g     v  &   �     �  *   �      �        /   2     b  =   ~  	   �     �     �     �     �  (   �       �  %  4   �  L  )  I   v  �   �  �   �  �   �  m   ^     �  J   �  2   %     X     k     ~  U   �  Q   �     B  "   X  �   {  d      K   e  /   �     �     �  !   �  v        �  ,   �  2   �       w   #  n   �     
  j        �  �   �  M   d  =   �     �     �  f     R   t  �   �  1   �   D   �   K   !  .   P!  �   !  l   "  X   r"  �   �"  A   `#  �   �#     E$     d$     �$     �$     �$  y   �$  .   K%     $             3       0                  4                    -       	           7          1   )           #   *      (                 "                &   +      /         5   6            9   ;         <       2           '   !           ,             :   .   %       
                   8        - the GNOME session manager %s [OPTION...] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can't recover.
Please log out and try again. A problem has occurred and the system can't recover. All extensions have been disabled as a precaution. A problem has occurred and the system can't recover. Please contact a system administrator A session named ‘%s’ already exists AUTOSTART_DIR Additional startup _programs: Allow logout Browse… Co_mmand: Comm_ent: Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Do not load user-specified applications Don't prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Wayland Ignoring any existing inhibitors Log out Not responding Oh no!  Something has gone wrong. Options Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Session %d Session names are not allowed to contain ‘/’ characters Session names are not allowed to start with ‘.’ Session names are not allowed to start with ‘.’ or contain ‘/’ characters Session to use Show extension warning Show the fail whale dialog for testing Startup Programs This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME This session logs you into GNOME, using Wayland Version of this application _Automatically remember running applications when logging out _Continue _Log Out _Log out _Name: _New Session _Remember Currently Running Applications _Remove Session Project-Id-Version: mr
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-session&keywords=I18N+L10N&component=general
PO-Revision-Date: 2014-09-12 09:43+0530
Last-Translator: Sandeep Shedmake <sshedmak@redhat.com>
Language-Team: Marathi <fedora-trans-mr@redhat.com>
Language: mr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n!=1);
  - GNOME सत्र व्यवस्थापक %s [OPTION...] COMMAND

काही ठराविक सत्र कामगिरी थांबवतेवेळी COMMAND चालवा.

  -h, --help        ही मदत दाखवा
  --version         प्रोग्राम आवृत्ती दाखवा
  --app-id ID       रोखतेवेळी वापरण्याजोगी ॲप्लिकेशन id
                    (वैकल्पिक)
  --reason REASON   रोखण्यासाठीचे कारण (वैकल्पिक)
  --inhibit ARG     रोखण्याजोगी बाबी, स्वल्पविराम-विभाजीत सूची
                    लॉगआउट, स्वीच-युजर, सस्पेंड, आयडल, ऑटोमाउंट
  --inhibit-only    COMMAND सुरू करू नका व त्याऐवजी नेहमीसाठी थांबा

--inhibit पर्याय निर्देशीत न केल्यास, आयडल गृहीत घेतले जाते.
 %s ला आर्ग्युमेंट आवश्यक आहे
 अडचण आढळली व प्रणालीची पुनःप्राप्ति अशक्य. 
कृपया बाहेर पडा व पुनः प्रयत्न करा. अडचण आढळली व प्रणालीची पुनःप्राप्ति अशक्य. सर्व एक्सटेंशन्स्ला सावधगिरी म्हणून बंद केले आहे. अडचण आढळली व प्रणालीची पुनःप्राप्ति अशक्य. कृपया प्रणाली प्रशासकाशी संपर्क करा ‘%s’ नावाचे सत्र आधिपासूनच अस्तित्वात आहे AUTOSTART_DIR अगाऊ प्रारंभिक कार्यक्रम (_p): बाहेर पडा स्वीकारा तपासा... आदेश (_m): टिपण्णी (_e): सत्र संचालकाशी संपर्क झाला नाही ICE सक्रीय सॉकेट बनवू शकला नाही: %s पसंतीचे पसंतीचे सत्र वापरकर्ता-निर्देशीत अनुप्रयोग दाखल करू शकले नाही वापरकर्ता खात्रीकरीता विनंती करू नका डीबगींग कोड कार्यान्वीत करा %s चालवण्यास अपयशी
 GNOME GNOME डम्मी वेलँडवरील GNOME अस्तित्वाती इंहिबिटर्सकडे दुर्लक्ष करत आहे बाहेर पडा प्रतिसाद देत नाह अरे!  काहितरी चुकले. पर्याय मानक स्वप्रारंभ संचयीकावर खोडून पुन्हा लिहा कृपया चालवण्याजोगी पसंतीचे सत्र पसंत करा बंद करा मतभेदीय पर्यायसह कार्यक्रमाला कॉल केले पुनःबूट करा नविन क्लाऐंट जुळवणी नकारत आहे कारण सत्र वर्तमानक्षणी बंद केले जात आहे
 लक्षात ठेवलेले ॲप्लिकेशन्स् सत्राला पुनःनाव द्या (_m) SESSION_NAME सत्र %d सत्र नावात ‘/’ अक्षरे समाविष्टीत नसावे सत्र नाव ‘.’ पासून सुरू होत नाही सत्र नाव ‘.’ पासून सुरू होत नाही किंवा त्यामध्ये ‘/’ अक्षरे समाविष्टीत नाही वापरण्याजोगी सत्र एक्सटेंशन सावधानता दाखवा चाचणीवेळी अपयशी संवाद दाखवा आरंभिक कार्यक्रम हि नोंदणी तुम्हाला साठवलेले सत्र पसंत करायला देते हा प्रोग्राम बाहेर पडण्यापासून रोखत आहे. GNOME मध्ये दाखल करण्याजोगी सत्र लॉग हे सत्र तुम्हाला GNOME मध्ये दाखल करते, वेलँडचा वापर करत आहे या अनुप्रयोगाची आवृत्ती बाहेर पडतेवेळी आपोआप अनुप्रयोग कार्यरत करणे स्मर्णात ठेवा (_A) सुरू ठेवा (_C) बाहेर पडा (_L) बाहेर पडा (_L) नाव (_N): नवीन सत्र (_N) सध्या सुरू असलेले ॲप्लिकेशन्स् लक्षात ठेवा (_R) सत्र काढून टाका (_R) 