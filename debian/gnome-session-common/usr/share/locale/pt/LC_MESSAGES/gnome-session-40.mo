��    D      <  a   \      �     �  p        q  T   �  i   �  \   I	  '   �	     �	     �	  (   �	  )   
     <
     C
  #   R
  '   v
  $   �
     �
     �
     �
     �
       q     G   �  >   �           )     1     ?  !   N  '   p  %   �  	   �  '   �  #   �       P        l     �  %   �  M   �          "     /     =  
   O  ;   Z  3   �  Q   �          +  &   B     i  +   �  #   �  P   �  *   $      O      p  B   �     �     �  p     	   �     �     �     �     �  �  �  "   �  �  �     L  h   d  p   �  h   >  %   �     �     �  .     1   5     g     u  5   �  ;   �  *   �     (     H     ^     d     s  }   �  T   �  B   T  )   �     �     �     �       6     <   V     �  ,   �  .   �  	   �  Z        ]     t  1   �  d   �  "        @     O     `  
   u  E   �  <   �  U        Y     l  +   �     �  8   �  '     U   /  7   �  ;   �     �  F      &   Y      �   �   �   
   )!     4!     F!     X!     f!         !                    #       *   9   6   4           0   )              8       (      '   $   %           >                     	          -          A   D   ,                                        B              ?   ;              :      
       +   .   3          /   5   1           2   =          @      "      C                7   &      <        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: 3.18
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2020-09-06 16:42-0300
Last-Translator: Juliano Camargo <julianosc@pm.me>
Language-Team: Portuguese <>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1)
X-Generator: Gtranslator 3.36.0
X-Project-Style: gnome
X-Language: pt_PT
X-Source-Language: C
  — o gestor de sessões do GNOME %s [OPÇÃO...] COMANDO

Executar 'COMANDO' enquanto inibe alguma funcionalidade da sessão.

  -h, --help        Mostrar esta ajuda
  --version         Mostrar a versão do programa
  --app-id ID       A id de aplicação a utilizar
                    quando inibir (opcional)
  --reason REASON   O motivo para inibir (opcional)
  --inhibit ARG     Funcionalidades a inibir, lista separada por vírgulas de:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Não iniciar 'COMANDO' e esperar indefinidamente
  -l, --list        Listar as inibições existentes e sair

Se não for especificada nenhuma opção --inhibit, é assumida idle.
 %s requer um argumento
 Ocorreu um problema e o sistema não consegue recuperar.
Por favor, termine a sessão e tente novamente. Ocorreu um problema e o sistema não consegue recuperar. Por precaução, foram desativadas todas as extensões. Ocorreu um problema e o sistema não consegue recuperar. Por favor, contacte um administrador do sistema Já existe uma sessão designada "%s" DIRETÓRIO_INICIOAUTOMÁTICO Permitir terminar a sessão Não foi possível ligar ao gestor de sessões Não foi possível criar socket de escuta ICE: %s Personalizado Sessão Personalizada Desativar a verificação de aceleração de hardware Não carregar as aplicações especificadas pelo utilizador Não solicitar confirmação ao utilizador Ativar o código de depuração Falha ao executar %s
 GNOME GNOME de teste GNOME em Xorg Se ativado e se a sessão foi recuperada automaticamente, o "gnome-session" irá exibir uma janela de aviso depois de entrar. Se ativo, o "gnome-session" questionará o utilizador antes de terminar uma sessão. Se ativo, o "gnome-session" irá gravar a sessão automaticamente. A ignorar quaisquer inibidores existentes Terminar sessão Perguntar ao terminar sessão Não está a responder Ocorreu algo de errado. Ignorar os diretórios de arranque automático padrão Por favor, selecione uma sessão personalizada para executar Desligar O programa invocado com conflito de opções O programa precisa exatamente de um parâmetro Reiniciar A recusar uma nova ligação de cliente porque a sessão está atualmente a ser terminada
 Aplicação Memorizada Renomear _Sessão Reiniciar "dbus.service" se estiver em execução Na falha do serviço, executar a partir de ExecStopPost para iniciar o "gnome-session-failed.target" A executar como serviço "systemd" NOME_DA_SESSAO Guardar sessões Guardar esta sessão Sessão %d Não são permitidos os nomes de sessão que contenham o caráter "/" Não são permitidos os nomes de sessão que comecem por "." Não são permitidos nomes de sessão que comecem por "." ou contenham o caráter "/" Sessão a utilizar Mostrar aviso de extensão Mostrar a janela de falha total para testar Mostrar aviso de extensão Inicialização de sinal efetuada para a "gnome-session" Iniciar "gnome-session-shutdown.target" Iniciar "gnome-session-shutdown.target" quando receber EOF ou um único byte em stdin Esta entrada permite-lhe selecionar uma sessão gravada Esta aplicação está a impedir o encerramento da sessão. Esta sessão usa o GNOME Utilize a gestão de sessões integrada (em vez da baseada no sistema) Utilizar gestão de sessões "systemd" Versão desta aplicação Se ativado, "gnome-session" irá guardar automaticamente a próxima sessão ao terminar a sessão se o guardar automático estiver desativado. _Continuar _Terminar Sessão _Terminar sessão _Nova Sessão _Remover Sessão 