��    D      <  a   \      �     �  p        q  T   �  i   �  \   I	  '   �	     �	     �	  (   �	  )   
     <
     C
  #   R
  '   v
  $   �
     �
     �
     �
     �
       q     G   �  >   �           )     1     ?  !   N  '   p  %   �  	   �  '   �  #   �       P        l     �  %   �  M   �          "     /     =  
   O  ;   Z  3   �  Q   �          +  &   B     i  +   �  #   �  P   �  *   $      O      p  B   �     �     �  p     	   �     �     �     �     �  �  �  %   n  �  �       �   �  �     u   �  /        O     _  2   q  4   �     �     �  4     ,   ;     h     �     �     �     �     �  y   �  c   `  @   �  &        ,     5     I  #   W  0   {  N   �     �  E     ,   M     z  \   �     �     �  *     Y   2     �     �     �     �  
   �  3   �  4     I   S     �      �  7   �  $     G   2  %   z  `   �  K      (   M   %   v   M   �   $   �      !  �   (!     �!     �!  	   �!     �!     �!         !                    #       *   9   6   4           0   )              8       (      '   $   %           >                     	          -          A   D   ,                                        B              ?   ;              :      
       +   .   3          /   5   1           2   =          @      "      C                7   &      <        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session master
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2020-08-05 05:09+0200
Last-Translator: Christian Kirbach <christian.kirbach@gmail.com>
Language-Team: Deutsch <gnome-de@gnome.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3.1
  – Die Verwaltung der GNOME-Sitzung %s [OPTION …] BEFEHL

BEFEHL ausführen und bestimme Funktionalitäten der Sitzung unterdrücken.

  -h, --help        Diese Hilfe anzeigen
  --version         Die Programmversion anzeigen
  --app-id KENNUNG  Die beim Unterdrücken zu benutzende
                    Programmkennung (optional)
  --reason GRUND    Der Grund für das Unterdrücken (optional)
  --inhibit ARG     Zu unterdrückende Funktionalitäten als durch Doppelpunkte getrennte Liste:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    BEFEHL nicht starten und stattdessen dauerhaft warten
  -l, --list        Mögliche Unterdrückungen auflisten und beenden

Wenn die Option »--inhibit« nicht angegeben ist, so wird »idle« angenommen.
 %s benötigt ein Argument
 Ein Problem ist aufgetreten, welches vom System nicht behoben werden kann.
Bitte melden Sie sich ab und versuchen Sie es erneut. Ein Problem ist aufgetreten, welches vom System nicht behoben werden kann. Als Vorsichtsmaßnahme sind alle Erweiterungen deaktiviert worden. Ein Problem ist aufgetreten, welches vom System nicht behoben werden kann. Bitte kontaktieren Sie den Systemverwalter Eine Sitzung mit Namen »%s« existiert bereits AUTOSTARTORDNER Abmelden erlauben Sitzungsverwaltung konnte nicht kontaktiert werden ICE-Listening-Socket konnte nicht erzeugt werden: %s Benutzerdefiniert Benutzerdefinierte Sitzung Überprüfung des Hardware-Beschleunigers abschalten Keine benutzerspezifischen Anwendungen laden Keine Bestätigung abfragen Debugging-Code aktivieren Fehler beim Ausführen von %s
 GNOME GNOME Platzhalter GNOME unter Xorg Wenn aktiviert, zeigt gnome-session eine Warnung nach der Anmeldung an, dass die Sitzung automatisch zurückgefallen ist. Wenn aktiviert, bittet gnome-session den Benutzer um Bestätigung, bevor eine Sitzung beendet wird. Wenn aktiviert, speichert gnome-session die Sitzung automatisch. Bestehende Unterdrückungen ignorieren Abmelden Abmeldebestätigung Keine Antwort Leider ist ein Problem aufgetreten. Vorgegebene Autostart-Ordner außer Kraft setzen Bitte wählen Sie eine benutzerdefinierte Sitzung, die ausgeführt werden soll Ausschalten Programm wurde mit Optionen aufgerufen, die zu einem Konflikt führen Das Programm erfordert genau einen Parameter Neustart Neue Client-Verbindung wurde abgelehnt, weil die aktuelle Sitzung gegenwärtig beendet wird
 Gemerkte Anwendung Sitzung _umbenennen dbus.service neu starten, sofern es läuft Von ExecStopPost ausführen, um bei Dienstversagen gnome-session-failed.target zu starten Läuft als systemd-Dienst SITZUNGSNAME Sitzungen speichern Diese Sitzung speichern Sitzung %d Sitzungsnamen dürfen keine »/«-Zeichen enthalten Sitzungsnamen dürfen nicht mit einem ».« anfangen Sitzungsnamen dürfen nicht mit einem ».« anfangen oder »/« enthalten Zu verwendende Sitzung Warnung der Erweiterung anzeigen Den Dialog zum Fehlschlag für Prüfungszwecke anzeigen Warnung für Rückfallmodus anzeigen gnome-session signalisieren, dass die Initialisierung abgeschlossen ist gnome-session-shutdown.target starten gnome-session-shutdown.target starten, wenn EOF oder ein einzelnes Byte von stdin empfangen wird Dieser Eintrag ermöglicht es Ihnen, eine gespeicherte Sitzung auszuwählen Dieses Programm blockiert die Abmeldung. Diese Sitzung meldet Sie bei GNOME an Die interne Sitzungsverwaltung verwenden (im Gegensatz zur systemd-basierten) systemd-Sitzungsverwaltung verwenden Version dieser Anwendung Wenn aktiviert, speichert gnome-session die nächste Sitzung beim Abmelden automatisch, selbst wenn automatisches Speichern deaktiviert ist. _Fortfahren Benutzer ab_melden _Abmelden _Neue Sitzung Sitzung _entfernen 