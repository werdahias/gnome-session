��    ;      �  O   �           	     &  R   ?  g   �  Z   �  '   U     }     �     �  	   �  	   �  	   �  (   �  )   �     '     .  '   =  "   e     �     �     �     �     �      �     �        !        1  '   9  %   a  	   �  '   �     �  P   �     	     (	     8	  
   E	  ;   P	  3   �	  Q   �	     
     !
  &   8
     _
  *   p
      �
      �
  /   �
       =   )  	   g     q     z     �     �  (   �     �  �  �     �  (   �  m   �  �   ]  j   �  4   J       3   �     �     �     �     �  3     3   9     m     t  I   �  0   �  '        )     C     L     a  1   �     �     �     �     �  <   �  5   4     j  B   s     �  X   �  #   '     K     e     r  B   ~  >   �  Q      &   R  $   y  -   �      �  H   �  .   6  ,   e  L   �  "   �  [     	   ^  	   h  	   r     |     �  :   �     �         7      '      5             )           .   *   ;            -   	   9           /       &   !   %              1             :   6      +      8   #             0   (         "                        4   3         2                  $      ,                 
                      - the GNOME session manager %s requires an argument
 A problem has occurred and the system can't recover.
Please log out and try again. A problem has occurred and the system can't recover. All extensions have been disabled as a precaution. A problem has occurred and the system can't recover. Please contact a system administrator A session named ‘%s’ already exists AUTOSTART_DIR Additional startup _programs: Allow logout Browse… Co_mmand: Comm_ent: Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Do not load user-specified applications Don't prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Wayland Ignoring any existing inhibitors Log out Not responding Oh no!  Something has gone wrong. Options Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Session %d Session names are not allowed to contain ‘/’ characters Session names are not allowed to start with ‘.’ Session names are not allowed to start with ‘.’ or contain ‘/’ characters Session to use Show extension warning Show the fail whale dialog for testing Startup Programs This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME This session logs you into GNOME, using Wayland Version of this application _Automatically remember running applications when logging out _Continue _Log Out _Log out _Name: _New Session _Remember Currently Running Applications _Remove Session Project-Id-Version: gnome-session.HEAD
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-02-28 23:26+0200
Last-Translator: Khaled Hosny <khaledhosny@eglug.org>
Language-Team: Arabic <doc@arabeyes.org>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 ? 4 : 5;
X-Generator: Virtaal 1.0.0-beta1
X-Project-Style: gnome
  - مدير جلسات جنوم يتطلب الخيار %s معاملا
 حدثت مشكلة وتعذّر تعافي النظام.
حاول الخروج ثم الولوج مجددا. حدثت مشكلة وتعذّر تعافي النظام. ستتعطل جميع الامتدادات كإجراء احترازي. حدثت مشكلة وتعذّر تعافي النظام. حاول الاتصال بمدير النظام. جلسة باسم ‘%s’ موجودة بالفعل AUTOSTART_DIR برامج بدء الت_شغيل الإضافية: اسمح بالخروج تصفّح… الأ_مر: ال_تعليق: تعذّر الاتصال بمدير الجلسات تعذّر إنشاء مقبس استماع ICE: %s خاص جلسة خاصة لا تُحمّل التطبيقات التي حددها المستخدم لا تطلب تأكيدا من المستخدم فعّل كود تتبع الأخطاء فشل التنفيذ %s
 جنوم جنوم لا شيء جنوم على وايلاند متجاهلًا أية معيقات موجودة اخرج لا يستجيب لا! ثمة مشكلة. الخيارات تخطى مجلدات بدء التشغيل القياسية من فضلك، اختر جلسة خاصة للعمل أطفئ تم استدعاء البرنامج بخيارات متضاربة أعِد التشغيل يرفض العملاء الجدد لأن الجلسة يتم إطفاؤها حاليا
 التطبيقات المتذكرة غيّر ا_سم جلسة SESSION_NAME جلسة %d لا يسمح أن تتضمن أسماء الجلسات حرف '/' لا يسمح أن تبدأ أسماء الجلسات بـ '.' لا يسمح أن يبدأ اسم الجلسة بـ '.' أو أن يتضمن '/' الجلسة التي ستُستخدم أظهر تحذير الامتداد أظهر حوار الفشل للاختبار برامج بدء التشغيل هذه الخاصية تسمح لك باختيار جلسة محفوطة يعيق هذا البرنامج الخروج. تولجك هذه الجلسة في جنوم تولجك هذه الجلسة في جنوم باستخدام وايلاند إصدارة هذا التطبيق ت_ذكّر تلقائيا التطبيقات المشغلة عند تسجيل الخروج _أكمل ا_خرج ا_خرج الا_سم: _جلسة جديدة _تذكّر التطبيقات المشغلة حاليًا _احذف الجلسة 