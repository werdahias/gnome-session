��    <      �  S   �      (     )  4  F     {  R   �  g   �  Z   O  '   �     �     �     �  	   	  	   	  	   	  (   )	  )   R	     |	     �	  #   �	  '   �	  "   �	     
     
     -
     3
     ?
      M
     n
     v
  !   �
     �
  '   �
  %   �
  	   �
  '        /  P   6     �     �     �  
   �  ;   �  3     Q   6     �     �  &   �     �  *   �            2     S  =   o  	   �     �     �     �     �  (   �       �    ;   �  Z  0  :   �  �   �  1  �  �   �  L   �       M     9   d     �     �     �  `   �  }   H     �  -   �  �     f   �  T   �  ?   N  >   �     �  *   �     �  X        i     �  T   �     �  �     x   �       �   ;  <   �  �   �  -   �  .   �     �        D      M   X   z   �   $   !!  B   F!  N   �!  0   �!  r   	"  f   |"  6   �"  3   #  �   N#  (   �#     $     >$     ^$     m$  U   �$     �$     %                    1                 4                    .       	           7          2   *           $   +      )                 #                '   ,      0         5   6             9   ;         <       3           (   "   !       -              :   /   &       
                   8        - the GNOME session manager %s [OPTION...] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can't recover.
Please log out and try again. A problem has occurred and the system can't recover. All extensions have been disabled as a precaution. A problem has occurred and the system can't recover. Please contact a system administrator A session named ‘%s’ already exists AUTOSTART_DIR Additional startup _programs: Allow logout Browse… Co_mmand: Comm_ent: Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don't prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg Ignoring any existing inhibitors Log out Not responding Oh no!  Something has gone wrong. Options Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Session %d Session names are not allowed to contain ‘/’ characters Session names are not allowed to start with ‘.’ Session names are not allowed to start with ‘.’ or contain ‘/’ characters Session to use Show extension warning Show the fail whale dialog for testing Startup Programs This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Version of this application _Automatically remember running applications when logging out _Continue _Log Out _Log out _Name: _New Session _Remember Currently Running Applications _Remove Session Project-Id-Version: gnome-session
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-session&keywords=I18N+L10N&component=general
PO-Revision-Date: 2016-01-04 12:26+0700
Last-Translator: Akom Chotiphantawanon <knight2000@gmail.com>
Language-Team: Thai <thai-l10n@googlegroups.com>
Language: th
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Gtranslator 2.91.6
 - โปรแกรมจัดการวาระ GNOME %s [ตัวเลือก...] คำสั่ง

เรียก "คำสั่ง" ขณะระงับการทำงานบางส่วนของวาระ

  -h, --help        แสดงวิธีใช้นี้
  --version         แสดงรุ่นของโปรแกรม
  --app-id ID       ID ของโปรแกรมที่ใช้
                    เมื่อจะระงับ (ระบุหรือไม่ก็ได้)
  --reason REASON   เหตุผลของการระงับ (ระบุหรือไม่ก็ได้)
  --inhibit ARG     สิ่งที่จะระงับ, เป็นรายชื่อที่คั่นด้วยทวิภาคของ:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    ไม่ต้องเรียก "คำสั่ง" และหยุดรอแทน

ถ้าไม่ระบุตัวเลือก --inhibit จะถือเป็นการเดินเครื่องเปล่า
 %s ต้องมีอาร์กิวเมนต์
 เกิดปัญหาบางอย่างขึ้น โดยระบบไม่สามารถฟื้นคืนได้
กรุณาออกจากระบบ แล้วลองเข้ามาใหม่ เกิดปัญหาบางอย่างขึ้น โดยระบบไม่สามารถฟื้นคืนได้ ระบบได้ปิดการใช้ส่วนขยายทั้งหมดเพื่อเป็นการป้องกันแล้ว เกิดปัญหาบางอย่างขึ้น โดยระบบไม่สามารถฟื้นคืนได้ กรุณาติดต่อผู้ดูแลระบบ มีวาระชื่อ ‘%s’ อยู่ก่อนแล้ว AUTOSTART_DIR โ_ปรแกรมเริ่มวาระเพิ่มเติม: อนุญาตให้ออกจากระบบ เรียกดู… _คำสั่ง: ห_มายเหตุ: ติดต่อโปรแกรมจัดการวาระไม่สำเร็จ ไม่สามารถสร้างซ็อกเก็ตรอรับการเชื่อมต่อ ICE: %s กำหนดเอง วาระที่กำหนดเอง ปิดใช้การตรวจสอบการเร่งความเร็วด้วยฮาร์ดแวร์ ไม่ต้องโหลดโปรแกรมที่ผู้ใช้ระบุไว้ ไม่ต้องถามการยืนยันจากผู้ใช้ เปิดใช้โค้ดส่วนดีบั๊ก เรียกทำงาน %s ไม่สำเร็จ
 GNOME หุ่นตัวอย่าง GNOME GNOME ใน Xorg ไม่สนใจการระงับต่างๆ ที่มีอยู่ ออกจากระบบ ไม่ตอบสนอง คุณพระ!  มีอะไรผิดพลาดสักอย่าง ตัวเลือก กำหนดค่าไดเรกทอรีเริ่มต้นอัตโนมัติทับค่ามาตรฐาน กรุณาเลือกวาระที่กำหนดเองที่จะเรียกทำงาน ปิดเครื่อง โปรแกรมถูกเรียกด้วยตัวเลือกที่ขัดแย้งกันเอง เริ่มเปิดเครื่องใหม่ ไม่รับการเชื่อมต่อใหม่จากลูกข่าย เพราะวาระกำลังปิดตัว
 โปรแกรมที่จำไว้ เ_ปลี่ยนชื่อวาระ SESSION_NAME วาระ %d ชื่อวาระห้ามมีอักขระ ‘/’ ชื่อวาระห้ามขึ้นต้นด้วย ‘.’ ชื่อวาระห้ามขึ้นต้นด้วย ‘.’ หรือมีอักขระ ‘/’ วาระที่จะใช้ แสดงคำเตือนของส่วนขยาย แสดงกล่องแจ้งพังเพื่อทดสอบ โปรแกรมเริ่มวาระ รายการนี้จะให้คุณเลือกวาระที่บันทึกไว้ โปรแกรมนี้ขัดขวางการออกจากระบบอยู่ วาระนี้จะเข้าสู่ GNOME รุ่นของโปรแกรมนี้ จำรายชื่อโปรแกรมที่ทำงานอยู่โดย_อัตโนมัติเมื่อออกจากระบบ เข้าระบบ_ต่อไป _ออกจากระบบ _ออกจากระบบ _ชื่อ: วาระใ_หม่ _จำรายชื่อโปรแกรมที่ทำงานอยู่ _ลบวาระ 