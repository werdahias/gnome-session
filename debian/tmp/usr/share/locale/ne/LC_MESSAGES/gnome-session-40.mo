��    !      $  /   ,      �     �       '   !     I     W  (   d     �     �     �     �     �     �     �     �     �  	     '        8     ?     V     f  
   s     ~     �      �      �     �  	                       +    ;  6   L  =   �  `   �     "  ;   0  R   l     �  "   �  A   �  A   :	     |	     �	     �	     �	  8   �	     
  ^   ,
  *   �
  4   �
  /   �
          (  -   8  7   f  R   �  @   �  ;   2  +   n     �     �     �  +   �              	          
                                              !                                                                                          — the GNOME session manager %s requires an argument
 A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Custom Custom Session Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg Log out Not responding Power off Program called with conflicting options Reboot Remembered Application Rena_me Session SESSION_NAME Session %d Session to use Show extension warning This program is blocking logout. This session logs you into GNOME Version of this application _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: Gnome Nepali Translation Project
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=gnome-session&keywords=I18N+L10N&component=general
PO-Revision-Date: 2017-08-15 10:53+0545
Last-Translator: Pawan Chitrakar <chautari@gmail.com>
Language-Team: Nepali Translation Team <chautari@gmail.com>
Language: ne
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.3
Plural-Forms: nplurals=2; plural=n !=1;
X-Poedit-SourceCharset: UTF-8
 जिनोम  सेसन प्रबन्धक %sलाई एउटा तर्क आवश्यक छ
 '%s' नाम गरेको सेसन पहिले नै अवस्थित छ । AUTOSTART_DIR लगआउट अनुमति दिनुहोस् सत्र प्रबन्धकमा जडान गर्न सकेन अनुकूलन अनुकूलन सत्र डिबगिङ सक्षम पार्नुहोस् %s कार्यान्वयन गर्न असफल: 
 जिनोम जिनोम डम्मि  Xorg मा जिनोम लगआउट जवाफ दिन बन्द गरेको छ. पावर बन्द हाल विवादित विकल्प निम्न हुन्: --%s र --%s फेरि सुरु गर्ने  सम्झिएको अनुप्रयोग सत्रको नाम फेर्ने SESSION_NAME %d सत्र प्रयोग गर्न सत्र  चेतावनी देखाउनुहोस् यो कार्यक्रमले लगआउट गर्न दिएन यो सत्र जिनोम लगईन हुन्छ यो अनुप्रयोगको स‌सकरण जारी राख्नुहोस् लगआउट लगआउट नया सत्र सत्र हटाउनुहोस् 