��    2      �  C   <      H     I     h  T   �  i   �  \   @     �  (   �  )   �     �       #     '   7  $   _     �     �     �     �  >   �            !     '   <  %   d  	   �  '   �     �  P   �          +     ;     H     V  
   h  ;   s  3   �  Q   �     5	     D	  &   [	  *   �	      �	      �	     �	  p   
  	   |
     �
     �
     �
     �
  )  �
  !   �       b     _   w  S   �     +  )   9  .   c     �     �     �  -   �     �          /     G     M  ?   ]     �     �     �  ,   �  ,   �     +  +   1     ]  G   k     �     �     �     �     �       :     1   Y  Q   �     �     �  %     -   <     j  #   �     �  s   �     1     9     H     P     c        0   2      1                                  &   "       
                             ,          *           .   %       )                         (      !      -   '                             /      $          +   #      	        — the GNOME session manager %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME on Xorg If enabled, gnome-session will save the session automatically. Log out Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: 
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2018-12-16 21:01+0100
Language: kab
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: 
Language-Team: 
X-Generator: Poedit 2.2
  — Amsefrak n tɣimiyin n GNOME %s yesra tiɣiret
 Yeḍra-d wugur yerna anagraw ulamek ara d-yuɣal.
Ttxil-k senser sakin eɛreḍ tikkelt-nniḍen. Yeḍra-d wugur yerna anagraw ulamek ara d-yuɣal. Akk isiɣzifen ttusexsin i lmend n leḥdar. Yeḍra-d wugur yerna anagraw ulamek ara d-yuɣal. Ttxil-k nermes anedbal n unagraw Sureg asenser D awezɣi tuqqna ɣer usefrak n tɣimiyin D awezɣi asnulfu n umaqqan n usefled ICE : %s Yugen Tiɣimit yugnen Sens aselken n wesɣiwel angaw Ur ttɛebbi ara isnasen ara d-yesuter useqdac Ur ssutur ara asentem n useqdac Sermed tangalt n weseɣti D awezɣi aselkem n %s
 GNOME GNOME ɣef Xorg Ma yermed, gnome-session ad isekles s wudem awurman tiɣimiyin. Senser Ulac tiririt Ayaa ! Yella wacu ur neddi ara. Ad yesemselsi ikaramen autostart imesluganen Ttxil-k fren tiɣimit yugnen ara tsekkereḍ Sexsi Ahil nessawel-as s textiṛiyin yemgarraden Alles asekker Ahil yugi tuqqna n umsaɣ amaynut acku tiɣimit ala tetteddu ad temdel
 Isnasen yettumektan _Beddel isem n tɣimit ISEM_TIƔIMIT Sekles tiɣimiyin Sekles tiɣimit-agi Tiɣimit %d Ismawen n tɣimit ur zmiren ara ad sɛun isekkilen “/” Ismawen n tɣimit ur zmiren ara ad bdun s “.” Ismawen n tɣimit ur zmiren ara ad bdun s “.” neɣ ad sɛun isekkilen “/” Tiɣimit ara yettwasqedcen Beqqeḍ asmigel n isiɣzifen Beqqeḍ adiwenni n unezri i wesekyed S wefran-agi ad tferneḍ tɣimit ittuskelsen Ahil-agi yeseḥbes asenser. Tiɣimit-agi ad k-teqqen ɣer GNOME Lqem n wesnas-agi Ma yermed, gnome-session ad isekles s wudem awurman tiɣimit tuḍfirt mi ara tsensdreḍ ma yexsi usekles awurman. _Kemmel _Mdel tiɣimit _Senser Tiɣimit ta_maynut Kke_s tiɣimit 