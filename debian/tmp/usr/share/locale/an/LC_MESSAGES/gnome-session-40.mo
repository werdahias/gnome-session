��    <      �  S   �      (     )  4  F     {  R   �  g   �  Z   O  '   �     �     �     �  	   	  	   	  	   	  (   )	  )   R	     |	     �	  #   �	  '   �	  "   �	     
     
     -
     3
     ?
      M
     n
     v
  !   �
     �
  '   �
  %   �
  	   �
  '        /  P   6     �     �     �  
   �  ;   �  3     Q   6     �     �  &   �     �  *   �            2     S  =   o  	   �     �     �     �     �  (   �                 S  =     �  _   �  m     a   y  $   �        !        ?     W  	   c     m  -   z  ,   �     �     �  3   �  4   ,  &   a      �     �     �     �     �  -   �          #  #   /     S  7   [  5   �     �  0   �  
     Z        h     }     �  
   �  2   �  8   �  N        i  %   }  (   �     �  /   �  /        B     ^  K   z  	   �     �     �     �     �  8        F     %                    1                 4                    .       	           7          2   *           $   +      )                 #                '   ,      0         5   6             9   ;         <       3           (   "   !       -              :   /   &       
                   8        - the GNOME session manager %s [OPTION...] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can't recover.
Please log out and try again. A problem has occurred and the system can't recover. All extensions have been disabled as a precaution. A problem has occurred and the system can't recover. Please contact a system administrator A session named ‘%s’ already exists AUTOSTART_DIR Additional startup _programs: Allow logout Browse… Co_mmand: Comm_ent: Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don't prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg Ignoring any existing inhibitors Log out Not responding Oh no!  Something has gone wrong. Options Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Session %d Session names are not allowed to contain ‘/’ characters Session names are not allowed to start with ‘.’ Session names are not allowed to start with ‘.’ or contain ‘/’ characters Session to use Show extension warning Show the fail whale dialog for testing Startup Programs This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Version of this application _Automatically remember running applications when logging out _Continue _Log Out _Log out _Name: _New Session _Remember Currently Running Applications _Remove Session Project-Id-Version: gnome-session master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-session&keywords=I18N+L10N&component=general
PO-Revision-Date: 2015-12-27 18:11+0200
Last-Translator: Daniel <entaltoaragon@gmail.com>
Language-Team: Aragonés <softaragones@googlegroups.com>
Language: an
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.5.1.1
X-POOTLE-MTIME: 1451236260.000000
  - O chestor de sesions de GNOME %s [OPCIÓN...] COMANDO

Executar COMANDO a l'inhibir bella funcionalidat d'a sesión.

  -h, --help        Amostrar ista aduya
  --version         Amostrar a versión d'o programa
  --app-id ID       L'ID d'aplicación ta emplegar
                    a l'inhibir (opcional)
  --reason RAZÓN   a razón ta inhibir (opcional)
  --inhibit ARG     Qué inhibir, lista d'elementos separaus por dos puntos de:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    No executar COMANDO y asperar indefinidament

Si no s'especifica a opción --inhibit s'asume «idle».
 %s amenista un argumento
 Ocurrió un problema y o sistema no se puet recuperar.
zarra la sesión y intenta-lo de nuevas. Ocurrió un problema y o sistema no se puet recuperar. Por precacción, s'han desactivau todas as extensions. Ocurrió un problema y o sistema no se puet recuperar. Contacta con un administrador de sistemas. Ya bi ha una sesion que se dice '%s' CARPETA_D'ENCIETO_AUTOMATICO _Programas adicionals ta encetar: Permitir zarrar sesión Examinar… Co_mando: Com_entario: No se pudo connectar con o chestor de sesions No se pudo creyar o socket d'escuita ICE: %s Personalizau Sesión personalizada Desactivar a verificación d'acceleración hardware No cargar as aplicacions especificadas por l'usuario No requerir confirmación de l'usuario Enchegar o codigo de depuración Ha fallau en executar %s
 GNOME GNOME dummy GNOME en Xorg Se ye inorando qualsiquier inhibidor existent Trancar a sesión No responde Redios! Bella cosa salió malament. Opcions Sobreescribir as carpetas d'encieto automatico estandar Por fabor tríe una sesión personalizada ta executar Amortar Se clamó a lo programa con opcions en conflicto Reenchegar Se ye refusando a connexión d'un nuevo client porque actualment se ye zarrando a sesión
 Aplicación remerada Reno_mbrar sesión NOMBRE_D'A_SESIÓN Sesión %d Os nombres de sesion no pueden tener caracters '/' Os nombres de sesion no pueden estar prencipiaus con '.' Os nombres de sesion no pueden estar prencipiaus con '.' u tener caracters '/' Sesión ta emplegar Amostrar alvertencias d'as extensions Amostrar o dialogo d'a ballena de prebas Programas a l'encieto Ista dentrada te deixa trigar una sesion alzada Iste programa ye bloquiando a zarrada de sesió Ista sesión accede a GNOME Versión d'ista aplicación Remerar _automaticament as aplicacions en execución a lo salir d'a sesión _Continar T_rancar a sesión _Zarrar a sesión _Nombre: _Nueva sesión _Remerar as aplicacions que se son executando actualment Saca_r sesión 