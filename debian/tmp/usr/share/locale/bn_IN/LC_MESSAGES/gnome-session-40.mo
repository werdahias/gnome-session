��    D      <  a   \      �     �  p        q  T   �  i   �  \   I	  '   �	     �	     �	  (   �	  )   
     <
     C
  #   R
  '   v
  $   �
     �
     �
     �
     �
       q     G   �  >   �           )     1     ?  !   N  '   p  %   �  	   �  '   �  #   �       P        l     �  %   �  M   �          "     /     =  
   O  ;   Z  3   �  Q   �          +  &   B     i  +   �  #   �  P   �  *   $      O      p  B   �     �     �  p     	   �     �     �     �     �  �  �  :   �  '  �  U   �  �   A  0  <    m  d   �     �  3   �  �   2  c   �  $   '  4   L  y   �  �   �  }   �  R      4   g      �      �      �   B  �   �   "  �   �"  h   �#     $  %    $  ;   F$  B   �$  o   �$  �   5%     �%  �   �%  d   �&     �&  T  '  E   g(  G   �(  Y   �(  �   O)  R   �)     7*  /   D*  6   t*     �*  l   �*  p   (+  �   �+  4   9,  A   n,  �   �,  ;   6-  h   r-  5   �-  �   .  �   �.  �   </  a   �/  �   (0  t   �0  @   p1  9  �1  !   �2  "   3  "   03     S3     r3         !                    #       *   9   6   4           0   )              8       (      '   $   %           >                     	          -          A   D   ,                                        B              ?   ;              :      
       +   .   3          /   5   1           2   =          @      "      C                7   &      <        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: bn_IN
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2020-10-11 12:49+0530
Last-Translator: Akarshan Biswas <akarshan.biswas@hotmail.com>
Language-Team: Bengali <anubad@lists.ankur.org.in>
Language: bn_IN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1)
X-DamnedLies-Scope: partial
X-Generator: Gtranslator 3.38.0
 - জিনোম সেশন ম্যানেজার %s [বিকল্প…] COMMAND

কিছু সেশন কার্যকারিতা বাধা দেওয়ার সময় COMMAND সম্পাদন করুন।

  -h, --help এই সাহায্যটি দেখান
 --version  প্রোগ্রাম সংস্করণ প্রদর্শন করুন
 --app-id ID ইনহিবিত করার সময় যেই অ্যাপ্লিকেশান আইডি ব্যাবহার করবেন(ঐচ্ছিক)
--reason REASON  ইনহিবিত দেওয়ার কারণ (ঐচ্ছিক)
--inhibit ARG  ইনহিবিত দেওয়ার বিষয়গুলি, কোলন-বিচ্ছিন্ন তালিকা:
  logout, switch-user, suspend, idle, automount
 --inhibit-only COMMAND আরম্ভ করবেন না এবং পরিবর্তে চিরকালের জন্য অপেক্ষা
-l, --list বিদ্যমান ইনহিবিতগুলি তালিকাভুক্ত করুন এবং প্রস্থান করুন
যদি কোনও --inhibit বিকল্প নির্দিষ্ট না করা থাকে তবে idle ধরে নেওয়া হয়।
 %s এর একটি অার্গুমেন্টের অাবশ্যক
 একটি সমস্যা দেখা দিয়েছে এবং সিস্টেমটি পুনরুদ্ধার করতে পারছে না।
 লগ আউট করে আবার চেষ্টা করুন। একটি সমস্যা দেখা দিয়েছে এবং সিস্টেমটি পুনরুদ্ধার করতে পারছে না। সাবধানতা জন্য সমস্ত এক্সটেনশন অক্ষম করা হয়েছে। একটি সমস্যা দেখা দিয়েছে এবং সিস্টেমটি পুনরুদ্ধার করতে পারছে না। একটি সিস্টেম প্রশাসকের সাথে যোগাযোগ করুন ‘%s’ নামের একটি সেশন ইতিমধ্যেই উপস্থিত AUTOSTART_DIR লগ আউটের অনুমতি দিন সেশান পরিচালনব্যবস্থার সাথে সংযোগ স্থাপন করতে ব্যর্থ ICE অপেক্ষার সকেট নির্মাণ করতে ব্যর্থ: %s স্বনির্ধারিত স্বনির্ধারিত সেশান হার্ডওয়্যার এক্সিলারেশন পরীক্ষা অক্ষম করুন ব্যবহারকারী দ্বারা নির্ধারিত অ্যাপ্লিকেশন লোড করা হবে না ব্যবহারকারী নিশ্চিতকরণের জন্য অনুরোধ করবেন না কোড ডিবাগ ব্যবস্থা সক্রিয় করুন %s সম্পাদন করা গেল না
 GNOME GNOME ডামি Xorg এ জিনোম যদি সক্ষম করা থাকে, সেশনটি স্বয়ংক্রিয়ভাবে পিছনে পড়ে গেলে জিনোম-সেশন লগইন করার পরে একটি সতর্কতা ডায়ালগ প্রদর্শন করবে। যদি সক্ষম করা থাকে, জিনোম-সেশনটি অধিবেশন শেষ করার আগে ব্যবহারকারীকে অনুরোধ জানাবে। সক্ষম করা থাকলে, জিনোম-সেশনটি স্বয়ংক্রিয়ভাবে সেশনটি সংরক্ষণ করবে। কোনো উপস্থিত প্রতিরোধ অগ্রাহ্য করা হবে লগ-আউট লগআউট প্রম্পট কোনো প্রতিক্রিয়া হয়নি ওহ হো!  কিছু সমস্যা হয়েছে। প্রমিত autostart ডিরেক্টরির মান উপেক্ষা করা হবে চালাতে অনুগ্রহ করে একটি স্বনির্বাচিত সেশন নির্বাচন করুন বন্ধ করুন দ্বন্দ্বযুক্ত বিকল্প সহ প্রোগ্রাম আরম্ভের প্রচেষ্টা করা হয়েছে প্রোগ্রামের ঠিক এক প্যারামিটার দরকার পুনরারম্ভ সিস্টেম বর্তমানে বন্ধ করার প্রচেষ্টা করা হচ্ছে ও এই কারণে নতুন ক্লায়েন্টের সাথে সংযোগ স্থাপনের অনুরোধ প্রত্যাখ্যান করা হচ্ছে।
 অ্যাপ্লিকোশন মনে রাখা হবে সেশনের নাম পরিবর্তন করুন (_m) dbus.service চালু থাকলে পুনরায় চালু করুন পরিষেবা ব্যর্থতায় gnome-session-failed.target শুরু করতে ExecStopPost থেকে চালান সিস্টেম ডী সার্ভিস হিসাবে চলছে SESSION_NAME সেশন সংরক্ষণ করুন এই সেশন সংরক্ষণ করুন সেশন %d সেশন নামগুলিতে "/" অক্ষর থাকার অনুমতি নেই। সেশন নামগুলি "." দিয়ে শুরু করার অনুমতি নেই। সেশন নামগুলি "." দিয়ে শুরু করা অথবা "/" অক্ষর  থাকার অনুমতি নেই। ব্যবহারযোগ্য সেশান এক্সটেনশন সর্তকতা দেখান পরীক্ষার জন্য ফেইল-ওয়েইল ডায়লগটি প্রদর্শন করা হবে ফলব্যাক সতর্কতা দেখান জিনোম-সেশনে সংকেত সূচনা সম্পন্ন হয়েছে Gnome-session-utdown.target শুরু করুন  EOF বা stdin এ একটি একক বাইট পাওয়ার সময় gnome-session-shutdown.target শুরু করুন এই এন্ট্রি অাপনাকে একটি সংরক্ষিত সেশন নির্বাচন করতে দেয় এই প্রোগ্রাম দ্বারা লগ-আউট করতে প্রতিরোধ করা হচ্ছে। এই সেশানের মাধ্যমে GNOME-এ লগ-ইন করা যাবে অন্তর্নির্মিত সেশন ম্যানেজমেন্টে ব্যবহার করুন (সিস্টেম  ডী ভিত্তিক একের চেয়ে) সিস্টেমে ডী সেশন ম্যানেজমেন্ট ব্যবহার করুন অ্যাপ্লিকেশনের সংস্করণ সক্ষম করা থাকলে, স্বয়ংক্রিয় সংরক্ষণ অক্ষম থাকলেও জিনোম-সেশন লগ আউটে পরবর্তী সেশনটি স্বয়ংক্রিয়ভাবে সংরক্ষণ করবে। এগিয়ে চলুন (_C) লগ-আউট করুন (_L) লগ-আউট করুন (_L) নতুন সেশন (_N) সেশন সরান (_R) 