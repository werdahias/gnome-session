��    <      �  S   �      (     )  4  F     {  R   �  g   �  Z   O  '   �     �     �     �  	   	  	   	  	   	  (   )	  )   R	     |	     �	  '   �	  "   �	     �	     �	     	
     
     
      ,
     M
     U
  !   d
     �
  '   �
  %   �
  	   �
  '   �
       P        f     }     �  
   �  ;   �  3   �  Q        g     v  &   �     �  *   �      �        /   2     b  =   ~  	   �     �     �     �     �  (   �       �  %  C   �  4  9  C   n  �   �    �  �   �  g   w     �  I   �  .   7     f     z  $   �  f   �  �        �  .   �  T   �  \   G  l   �  N        `     f     |  h   �     �  +     U   E     �  s   �  �   +  4   �  W   �  !   K  �   m  A   =  6        �     �  �   �  �   ~  �     1   �  \      j   k   Z   �   �   1!  ]   �!  o   4"  �   �"  H   P#  �   �#     N$     l$     �$     �$  '   �$  ~   �$  *   e%     $             3       0                  4                    -       	           7          1   )           #   *      (                 "                &   +      /         5   6            9   ;         <       2           '   !           ,             :   .   %       
                   8        - the GNOME session manager %s [OPTION...] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can't recover.
Please log out and try again. A problem has occurred and the system can't recover. All extensions have been disabled as a precaution. A problem has occurred and the system can't recover. Please contact a system administrator A session named ‘%s’ already exists AUTOSTART_DIR Additional startup _programs: Allow logout Browse… Co_mmand: Comm_ent: Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Do not load user-specified applications Don't prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Wayland Ignoring any existing inhibitors Log out Not responding Oh no!  Something has gone wrong. Options Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Session %d Session names are not allowed to contain ‘/’ characters Session names are not allowed to start with ‘.’ Session names are not allowed to start with ‘.’ or contain ‘/’ characters Session to use Show extension warning Show the fail whale dialog for testing Startup Programs This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME This session logs you into GNOME, using Wayland Version of this application _Automatically remember running applications when logging out _Continue _Log Out _Log out _Name: _New Session _Remember Currently Running Applications _Remove Session Project-Id-Version: gnome-session.HEAD.ta
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-session&keywords=I18N+L10N&component=general
PO-Revision-Date: 2013-11-25 12:31+0530
Last-Translator: Shantha kumar <shkumar@redhat.com>
Language-Team: American English <>
Language: ta
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);\n

X-Generator: Lokalize 1.5
 - க்னோம் அமர்வு மேலாலாளர் %s [OPTION...] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s க்கு ஒரு மதிப்புரு தேவை
 ஒரு பிரச்சினை நிகழ்ந்தது. கணினி மீள இயலாது.
வெளியேறி மீண்டும் முயற்சிக்கவும் ஒரு பிரச்சினை நிகழ்ந்தது. கணினி மீள இயலாது. முன் எச்சரிக்கையாக எல்லா நீட்சிகளும் செயல் இழக்கப்பட்டுள்ளன. ஒரு பிரச்சினை நிகழ்ந்தது. கணினி மீள இயலாது. ஒரு கணினி மேலாளரை தொடர்பு கொள்ளவும். ‘%s’ பெயருடைய ஒரு அமர்வு ஏற்கனவே உள்ளது AUTOSTART_DIR மேலும்சில தொடங்க _நிரல்கள்: வெளிசெல்ல அனுமதி உலாவு...  (_m) கட்டளை: (_e) குறிப்புரை: அமர்வு மேலாளருடன் இணைக்க முடியவில்லை ஐஸ் கேட்கும் பொருத்துவாயை உருவாக்க முடியவில்லை : %s தனிப்பயன் தனிப்பயன் அமர்வு பயனர் குறித்த நிரல்களை ஏற்றாதே பயனர் உறுதிபடுத்தலுக்கு தூண்டாதே பிழைத்திருத்த குறியீட்டை செயல்படுத்து. %s ஐச் செயல்படுத்துவது தோல்வி
 GNOME GNOME டம்மி Wayland இல் GNOME ஏதாவது தடை வரின் அவற்றை உதாசீனம் செய்க வெளியேறுக பதிலளிக்க இல்லை அடடா! ஏதோ தவறு நிகழ்ந்துவிட்டது தேர்வுகள் செந்தர தானியங்கி அடைவுகளை வலுவாக புறக்கணி ஒரு தனிப்பயன் அமர்வை இயக்குவதற்கு தேர்ந்தெடுக்கவும்  மின்சக்தி நிறுத்து நிரல் முரணான தேர்வுகளை அழைத்தது மறுதுவக்கம் புதிய சார்ந்தோன் இணைப்பை மறுக்கிறது; ஏனெனில் இப்போதைய அமர்வு மூடப்படுகிறது
 நினைவில் கொண்ட பயன்பாடு அமர்வை மறுபெயரிடு (_m) SESSION_NAME அமர்வு %d அமர்வு பெயர்கள் '/' எழுத்துக்களை கொண்டிருக்க அனுமதிப்பதில்லை அமர்வு பெயர்கள் ஒரு '.' எழுத்துடன் துவக்க அனுமதிப்பதில்லை அமர்வு பெயர்கள் ஒரு '.' அல்லது ‘/’ எழுத்துக்களுடன் துவக்க அனுமதிப்பதில்லை பயன்படுத்த அமர்வு நீட்டிப்பு எச்சரிக்கையைக் காட்டு சோதனைக்கு பெய்ல் வேல் உரையாடலை காட்டுக தொடங்கும் போது இயக்கும் நிரல்கள் இந்த உள்ளீடானது ஒரு சேமிக்கப்பட்ட அமர்வை தேர்ந்தெடுக்கிறது இந்த நிரல் வெளியேறுவதை தடுக்கிறது இந்த அமர்வு க்நோம் இல் உங்களை நுழைக்கும்  இந்த அமர்வு உங்களை Wayland ஐப் பயன்படுத்தி GNOME இல் புகுபதிவு செய்யும் இந்த பயன்பாட்டின் பதிப்பு  தானியங்கியாக வெளியேறும் போது இயங்கும் பயன்பாடுகளை நினைவில் கொள்க தொடரவும் (_C) (_L) விலகவும் வெளியேறு (_L) பெயர்: (_N) புதிய அமர்வு (_N) (_R) இப்போது இயங்கும் பயன்பாடுகளை நினைவில் கொள்க அமர்வை நீக்கு (_R) 