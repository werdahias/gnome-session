��    C      4  Y   L      �     �  4  �       T     i   s  \   �  '   :	     b	     p	  (   }	  )   �	     �	     �	  #   �	  '   

  $   2
     W
     m
     �
     �
     �
  q   �
  G     >   ]      �     �     �     �  !   �  '     %   ,  	   R  '   \  #   �     �  P   �             %   '     M     h     u     �  
   �  ;   �  3   �  Q        b     q  &   �     �  +   �  #   �  P     *   j      �      �  B   �          9  p   U  	   �     �     �     �     �  �  �     �  S  �       s   (  �   �  g   &  "   �     �     �  +   �  ,        .  	   3  &   =  )   d  "   �     �     �     �  
   �     �  �     U   �  D   �  '        F     T     j  "   x      �  *   �     �  +   �  #     
   =  H   H     �     �  +   �  %   �  	                  -  ,   5  (   b  D   �     �     �  *   �     '  /   A  %   q  S   �  1   �  )     &   G  G   n     �     �  z   �  	   a     k     z     �     �     +   7      >   C                0   %          5   6   #       8      =   4              *          2       <   ?                               1                 -   A   :          '             9   (              3       	              /          !   )   $         ,   .   ;   
         &           @            B   "        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session HEAD
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2020-01-26 09:22+0800
Last-Translator: abuyop <abuyop@gmail.com>
Language-Team: Pasukan Terjemahan GNOME Malaysia
Language: ms
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
Plural-Forms: nplurals=2; plural=n != 1;
  — pengurus sesi GNOME %s [OPTION…] COMMAND

Lakukan COMMAND ketika merencat beberapa kefungsian sesi.

  -h, --help        Tunjuk bantuan ini
  --version         Tunjuk versi program
  --app-id ID       Id aplikasi yang digunakan
                    ketika merencat (pilihan)
  --reason REASON   Sebab melakukan perencatan (pilihan)
  --inhibit ARG     Perkara yang direncatkan, senarai dipisah-tanda-titik-bertindih:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Jangan lancarkan COMMAND dan tunggu selamanya

Jika tiada pilihan --inhibit dinyatakan, melahu dikuatkuasakan.
 %s memerlukan satu argumen
 Satu masalah telah berlaku dan menyebabkan sistem tidak dapat dipulihkan. 
Sila daftar keluar dan cuba sekali lagi. Satu masalah telah berlaku dan menyebabkan sistem tidak dapat dipulihkan. Semua sambungan telah dilumpuhkan sebagai langkah berjaga-jaga. Satu masalah telah berlaku dan menyebabkan sistem tidak dapat dipulihkan. Sila hubungi pentadbir sistem Satu sesi bernama "%s" telah wujud DIR_AUTOMULA Benarkan daftar keluar Tidak dapat bersambung dengan pengurus sesi Tidak dapat mencipta soket mendengar ICE: %s Suai Sesi Suai Lumpuhkan semakan pemecutan perkakasan Jangan memuatkan aplikasi khusus-pengguna Jangan bisikan pengesahan pengguna Benarkan kod penyahpepijatan Gagal melakukan %s
 GNOME semu GNOME GNOME di Xorg Jika dibenarkan, gnome-session akan paparkan satu dialog amaran selepas daftar masuk jika sesi dijatuh-balikkan secara automatik. Jika dibenarkan, gnome-session akan membisik pengguna sebelum mengakhirkan satu sesi. Jika dibenarkan, gnome-session akan menyimpan sesi secara automatik. Mengabaikan mana-mana perencat yang ada Daftar keluar Bisikan daftar keluar Tiada respons Alamak! Ada masalah telah berlaku. Batal direktori auto-mula piawai Sila pilih satu sesi suai untuk dijalankan Padam Program dipanggil dengan pilihan bercanggah Program perlu satu parameter sahaja But Semula Enggan menerima sambungan klien baharu kerana sesi ini sedang dimatikan
 Aplikasi Diingati Nam_a Semula Sesi Mula semula dbus.service jika ia dijalankan Berjalan sebagai perkhidmatan systemd NAMA_SESI Simpan sesi Simpan sesi ini Sesi %d Nama sesi tidak boleh mengandungi aksara "/" Nama sesi tidak boleh bermula dengan "." Nama sesi tidak boleh bermula dengan "." atau mengandungi aksara "/" Sesi yang digunakan Tunjuk amaran sambungan Tunjukkan dialog kegagalan untuk pengujian Tunjuk amaran jatuh-balik Pengawalan isyarat dilakukan pada gnome-session Mulakan gnome-session-shutdown.target Mulakan gnome-session-shutdown.target ketika menerima EOF atau satu bait pada stdin Masukan ini membolehkan anda pilih sesi tersimpan Program ini telah menyekat daftar keluar. Sesi ini akan mendaftar masuk ke GNOME Guna pengurusan sesi terbina-dalam (berbanding yang berasaskan systemd) Guna pengurusan sesi systemd Versi aplikasi ini Jika dibenarkan, gnome-session akan menyimpan sesi secara automatik ketika daftar keluar walaupun auto-simpan dilumpuhkan. _Teruskan _Daftar Keluar _Daftar keluar _Sesi Baharu _Buang Sesi 