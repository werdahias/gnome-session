��    D      <  a   \      �     �  p        q  T   �  i   �  \   I	  '   �	     �	     �	  (   �	  )   
     <
     C
  #   R
  '   v
  $   �
     �
     �
     �
     �
       q     G   �  >   �           )     1     ?  !   N  '   p  %   �  	   �  '   �  #   �       P        l     �  %   �  M   �          "     /     =  
   O  ;   Z  3   �  Q   �          +  &   B     i  +   �  #   �  P   �  *   $      O      p  B   �     �     �  p     	   �     �     �     �     �  �  �     W  �  u     '  V   A  c   �  P   �     M     i     �  %   �  '   �     �     �  6   �  )   0     Z  %   u     �     �     �     �  v   �  K   S  =   �  ,   �  	   
          *  "   9  8   \  /   �     �  /   �  /        8  L   H     �     �  0   �  M   �  $   C  
   h     s     �  
   �  ,   �  &   �  G   �     ?     T  8   q  %   �  4   �  *     S   0  0   �  *   �  %   �  <     !   C     e  �     	      
   (   
   3      >      K          !                    #       *   9   6   4           0   )              8       (      '   $   %           >                     	          -          A   D   ,                                        B              ?   ;              :      
       +   .   3          /   5   1           2   =          @      "      C                7   &      <        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2020-08-07 17:56+0300
Last-Translator: Emin Tufan Çetin <etcetin@gmail.com>
Language-Team: Türkçe <gnome-turk@gnome.org>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 2.4
  — GNOME oturum yöneticisi %s [SEÇENEK…] KOMUT

Bazı oturum işlevlerini kısıtlayarak KOMUT çalıştır.

  -h, --help        Bu yardımı göster
  --version         Program sürümünü göster
  --app-id ID       Kısıtlama yapılırken kullanılacak
                    uygulama ID’si (isteğe bağlı)
  --reason REASON   Kısıtlama nedeni (isteğe bağlı)
  --inhibit ARG     Kısıtlamaların virgülle ayrılmış listesi:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    KOMUT’u çalıştırmak yerine sürekli bekle
  -l, --list        Var olan kısıtlamaları göster ve çık

Eğer hiç --inhibit seçeneği verilmediyse, idle kısıtı kabul edilir.
 %s için parametre eksik
 Sorun oluştu ve sistem kurtarılamıyor.
Lütfen çıkış yapın ve yeniden deneyin. Sorun oluştu ve sistem kurtarılamıyor. Önlem olarak tüm eklentiler devre dışı bırakıldı. Sorun oluştu ve sistem kurtarılamıyor. Lütfen sistem yöneticisine başvurun “%s” adında oturum var KENDILIGINDANBASLAT_DIZINI Çıkışa izin ver Oturum yöneticisine bağlanılamadı ICE dinleme soketi oluşturulamadı: %s Özel Özel Oturum Donanım hızlandırma denetimini devre dışı bırak Kullanıcıya özel programları yükleme Kullanıcıdan onay isteme Kod hatası ayıklamayı etkinleştir %s çalıştırılamadı
 GNOME GNOME dummy Xorg üzerinde GNOME Etkinse; gnome-session, oturum kendiliğinden yedeğe düşmüşse girişin ardından uyarı penceresi gösterecektir. Etkinse; gnome-session, oturumu bitirmeden önce kullanıcıya soracaktır. Etkinse; gnome-session, oturumu kendiliğinden kaydedecektir. Var olan tüm engelleyiciler yok sayılıyor Çıkış Oturum kapatma istemi Yanıtlamıyor Tüh!  Bir şeyler yanlış gitti. Standart kendiliğinden başlat dizinlerinin yerine geç Lütfen çalıştırılacak özel oturum seçin Bilgisayarı kapat Program çakışan seçeneklerle çağırıldı Program tam olarak bir parametreye gereksiniyor Yeniden Başlat Oturum şu anda kapatıldığından yeni istemci bağlantısı reddediliyor
 Anımsanan Uygulama Oturumu Yeniden _Adlandır Çalışıyorsa dbus.service’i yeniden başlat Servis hatasında gnome-session-failed.target’ı ExecStopPost’tan başlat systemd servisi olarak çalışıyor OTURUM_ADI Oturumları kaydet Bu oturumu kaydet %d Oturumu Oturum adları “/” karakterini içeremez Oturum adları “.” ile başlayamaz Oturum adları “.” ile başlayamaz ve “/” karakterini içeremez Kullanılacak oturum Eklenti uyarısını göster Sınama için büyük hata iletişim penceresini göster Yedeğe dönüş uyarısını göster Sinyal ilklendirilmesi gnome-session’a tamamlandı gnome-session-shutdown.target’ı başlat stdin’de EOF veya tek bayt alınırken gnome-session-shutdown.target’ı başlat Bu girdi, kaydedilmiş oturum seçmenizi sağlar Bu program, oturumu kapatmayı engelliyor. Bu oturum GNOME’a girmenizi sağlar Yerleşik oturum yönetimini kullan (systemd temelli yerine) systemd oturum yönetimini kullan Bu uygulamanın sürümü Etkinleştirildiğinde; gnome-session, kendiliğinden kaydetme devre dışı olsa bile oturum kapatıldığında sonraki oturumu kendiliğinden kaydedecektir. _Sürdür _Çıkış _Çıkış _Yeni Oturum Oturumu _Kaldır 