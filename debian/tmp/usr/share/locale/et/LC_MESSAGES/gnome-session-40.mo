��    3      �  G   L      h     i  4  �     �  T   �  i   +  \   �  '   �          (  (   5  )   ^     �     �  #   �  '   �  $   �     	     %	     ;	     A	     M	      [	     |	     �	  !   �	  '   �	  %   �	  	   
  '   
     5
  P   <
     �
     �
     �
  
   �
  ;   �
  3     Q   <     �     �  &   �  *   �            '     H  	   d     n     w     �     �  �  �     _  I  w     �  H   �  X   (  S   �  %   �     �       (   -  *   V  
   �     �  '   �  (   �     �          !     9     ?     P  )   a     �     �     �  O   �  -        ;  4   M     �  I   �     �     �       	     .     '   I  E   q     �     �  ,   �  )     %   ;  3   a     �     �     �     �     �     �                                    	   1      $   2                         *              0   -            "   %   &      3           !   /             #                   
          +                                         .   '   (             )   ,     — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg Ignoring any existing inhibitors Log out Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Version of this application _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session MASTER
Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=gnome-session&keywords=I18N+L10N&component=general
PO-Revision-Date: 2018-02-09 16:00+0200
Last-Translator: Mart Raudsepp <leio@gentoo.org>
Language-Team: Estonian <>
Language: et
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);
X-Generator: Poedit 2.0.6
  — GNOME seansihaldur %s [VALIKUD…] KÄSK

Käivita KÄSK, piirates selleks ajaks seansi võimekust.

  -h, --help        Selle abi kuvamine
  --version         Rakenduse versiooni kuvamine
  --app-id ID       Kasutatava rakenduse ID
                    piirangu ajal (valikuline)
  --reason PÕHJUS   Piirangu põhjus (valikuline)
  --inhibit ARG     Piiratavad asjad, koolonitega loetelu:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Ei käivitata KÄSKU, selle asemel oodatakse igavesti

Kui ühtegi --inhibit valikut pole määratud, jääb idle (jõudeolek).
 %s jaoks on vajalik argument
 Esines viga ja süsteem ei suuda taastuda.
Logi välja ja proovi uuesti. Esines viga ja süsteem ei suuda taastuda. Ettevaatusabinõuna keelati kõik laiendused. Esines viga ja süsteem ei suuda taastuda. Palun võta ühendust süsteemihalduriga Seanss nimega „%s“ on juba olemas AUTOMAATKÄIVITUS_KAUST Väljalogimine on lubatud Seansihalduriga pole võimalik ühenduda ICE kuulamissoklit pole võimalik luua: %s Kohandatud Kohandatud seanss Riistvarakiirenduse kontrolli keelamine Kasutaja määratud rakendusi ei laadita Kinnitust pole vaja küsida Silumiskoodi lubamine %s käivitamine nurjus
 GNOME GNOME libaseanss GNOME Xorg’iga Kõiki olemasolevaid piiranguid eiratakse Väljalogimine Ei vasta Oo ei!  Miski läks nihu. Standardsete automaatkäivituse kataloogide asemel muude kataloogide kasutamine Palun vali kohandatud seanss, mida käivitada Väljalülitamine Rakendus kutsuti välja vastuoluliste parameetritega Taaskäivita Uue kliendi ühendus lükati tagasi, kuna hetkel jäetakse seanss seisma
 Meelde jäetud rakendus _Muuda seansi nime SEANSI_NIMI Seanss %d Seansi nimed ei tohi sisaldada kaldkriipsu (/) Seansi nimed ei tohi alata punktiga (.) Seansi nimed ei tohi alata punktiga (.) ega sisaldada kaldkriipsu (/) Kasutatav seanss Laienduse hoiatuse kuvamine Testimise jaoks kuvatakse nurjumise dialoogi Selle abil saab valida salvestatud seansi See rakendus takistab väljalogimist. Selle seansiga logitakse sind GNOME keskkonda sisse Rakenduse versioon _Jätka Logi _välja _Logi välja _Uus seanss _Eemalda seanss 