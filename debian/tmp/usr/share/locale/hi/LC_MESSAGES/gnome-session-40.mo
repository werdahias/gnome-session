��    <      �  S   �      (     )  4  F     {  R   �  g   �  Z   O  '   �     �     �     �  	   	  	   	  	   	  (   )	  )   R	     |	     �	  '   �	  "   �	     �	     �	     	
     
     
      ,
     M
     U
  !   d
     �
  '   �
  %   �
  	   �
  '   �
       P        f     }     �  
   �  ;   �  3   �  Q        g     v  &   �     �  *   �      �        /   2     b  =   ~  	   �     �     �     �     �  (   �       �  %  *   �  1    F   M  �   �    f  �   z  P   H     �  Y   �  6     %   8     ^     t  {   �  W        d     w  h   �  o      ?   p  I   �     �       &     p   E     �  P   �  :        V  t   i  b   �     A  i   X     �  �   �  7   �  2   �              o      �   �   �   !  &   �!  >   �!  a   *"  7   �"  �   �"  Q   O#  G   �#  y   �#  ?   c$  �   �$     5%     T%     m%     �%     �%  d   �%  '   &     $             3       0                  4                    -       	           7          1   )           #   *      (                 "                &   +      /         5   6            9   ;         <       2           '   !           ,             :   .   %       
                   8        - the GNOME session manager %s [OPTION...] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can't recover.
Please log out and try again. A problem has occurred and the system can't recover. All extensions have been disabled as a precaution. A problem has occurred and the system can't recover. Please contact a system administrator A session named ‘%s’ already exists AUTOSTART_DIR Additional startup _programs: Allow logout Browse… Co_mmand: Comm_ent: Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Do not load user-specified applications Don't prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Wayland Ignoring any existing inhibitors Log out Not responding Oh no!  Something has gone wrong. Options Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Session %d Session names are not allowed to contain ‘/’ characters Session names are not allowed to start with ‘.’ Session names are not allowed to start with ‘.’ or contain ‘/’ characters Session to use Show extension warning Show the fail whale dialog for testing Startup Programs This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME This session logs you into GNOME, using Wayland Version of this application _Automatically remember running applications when logging out _Continue _Log Out _Log out _Name: _New Session _Remember Currently Running Applications _Remove Session Project-Id-Version: gnome-session.master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-session&keywords=I18N+L10N&component=general
PO-Revision-Date: 2014-09-20 22:10+0630
Last-Translator: rajesh <rajesh>
Language-Team: Hindi <kde-i18n-doc@kde.org>
Language: hi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
net>
X-Generator: Lokalize 1.5
Plural-Forms: nplurals=2; plural=(n!=1);




 - GNOME सत्र प्रबंधक %s [विकल्प...] कमांड

जबकि बाधा कुछ सत्र कार्यक्षमता आदेश पर अमल..

  -h, --मदद        इस मदद को दिखाएँ
  --संस्करण         प्रोग्राम संस्करण को दिखाएँ
  --अनुप्रयोग आईडी आईडी       अनुप्रयोग के लिए उपयोग आईडी
                   बाधा जब (वैकल्पिक)
  --कारण कारण   बाधा के लिए कारण (वैकल्पिक)
  --ARG को बाधित     हालात को बाधित, बृहदान्त्र अलग की सूची:
                    लॉगआउट, स्विच उपयोगकर्ता को निलंबित, निष्क्रिय, स्वारोहित
  --को बाधित केवल    शुरू करने और आदेश नहीं हमेशा के बजाय इंतजार

अगर कोई विकल्प को बाधित करने के लिए निर्दिष्ट किया जाता है, निष्क्रिय माना जाता है.
 % s ने एक तर्क की आवश्यकता है
 एक समस्या आई और तंत्र रिकवर नहीं कर सकता है. 
कृपया लॉग आउट करें और फिर कोशिश करें. एक समस्या आई और तंत्र रिकवर नहीं कर सकता है. सभी विस्तारों को सावधानी के लिहाज से निष्क्रिय कर दिया गया है. एक समस्या आई और तंत्र रिकवर नहीं कर सकता है. कृपया तंत्र प्रशासक से संपर्क करें '%s' नामक सत्र पहले से ही मौजूद है AUTOSTART_DIR प्रारंभिक प्रोग्राम में जोड़ें (_p) लॉगआउट की अनुमति दें ब्राउज़ करें… कमांड (_m): टिप्पणी (_e): सत्र मैनेजर से संपर्क स्थापित नहीं हो पा रहा है ICE लाइसेंसिंग सॉकेट बना नहीं सका: %s मनपसंद मनपसंद सत्र उपयोक्ता विशिष्ट अनुप्रयोग मत लोड करें उपयोक्ता संपुष्टि के लिए प्राँप्ट मत करें डिबगिंग कोड सक्रिय करें %s कार्यान्वित करने में असफल
 गनोम गनोम डमी गनोम वेलैंड पर किसी मौजूदा इनहिबिटर्स को अनदेखा कर रहा है लॉग आउट कोई प्रतिक्रिया नहीं दे रहा है नहीं! कुछ गलत हो गया है. विकल्प मानक स्वतः आरंभ निर्देशिका अध्यारोहित करें चलाने के लिए एक कस्टम सत्र का चयन करें बंद करें विरोधी विकल्प के साथ लाया गया प्रोग्राम रिबूट करें नया क्लाइंट कनेक्शन अस्वीकार कर रहा है क्योंकि सत्र शट डाउन हो रहा है
 स्मृतिस्थ अनुप्रयोग सत्र का नाम बदलें (_m) SESSION_NAME सत्र %d सत्र नाम '/' अक्षर होते हैं की अनुमति नहीं है सत्र के नाम  ‘.’ के साथ शुरू करने की अनुमति नहीं है.  सत्र के नाम के साथ शुरू करने की अनुमति दी जाती है या '.' '/' अक्षर होते हैं उपयोग में सत्र विस्तार चेतावनी दिखाएं फेल ह्वेल संवाद को जाँच के लिए दिखाएँ प्रारंभिक प्रोग्राम इस प्रविष्टि की मदद से आप एक सहेजी गई सत्र का चयन करें यह प्रोग्राम लॉगआउट रोक रहा है. यह सत्र गनोम में लॉगइन होगा यह सत्र गनोम में लॉगइन होगा, वेलैंड के उपयोग से इस अनुप्रयोग का संस्करण चलते अनुप्रयोग को स्वतः याद रखें जब लॉग आउट हो रहे हों (_A) जारी रखें (_C) लॉग आउट (_L) लॉग आउट (_L) नाम (_N): नया सत्र (_N) हाल में कार्यशील अनुप्रयोग याद रखें (_R) सत्रें हटाएं (_R) 