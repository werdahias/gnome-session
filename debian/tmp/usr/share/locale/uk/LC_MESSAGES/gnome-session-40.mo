��    D      <  a   \      �     �  p        q  T   �  i   �  \   I	  '   �	     �	     �	  (   �	  )   
     <
     C
  #   R
  '   v
  $   �
     �
     �
     �
     �
       q     G   �  >   �           )     1     ?  !   N  '   p  %   �  	   �  '   �  #   �       P        l     �  %   �  M   �          "     /     =  
   O  ;   Z  3   �  Q   �          +  &   B     i  +   �  #   �  P   �  *   $      O      p  B   �     �     �  p     	   �     �     �     �     �    �  ,   �  0  �  %   -  �   S  �   �  �   �  3   O  !   �     �  P   �  X        m     v  T   �  X   �  N   >  0   �  (   �     �     �     
  �      �     f   �  B        b  A   �     �  $   �  W      @   Y   "   �   R   �   S   !     d!     �!  *   "  &   ."  B   U"  �   �"  *   @#     k#     �#  "   �#     �#  W   �#  Q   ,$  |   ~$  5   �$  4   1%  b   f%  \   �%  Q   &&  0   x&  {   �&  Q   %'  @   w'  '   �'  �   �'  B   m(  &   �(  �   �(     �)     �)     �)     *     (*         !                    #       *   9   6   4           0   )              8       (      '   $   %           >                     	          -          A   D   ,                                        B              ?   ;              :      
       +   .   3          /   5   1           2   =          @      "      C                7   &      <        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2020-07-31 17:06+0300
Last-Translator: Yuri Chornoivan <yurchor@ukr.net>
Language-Team: Ukrainian <trans-uk@lists.fedoraproject.org>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Lokalize 20.07.70
X-Project-Style: gnome
  — керування сеансом GNOME %s [ПАРАМЕТР…] КОМАНДА

Виконує КОМАНДУ, коли гальмується якась функціональність сеансу.

  -h, --help        Показати цю довідку
  --version         Показати версію програми
  --app-id ІД       Ідентифікатор програми, який використовується
                    під час гальмування (необов'язково)
  --reason ПРИЧИНА  Причина для гальмування (необов'язково)
  --inhibit ПУНКТ   Пункти гальмування, список, розділений комою:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Не запускати КОМАНДУ, натомість завжди чекати
  -l, --list        Вивести список наявних гальмувань і вийти

Якщо не вказано параметра --inhibit, допускається бездіяльність.
 %s потребує аргумент
 Виникла помилка і систему неможливо відновити.
Будь ласка, вийдіть і спробуйте знову. Виникла помилка і систему неможливо відновити. Всі розширення вимкнуло як запобіжний засіб. Виникла помилка і систему неможливо відновити. Будь ласка, зв'яжіться з системним адміністратором Сеанс з назвою «%s» вже існує САМОЗАПУСКНА_ТЕКА Дозволити вихід Не вдалось з'єднатись із менеджером сеансів Не вдалось створити гніздо прослуховування ICE: %s Інше Власний сеанс Вимкнути перевіряння на апаратне прискорення Не завантажувати вибрані користувачем програми Не вимагати підтвердження від користувача Увімкнути код зневадження Не вдалося виконати %s
 GNOME Несправжній GNOME GNOME через Xorg Якщо увімкнено, gnome-session показуватиме вікно попередження після входу до системи, якщо сеанс автоматично повернуто до резервного варіанта. Якщо увімкнено, то gnome-session буде попереджувати користувача перед завершенням сеансу. Якщо увімкнено, gnome-session автоматично зберігатиме сеанси. Ігнорувати будь-які наявні перепони Завершити сеанс Попередження про завершення сеансу Не відповідає Отакої!  Щось не так. Перевизначити стандартні каталоги автозапуску Виберіть власний сеанс для запуску Вимкнути комп'ютер Програму запущено з несумісними параметрами Програмі слід передавати точно один параметр Перезавантажити Відхиляються нові клієнти, бо система знаходиться у стані вимкнення.
 Запам'ятована програма _Перейменувати сеанс Перезапустити dbus.service, якщо запущено Запустити з ExecStopPost, щоб gnome-session-failed.target запускалася, якщо станеться критична помилка служби Запущено як службу systemd НАЗВА_СЕАНСУ Зберігати сеанси Зберегти цей сеанс Сеанс %d Назвам сеансів не дозволено містити символи «/» Назвам сеансів не дозволено починатись з «.» Назвам сеансів не дозволено починатись з «.» або містити символи «/» Сеанси, які використовуються Показати зауваги розширення Показувати величезне вікно помилок для випробування  Показувати попередження щодо резервного варіанта Сигнал завершення ініціалізації для gnome-session Запустити gnome-session-shutdown.target Запустити gnome-session-shutdown.target, якщо отримано EOF або одинарний байт у stdin Цей запис дозволяє вибрати збережений сеанс Ця програма блокує вихід з системи. Це — сеанс входу в GNOME Використовувати вбудоване керування сеансами (а не керування на основі systemd) Використати керування сеансами systemd Версія цієї програми Якщо увімкнено, gnome-session автоматично збереже наступний сеанс під час виходу із системи, навіть якщо автоматичне збереження вимкнено. Про_довжити _Завершити сеанс Ви_йти _Створити сеанс _Вилучити сеанс 