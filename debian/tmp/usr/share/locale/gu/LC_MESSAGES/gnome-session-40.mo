��    3      �  G   L      h     i  4  �     �  R   �  g   '  Z   �  '   �             (   -  )   V     �     �  #   �  '   �  "   �     	     	     1	     7	     C	      Q	     r	     z	  !   �	  '   �	  %   �	  	   �	  '   
     +
  P   2
     �
     �
     �
  
   �
  ;   �
  3   �
  Q   2     �     �  &   �  *   �      �           >  	   Z     d     m     v     �  �  �  >   |  i  �  1   %    W  
  j    u  r   x     �  -   �  `   '  d   �     �  "     a   &  �   �  X     C   j  C   �     �     �       n        �  #   �  C   �  t     �   �       �   #     �  �   �  5   o  1   �     �     �  z   �  r   o  �   �  ,   �  A   �  e      �   l   t   �   Q   n!  ;   �!     �!  !   "  !   ="     _"  +   ~"                                   	   "       $   2      &                *               0   -                          3       '   !                1                     
   #       +                        /                    (   )         %   .   ,     - the GNOME session manager %s [OPTION...] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can't recover.
Please log out and try again. A problem has occurred and the system can't recover. All extensions have been disabled as a precaution. A problem has occurred and the system can't recover. Please contact a system administrator A session named ‘%s’ already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don't prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg Ignoring any existing inhibitors Log out Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Session %d Session names are not allowed to contain ‘/’ characters Session names are not allowed to start with ‘.’ Session names are not allowed to start with ‘.’ or contain ‘/’ characters Session to use Show extension warning Show the fail whale dialog for testing This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Version of this application _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gu
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-session&keywords=I18N+L10N&component=general
PO-Revision-Date: 2016-07-11 22:10+0200
Last-Translator: વિશાલ ભલાણી <vishalbhalani89@gmail.com>
Language-Team: American English <kde-i18n-doc@kde.org>
Language: gu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
Plural-Forms: nplurals=2; plural=(n!=1);
  - જીનોમ સત્ર વ્યવસ્થાપક %s [OPTION...] COMMAND

COMMAND ચલાવો જ્યારે અમુક સત્ર કાર્યક્ષમતાઓને અવરોધી રહ્યુ હોય.

  -h, --help        આ મદદને બતાવો
  --version         કાર્યક્રમ આવૃત્તિને બતાવો
  --app-id ID       વાપરવા માટે કાર્યક્રમ id
                    જ્યારે અવરોધી રહ્યા હોય (વૈકલ્પિક)
  --reason REASON   અવરોધવા માટે કારણ (વૈકલ્પિકl)
  --inhibit ARG     અવરોધવા માટેની વસ્તુઓ, અવતરણ ચિહ્નથી અલગ થયેલ યાદી:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    COMMAND શરૂ કરો નહિં અને તેને બદલે હંમેશા રાહ જુઓ

If no --inhibit વિકલ્પ સ્પષ્ટ થયેલ છે, નિષ્ક્રિય તરીકે ધાર્યુ હતુ.
 %s ને દલીલની જરૂર છે
 સમસ્યા ઉદ્ભવી અને સિસ્ટમને પુન:પ્રાપ્ત કરી શક્યા નહિં.
 મહેરબાની કરીને બહાર નીકળો અને ફરીથી પ્રયત્ન કરો. સમસ્યા ઉદ્ભવી અને સિસ્ટમને પુન:પ્રાપ્ત કરી શક્યા નહિં. બધા એક્સટેન્શન એક સાવચેતી તરીકે નિષ્ક્રિય છે. સમસ્યા ઉદ્ભવી અને સિસ્ટમને પુન:પ્રાપ્ત કરી શક્યા નહિં. મહેરબાની કરીને સિસ્ટમ સંચાલકનો સંપર્ક કરો નામ થયેલ સત્ર ‘%s’ પહેલેથી અસ્તિત્વ ધરાવે છે AUTOSTART_DIR logout ની પરવાનગી આપો સત્ર સંચાલક સાથે સંપર્ક થઈ શક્યો નથી ICE સાંભળવાની સોકેટ ને બનાવી શકાતી નથી: %s વૈવિધ્ય વૈવિધ્ય સત્ર હાર્ડવેર પ્રવેગકની તપાસણી અક્ષમ કરો વપરાશકર્તા-સ્પષ્ટ થયેલ કાર્યક્રમો ને લોડ કરાતા નથી વપરાશકર્તા ખાતરી માટે પૂછશો નહિં ડિબગીંગ કોડ ને સક્રિય કરો %s ને ચલાવતી વખતે નિષ્ફળતા
 GNOME GNOME ડમી Xorg પર GNOME કોઈપણ વર્તમાન ઇન્હિબિટર્સ અવગણી રહ્યા છે બહાર નીકળો જવાબ આપતુ નથી અરે નહિં! કંઇક ખોટુ થઇ ગયુ. પ્રમાણભૂત આપોઆપશરૂઆત ડિરેક્ટરીઓ પર ફરી લખો મહેરબાની કરીને ચલાવવા માટે વૈવિધ્ય સત્રને પસંદ કરો પાવર બંધ તકરાતા વિકલ્પો સાથે કાર્યક્રમ બોલાવવામાં આવ્યો રીબુટ નવા ક્લાઇન્ટ જોડાણને ના પાડી રહ્યા છે કારણ કે સત્ર એ હાલમાં બંધ થયેલ છે
 યાદ રાખેલ કાર્યક્રમ સત્રનું નામ બદલો (_m) SESSION_NAME સત્ર %d સત્ર નામો ‘/’ અક્ષરોને સમાવવા પરવાનગી આપતુ નથી સત્ર નામો ‘.’ સાથે શરૂ કરવા પરવાનગી આપતા નથી સત્ર નામો ‘.’ સાથે શરૂ થવા પરવાનગી આપતા નથી અથવા ‘/’ અક્ષરોને સમાવે છે વાપરવા માટે સત્ર ઍક્સટેન્શન ચેતવણી બતાવો પરીક્ષણ માટે નિષ્ફળ વ્હેલ સંવાદ બતાવો આ નોંધણી કે જે તમને સંગ્રહ થયેલ સત્રને પસંદ કરવા દે છે આ કાર્યક્રમ એ બહાર નીકળવાને અટકાવી રહ્યો છે. આ સત્ર તમને GNOME માં પ્રવેશ આપે છે આ કાર્યક્રમની આવૃત્તિ ચાલુ રાખો (_C) બહાર નીકળો (_L) બહાર નીકળો (_L) નવું સત્ર (_N) સત્રને દૂર કરો (_R) 