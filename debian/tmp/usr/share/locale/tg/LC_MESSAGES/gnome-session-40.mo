��    =        S   �      8     9  4  V     �  R   �  g   �  Z   _  '   �     �     �     	  	   	  	   %	  	   /	  (   9	  )   b	     �	     �	  #   �	  '   �	  "   �	     
     '
     =
     C
     O
      `
     �
     �
  !   �
     �
  '   �
  %   �
  	     '        B  P   I     �     �     �  
   �  ;   �  3     Q   I     �     �  &   �     �  *   �      $      E  /   f     �  =   �  	   �     �                 (         I  �  Y  $     b  4  2   �  �   �  �   y  �   Z  C        X  8   f  (   �     �     �  
   �  M   �  X   M     �     �  R   �  S   3  =   �  L   �          *     0     A  T   V     �     �  -   �     
  [     a   u     �  Q   �     A  �   X  ;   �  .   0     _     l  X   ~  P   �  �   (  0   �  3   �  b     %   ~  �   �  J   3   C   ~   ^   �   "   !!  �   D!     �!     �!      "     "     "  E   4"  %   z"     %                    1                  5                    .       	           8          2   *   4       $   +      )                 #                '   ,      0         6   7            :   <         =       3           (   "   !       -              ;   /   &       
                   9        - the GNOME session manager %s [OPTION...] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can't recover.
Please log out and try again. A problem has occurred and the system can't recover. All extensions have been disabled as a precaution. A problem has occurred and the system can't recover. Please contact a system administrator A session named ‘%s’ already exists AUTOSTART_DIR Additional startup _programs: Allow logout Browse… Co_mmand: Comm_ent: Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don't prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Wayland Ignoring any existing inhibitors Log out Not responding Oh no!  Something has gone wrong. Options Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Session %d Session names are not allowed to contain ‘/’ characters Session names are not allowed to start with ‘.’ Session names are not allowed to start with ‘.’ or contain ‘/’ characters Session to use Show extension warning Show the fail whale dialog for testing Startup Programs This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME This session logs you into GNOME, using Wayland Version of this application _Automatically remember running applications when logging out _Continue _Log Out _Log out _Name: _New Session _Remember Currently Running Applications _Remove Session Project-Id-Version: Tajik Gnome
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-session&keywords=I18N+L10N&component=general
PO-Revision-Date: 2015-03-10 10:58+0500
Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>
Language-Team: 
Language: tg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=1;
X-Generator: Poedit 1.6.5
  - мудири ҷаласаи GNOME %s [OPTION...] ФАРМОН

Ҳангоми сусткунии баъзе функсияҳои ҷаласа, ФАРМОНРО иҷро кунед.

  -h, --help        Барои намоиш додани кӯмак
  --version         Барои намоиш додани версияи барнома
  --app-id ID       Рамзи барнома барои истофода
                    ҳангоми сусткунии ҷаласа (интихобӣ)
  --reason REASON   Сабаби сусткунии ҷаласа (интихобӣ)
  --inhibit ARG     Объектҳое, ки суст мешаванд, рӯйхат (бо ду нуқта тақсимшуда):
                    баромад (logout), қатъу васл кардани корбар (switch-user), таваққуф (suspend), бефаъолият (idle), васлкунии худкор (automount)
  --inhibit-only    ФАРМОНРО иҷро накунед ва доимо интизор шавед

Агар имконоти --inhibit муайян нашавад, ҳолати бефаъолият истифода мешавад.
 %s аргументро талаб мекунад
 Хатогӣ ба вуҷуд омад ва система барқарор карда натавонист.
Лутфан, бароед ва аз нав ворид шавед. Хатогӣ ба вуҷуд омад ва система барқарор карда натавонист. Ба сабабҳои бехатарӣ ҳамаи пасвандҳо ғайрифаъол карда шудаанд. Хатогӣ ба вуҷуд омад ва система барқарор карда натавонист. Лутфан, бо маъмури система дар тамос шавед Номи ҷаласаи ‘%s’ аллакай мавҷуд аст AUTOSTART_DIR Барномаҳои худоғозии _иловагӣ: Иҷозат додани баромад Тамошо кардан… _Фармон: _Шарҳ: Пайваст бо мудири ҷаласа имконнопазир аст Эҷоди бастагоҳи мунтазири ICE имконнопазир аст: %s Фармоиш додан Ҷаласаи фармоишӣ Ғайрифаъол кардани санҷиши шитоби сахтафзор Бор накардани барномаҳои таъиншуда бо корбар Дархост накардани тасдиқи корбар Фаъол кардани ислоҳи хатоҳои барномарезӣ %s иҷро нашуд
 GNOME Сохти GNOME GNOME дар Wayland Рад кардани ҳамаи сусткунандагони мавҷудбуда Баромадан Ҷавоб намедиҳад Ваҳ! Чизе вайрон шудааст. Имконот Аз нав таъинкунии феҳристҳои худоғозии стандартӣ Лутфан, ҷаласаи фармоиширо барои иҷро интихоб намоед Хомӯш кардан Барнома бо ихтилофи имконот дархост шудааст Бозоғозидан Пайвасти муштарии нав рад шуда истодааст, зеро ки ҷаласа дар ҳоли анҷоми кор мебошад
 Барномаҳои ба хотир гирифташуда _Иваз кардани номи ҷаласа SESSION_NAME Ҷаласаи %d Номи ҷаласа бояд аломати ‘/’ -ро дар бар нагирад Номи ҷаласа бояд бо аломати ‘.’ сар нашавад Номи ҷаласа бояд аломати ‘/’ -ро дар бар нагирад ё бо аломати ‘.’ сар нашавад Ҷаласа барои истифодабарӣ Намоиш додани огоҳи пасванд намоиш додани равзарани гуфтугӯи хатоҳо барои санҷиш Барномаҳои худоғозӣ Ин вуруд ба шумо имкон медиҳад, ки тавонед ҷаласаи захирашударо интихоб кунед Ин барнома баромади шуморо манъ мекунад. Ин ҷаласа шуморо ба GNOME ворид мекунад Ин ҷаласа шуморо ба GNOME тавассути Wayland ворид мекунад Версияи ин барнома _Ҳангоми баромад аз система барномаҳои кушодаро ба таври худкор ба хотир гиред _Идома додан _Баромадан _Баромадан _Ном: _Ҷаласаи нав _Ба хотир гирифтани барномаҳои кушода _Тоза кардани ҷаласа 