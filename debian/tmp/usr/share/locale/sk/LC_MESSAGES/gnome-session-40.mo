��    C      4  Y   L      �     �  4  �       T     i   s  \   �  '   :	     b	     p	  (   }	  )   �	     �	     �	  #   �	  '   

  $   2
     W
     m
     �
     �
     �
  q   �
  G     >   ]      �     �     �     �  !   �  '     %   ,  	   R  '   \  #   �     �  P   �             %   '     M     h     u     �  
   �  ;   �  3   �  Q        b     q  &   �     �  +   �  #   �  P     *   j      �      �  B   �          9  p   U  	   �     �     �     �     �  �  �     �  �  �     f  ]   �  f   �  Y   K  (   �     �     �  .   �  3   (     \     n  +   �  5   �  )   �          !     @     F     W  s   f  N   �  B   )  +   l     �     �     �     �  ;   �  :   /     j  (     (   �     �  J   �     1     I  #   _     �     �     �     �     �  0   �  ,     I   J     �  !   �  ;   �  *     8   =  %   v  f   �  7     "   ;  &   ^  R   �  "   �     �  �         �      �      �      �      �      +   7      >   C                0   %          5   6   #       8      =   4              *          2       <   ?                               1                 -   A   :          '             9   (              3       	              /          !   )   $         ,   .   ;   
         &           @            B   "        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2020-03-18 14:05+0100
Last-Translator: Dušan B. <drunkez@gmail.com>
Language-Team: Slovak <gnome-sk-list@gnome.org>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 1 : (n>=2 && n<=4) ? 2 : 0;
X-Generator: Poedit 2.3
  — správca relácie GNOME %s [VOĽBA...] PRÍKAZ

Spustenie PRÍKAZU počas potlačenia niektorých funkcií relácie.

  -h, --help        Zobrazí tohoto pomocníka
  --version         Zobrazí verziu programu
  --app-id ID       Identifikátor aplikácie, ktorá sa použije
                    pri potlačení (voliteľné)
  --reason PRÍČINA  Príčina, kvôli ktorej sa uskutoční potlačenie
                    (voliteľné)
  --inhibit PARAM   Dvojbodkou oddelené súčasti na potlačenie, z možností:
                    logout (odhlásenie)
                    switch-user (prepnutie používateľa)
                    suspend (uspatie)
                    idle (nečinnosť)
                    automount (automatické pripojenie)
  --inhibit-only    Nespustí PRÍKAZ, ale bude miesto toho čakať do
                    nekonečna

Ak nie je určená žiadna voľba typu --inhibit, bude sa predpokladať nečinnosť.
 Príkaz %s vyžaduje parameter
 Vyskytla sa chyba a systém sa nedokáže obnoviť.
Prosím, odhláste sa a skúste to znovu. Vyskytla sa chyba a systém sa nedokáže obnoviť. Všetky rozšírenia boli preventívne zakázané. Vyskytla sa chyba a systém sa nedokáže obnoviť. Prosím, kontaktuje správcu systému Relácia s názvom „%s“ už existuje ADRESÁR_AUTOSPUSTENIA Povolí odhlásenie Nepodarilo sa pripojiť k správcovi relácií Nepodarilo sa vytvoriť načúvajúci soket ICE: %s Vlastná relácia Vlastná relácia Zakáže kontrolu hardvérovej akcelerácie Nenačíta aplikácie špecifikované používateľom Nepýtať si potvrdenie od používateľa Povolí ladiaci kód Zlyhalo spustenie príkazu %s
 GNOME Fingované GNOME GNOME cez Xorg Ak je povolené, gnome-session zobrazí po prihlásení upozornenie, ak sa automaticky použije záchranný režim. Ak je povolené, gnome-session vyzve používateľa pred ukončením relácie. Ak je povolené, gnome-session bude reláciu ukladať automaticky. Budú sa ignorovať akékoľvek potlačenia Uskutoční odhlásenie Výzvna na odhlásenie Neodpovedá Ale nie! Niekde sa stala chyba. Preváži predvolené adresáre automatického spúšťania Prosím, vyberte vlastnú reláciu, ktorá sa má spustiť Uskutoční vypnutie Program volaný s konfliktnými voľbami Program vyžaduje presne jeden parameter Uskutoční reštart Nové pripojenie klienta odmietnuté, pretože sa relácia práve vypína
 Zapamätaná aplikácia Pre_menovať reláciu Reštartuje dbus.service, ak beží Pobeží ako služba systemd NÁZOV_RELÁCIE Ukladanie relácie Uloženie tejto relácie Relácia č. %d Názvy relácií nesmú obsahovať znaky „/“ Názvy relácií nesmú začínať s „.“ Názvy relácií nesmú začínať s „.“ ani obsahovať znaky „/“ Relácia, ktorá bude použitá Zobrazí upozornenie rozšírenia Zobrazí dialógové okno s chybou slúžiace na testovanie Zobrazí upozornenie záchranného režimu Signalizuje dokončenie inicializácie pre gnome-session Spustí gnome-session-shutdown.target Spustí gnome-session-shutdown.target pri obdržaní EOF alebo jedného bajtu zo štandardného vstupu Táto položka vám umožní vybrať uloženú reláciu Tento program blokuje odhlásenie. Táto relácia vás prihlási do GNOME Použije zabudovaného správcu relácií (namiesto toho, založeného na systemd) Použije správu relácií systemd Verzia tejto aplikácie Keď je povolené, gnome-session automaticky uloží nasledujúcu reláciu pri odhlásení, aj keď je automatické ukladanie zakázané. _Pokračovať _Odhlásiť sa Odh_lásiť sa _Nová relácia _Odstrániť reláciu 