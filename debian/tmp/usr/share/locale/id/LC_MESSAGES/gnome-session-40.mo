��    D      <  a   \      �     �  p        q  T   �  i   �  \   I	  '   �	     �	     �	  (   �	  )   
     <
     C
  #   R
  '   v
  $   �
     �
     �
     �
     �
       q     G   �  >   �           )     1     ?  !   N  '   p  %   �  	   �  '   �  #   �       P        l     �  %   �  M   �          "     /     =  
   O  ;   Z  3   �  Q   �          +  &   B     i  +   �  #   �  P   �  *   $      O      p  B   �     �     �  p     	   �     �     �     �     �  �  �     k  �  �     1  T   H  l   �  V   
     a     ~     �  '   �  )   �     �     �  2     1   >  #   p     �     �     �     �     �  }   �  P   c  C   �     �               2  8   ?  #   x  5   �     �  /   �  -   
     8  M   G     �     �  -   �  [   �      E  	   f     p     |     �  +   �  *   �  G   �     3     E  +   c      �  1   �  #   �  O     =   V  #   �  %   �  =   �          ;  �   N  
   �     �     �  
   �               !                    #       *   9   6   4           0   )              8       (      '   $   %           >                     	          -          A   D   ,                                        B              ?   ;              :      
       +   .   3          /   5   1           2   =          @      "      C                7   &      <        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session master
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2020-07-31 22:49+0700
Last-Translator: Kukuh Syafaat <kukuhsyafaat@gnome.org>
Language-Team: Indonesian <gnome-l10n-id@googlegroups.com>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 2.4
  — manajer sesi GNOME %s [OPSI…] PERINTAH

Mengeksekusi PERINTAH sambil mencegah beberapa fungsionalitas sesi.

  -h, --help        Tampilkan bantuan ini
  --version         Tampilkan versi program
  --app-id ID       ID aplikasi untuk dipakai
                    ketika mencegah (opsional)
  --reason REASON   Alasan mencegah (opsional)
  --inhibit ARG     Hal yang dicegah, daftar yang dipisah titik dua dari:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Jangan meluncurkan PERINTAH dan tunggu selamanya
                    sebagai gantinya
  -l, --list        Daftar hambatan yang ada, dan keluar

Bila tak ada opsi --inhibit yang dinyatakan, diasumsikan idle.
 %s memerlukan argumen
 Terjadi masalah dan sistem tidak dapat dipulihkan.
Silakan log keluar dan coba lagi. Terjadi masalah dan sistem tidak dapat dipulihkan. Semua ekstensi dibuat nonaktif sebagai tindak pencegahan. Terjadi masalah dan sistem tidak dapat dipulihkan. Silakan kontak administrator sistem Nama sesi “%s” telah ada DIR_MULAI_OTOMATIS Izinkan log keluar Tak dapat terhubung dengan manajer sesi Tak dapat membuat soket pendengar ICE: %s Gubahan Sesi Gubahan Nonaktifkan pemeriksaan akselerasi perangkat keras Tak memuat aplikasi yang ditentukan oleh pengguna Jangan tanyakan konfirmasi pengguna Mengaktifkan kode debug Gagal mengeksekusi %s
 GNOME GNOME dummy GNOME pada Xorg Jika diaktifkan, gnome-session akan menampilkan dialog peringatan setelah log masuk jika sesi secara otomatis mundur kembali. Jika diaktifkan, gnome-session akan menanyakan pengguna sebelum mengakhiri sesi. Jika diaktifkan, gnome-session akan menyimpan sesi secara otomatis. Mengabaikan sembarang pencegah Keluar Tanyakan log keluar Tak merespon Oh, tidak!  Sesuatu yang tidak diinginkan telah terjadi. Menimpa direktori standar autostart Silakan pilih suatu sesi gubahan yang akan dijalankan Matikan Program dipanggil dengan opsi yang bertentangan Program membutuhkan satu parameter yang tepat Nyalakan Ulang Menolak sambungan dari klien baru, karena sesi sedang dalam proses dimatikan
 Aplikasi Yang Diingat Ubah Na_ma Sesi Mulai ulang dbus.service jika sedang berjalan Jalankan dari ExecStopPost untuk memulai gnome-session-failed.target pada kegagalan layanan Jalankan sebagai layanan systemd NAMA_SESI Simpan sesi Simpan sesi ini Sesi %d Nama sesi tak boleh memuat karakter “/” Nama sesi tak boleh diawali dengan “.” Nama sesi tak boleh diawali dengan “.” atau memuat karakter “/” Sesi yang dipakai Tunjukkan peringatan ekstensi Tampilkan dialog paus gagal untul pengujian Tampilkan peringatan penggantian Inisialisasi sinyal dilakukan untuk gnome-session Mulai gnome-session-shutdown.target Mulai gnome-session-shutdown.target saat menerima EOF atau satu byte pada stdin Entri ini memungkinkan Anda memilih suatu sesi yang tersimpan Program ini menghalangi log keluar. Sesi ini melogkan Anda ke dalam GNOME Gunakan manajemen sesi tertanam (bukan yang berbasis systemd) Gunakan manajemen sesi systemd Versi aplikasi ini Saat diaktifkan, gnome-session akan secara otomatis menyimpan sesi berikutnya saat log keluar bahkan jika penyimpanan otomatis dinonaktifkan. _Lanjutkan Ke_luar _Log keluar Sesi _Baru _Hapus Sesi 