��    D      <  a   \      �     �  p        q  T   �  i   �  \   I	  '   �	     �	     �	  (   �	  )   
     <
     C
  #   R
  '   v
  $   �
     �
     �
     �
     �
       q     G   �  >   �           )     1     ?  !   N  '   p  %   �  	   �  '   �  #   �       P        l     �  %   �  M   �          "     /     =  
   O  ;   Z  3   �  Q   �          +  &   B     i  +   �  #   �  P   �  *   $      O      p  B   �     �     �  p     	   �     �     �     �     �  �  �      i  �  �       U   (  z   ~  j   �  2   d     �     �  -   �  &   �  	          "   $  "   G     j     �     �     �     �     �  p   �  L   L  <   �  -   �                    %  $   D  ,   i     �  -   �      �     �  H   �     <     K  %   [  U   �     �     �            
   !  -   ,  )   Z  E   �     �     �  4   �      *  .   K  #   z  X   �  7   �     /  $   N  E   s      �     �  �   �  	   u          �     �     �         !                    #       *   9   6   4           0   )              8       (      '   $   %           >                     	          -          A   D   ,                                        B              ?   ;              :      
       +   .   3          /   5   1           2   =          @      "      C                7   &      <        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session.HEAD
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2020-09-06 19:21+0200
Last-Translator: Alan Mortensen <alanmortensen.am@gmail.com>
Language-Team: Danish <dansk@dansk-gruppen.dk>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
  — GNOME-sessionshåndteringen %s [TILVALG …] KOMMANDO

Kør KOMMANDO mens nogen sessionsfunktionalitet hindres.

  -h, --help        Vis denne hjælp
  --version         Vis programversionen
  --app-id ID       Det program-id der skal bruges
                    ved hindring (valgfrit)
  --reason GRUND    Grunden til hindring (valgfri)
  --inhibit ARG     Ting som skal hindres. Kolonsepareret liste af:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Start ikke kommandoen og vent i stedet for altid
  -l, --list        Vis de eksisterende hindringer og afslut

Hvis intet --inhibit tilvalg er angivet, vil “idle” blive antaget.
 %s kræver et argument
 Et problem er opstået og systemet kan ikke gendannes.
Log venligst ud og prøv igen. Der er opstået et problem, og systemet kan ikke gendannes. Alle udvidelser er blevet deaktiveret for en sikkerheds skyld. Der er opstået et problem og systemet kan ikke gendannes. Henvend dig venligst til en systemadministrator Der findes allerede en session med navnet “%s” AUTOSTART_KAT Tillad log ud Kunne ikke tilkoble til sessionshåndteringen Kunne ikke oprette ICE-lyttesoklen: %s Tilpasset Tilpasset session Slå hardwareaccelerationstjek fra Hent ikke brugerangivne programmer Opkræv ikke brugerbekræftelse Aktivér fejlfindingskode Kunne ikke køre %s
 GNOME GNOME-attrap GNOME på Xorg Hvis aktiveret vil gnome-session vise en advarselsdialog efter indlogning, hvis sessionen gik i reservetilstand. Hvis aktiveret vil gnome-session spørge brugeren før en session afsluttes. Hvis aktiveret vil gnome-session automatisk gemme sessionen. Ignorerer eventuelle eksisterende blokeringer Log ud Log ud-prompt Svarer ikke Åh nej!  Noget er gået galt. Tilsidesæt standard-autostartmapper Vælg venligst en tilpasset session at køre Sluk Program blev kaldt med selvmodsigende tilvalg Programmet kræver én parameter Genstart Nægter ny klientforbindelse fordi sessionen er ved at blive lukket ned
 Husket program _Omdøb session Genstart dbus.service hvis den kører Kør fra ExecStopPost for at starte gnome-session-failed.target ved fejlende tjeneste Kører som systemd-tjeneste SESSIONS_NAVN Gem sessioner Gem denne session Session %d Sessionsnavne må ikke indeholde “/”-tegn Sessionsnavne må ikke starte med “.” Sessionnavne må ikke starte med “.” eller indeholde “/”-tegn Session som skal bruges Vis udvidelsesfejl Vis dialogen uheldig hval for at gennemføre en test Vis advarsel for reservetilstand Signalinitialisering udført til gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target ved modtagelse af EOF eller en enkelt byte på stdin Dette indtastningsfelt lader dig vælge en gemt session Dette program blokerer log ud. Denne session logger dig ind i GNOME Brug indbygget sessionshåndtering (frem for den baseret på systemd) Brug systemd-sessionshåndtering Version af dette program Hvis aktiveret vil gnome-session automatisk gemme næste session ved udlogning, selv når automatisk gemmetilstand er slået fra. _Fortsæt _Log ud _Log ud _Ny session _Fjern session 