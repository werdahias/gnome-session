��    <      �  S   �      (     )  4  F     {  R   �  g   �  Z   O  '   �     �     �     �  	   	  	   	  	   	  (   )	  )   R	     |	     �	  '   �	  "   �	     �	     �	     	
     
     
      ,
     M
     U
  !   d
     �
  '   �
  %   �
  	   �
  '   �
       P        f     }     �  
   �  ;   �  3   �  Q        g     v  &   �     �  *   �      �        /   2     b  =   ~  	   �     �     �     �     �  (   �       �  %  C     4  G  F   |    �  @  �      Q     9   k  P   �  7   �     .     J     `  w   |  t   �     i     y  �   �  T   !  S   v  M   �       "   +  5   N  x   �  $   �  *   "  S   M     �  �   �  W   H  "   �  ~   �  $   B    g  I   y  3   �  !   �       �   ,     �  �   A   (   !  M   C!  v   �!  :   "  �   C"  y   �"  d   p#  �   �#  H   s$  �   �$  #   �%  )   �%  )   �%     
&  $   &  �   B&  -   �&     $             3       0                  4                    -       	           7          1   )           #   *      (                 "                &   +      /         5   6            9   ;         <       2           '   !           ,             :   .   %       
                   8        - the GNOME session manager %s [OPTION...] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can't recover.
Please log out and try again. A problem has occurred and the system can't recover. All extensions have been disabled as a precaution. A problem has occurred and the system can't recover. Please contact a system administrator A session named ‘%s’ already exists AUTOSTART_DIR Additional startup _programs: Allow logout Browse… Co_mmand: Comm_ent: Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Do not load user-specified applications Don't prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Wayland Ignoring any existing inhibitors Log out Not responding Oh no!  Something has gone wrong. Options Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Session %d Session names are not allowed to contain ‘/’ characters Session names are not allowed to start with ‘.’ Session names are not allowed to start with ‘.’ or contain ‘/’ characters Session to use Show extension warning Show the fail whale dialog for testing Startup Programs This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME This session logs you into GNOME, using Wayland Version of this application _Automatically remember running applications when logging out _Continue _Log Out _Log out _Name: _New Session _Remember Currently Running Applications _Remove Session Project-Id-Version: gnome-session.master.te
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-session&keywords=I18N+L10N&component=general
PO-Revision-Date: 2014-08-27 09:30+0530
Last-Translator: Praveen Illa <mail2ipn@gmail.com>
Language-Team: Telugu <Fedora-trans-te@redhat.com>
Language: te
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
Plural-Forms: nplurals=2; plural=(n!=1);
  -గ్నోమ్ సెషన్ నిర్వాహకము %s [OPTION...] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s కు వొక ఆర్గుమెంట్ కావాలి
 ఒక సమస్య ఎదురయింది మరియు వ్యవస్థ దానినుండి కోలుకోలేదు.
దయచేసి నిష్క్రమించి మరలా ప్రయత్నించండి. ఒక సమస్య ఎదురయింది మరియు వ్యవస్థ దానినుండి కోలుకోలేదు. ముందు జాగ్రత్తగా దయచేసి మీ వ్యవస్థ నిర్వాహకుడిని సంప్రదించండి ఒక సమస్య ఎదురయింది మరియు వ్యవస్థ దానినుండి కోలుకోలేదు. దయచేసి మీ వ్యవస్థ నిర్వాహకుడిని సంప్రదించండి సెషన్ పేరు ‘%s’ యిప్పటికే వుంది స్వయంప్రారంభ సంచయం (_D) అదనపు ప్రారంభ కార్యక్రమాలు (_p): నిష్క్రమణ అనుమతించు విహరించు... ఆదేశం (_m): వ్యాఖ్య (_e): సెషన్ నిర్వాహకానికి అనుసంధానించలేకపోతుంది ICE లిజనింగ్ సాకెట్‌ను సృష్టించలేక పోయింది: %s మలచిన మలచిన సెషన్ వాడుకరి-తెలిపిన అనువర్తనములను లోడుచేయలేకపోయింది వాడుకరి నిర్ధారణ కోసం అడగవద్దు దోషనిర్మూలన కోడ్‌ను చేతనపరుచు %s నిర్వర్తించుటకు విఫలమైంది
 గ్నోమ్ గ్నోమ్ డమ్మీ వేల్యాండ్ పై గ్నోమ్ ఉన్నటువంటి ఏ అవరోధకాలనైనా వదిలివేస్తున్నది నిష్క్రమించు స్పందించుటలేదు అయ్యో! ఎక్కడో పొరపాటు జరిగినది. ఐచ్చికములు ప్రామాణిక స్వయంచాలక డైరెక్టరీలను తిరిగివ్రాయుము నడుపుటకు మలచిన సెషన్ యెంపికచేయి విద్యుత్ ఆపు విభేదిస్తున్న ఐచ్చికాలతో పిలువబడే ప్రోగ్రామ్ పునఃప్రారంభం కొత్త కక్షిదారి అనుసంధానమును తిరస్కరిస్తోంది యెంచేతంటే విభాగము అనునది ప్రస్తుతము మూసివేయబడుతోంది
 గుర్తించుకున్న అనువర్తనము సెషన్ పేరుమార్చు (_m) సెషన్ పేరు (_N) సెషన్ %d సెషన్ పేర్లు  ‘/’ అక్షరాలను కలిగివుండుటకు అనుమతించబడవు సెషన్ పేర్లు  ‘.’ తో ప్రారంభమగుటకు అనుమతించబడవు సెషన్ పేర్లు  ‘.’ తో ప్రారంభమగుటకు లేదా ‘/’ అక్షరాలను కలిగివుండుటకు అనుమతించబడవు వాడవలసిన సెషన్ పొడిగింపు హెచ్చరికను చూపుము పరీక్షించుటకు ఫెయిల్ వేల్ డైలాగును చూపించు ప్రారంభ కార్యక్రమాలు ఈ ప్రవేశం అనునది మిమ్ములను దాచిన సెషన్ యెంపికచేయుటకు అనుమతించును ఈ ప్రోగ్రామ్ నిష్క్రమించుటను నిరోధిస్తోంది. ఈ సెషన్ గ్నోమ్‌లోనికి లాగ్ చేస్తుంది వేల్యాండ్ ఉపయోగించి, ఈ సెషన్ గ్నోమ్‌లోనికి లాగ్ చేస్తుంది ఈ అనువర్తనం యొక్క రూపాంతరం నిష్క్రమించేటప్పుడు నడుస్తున్న అనువర్తనాలను స్వయంచాలకంగా గుర్తుంచుకొనుము (_A) కొనసాగించు (_C) నిష్క్రమించు (_L) నిష్క్రమించు (_L) పేరు (_N): కొత్త సెషన్ (_N) ప్రస్తుతం నడుస్తున్న అనువర్తనాలు గుర్తుంచుకో (_R) సెషన్ తీసివేయి (_R) 