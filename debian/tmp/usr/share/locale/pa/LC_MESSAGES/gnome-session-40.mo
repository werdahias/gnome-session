��    D      <  a   \      �     �  p        q  T   �  i   �  \   I	  '   �	     �	     �	  (   �	  )   
     <
     C
  #   R
  '   v
  $   �
     �
     �
     �
     �
       q     G   �  >   �           )     1     ?  !   N  '   p  %   �  	   �  '   �  #   �       P        l     �  %   �  M   �          "     /     =  
   O  ;   Z  3   �  Q   �          +  &   B     i  +   �  #   �  P   �  *   $      O      p  B   �     �     �  p     	   �     �     �     �     �  �  �  7   l  �  �  ;   1  �   m  �   1  �   ,  [   �     R  "   `  `   �  U   �     :  (   G  X   p  i   �  I   3  #   }  *   �     �     �     �      �   0  �   �  �   �        "   %   #   H   6   l   m   �   D   !     V!  Y   j!  i   �!     ."  �   H"  <   #  '   @#  b   h#  �   �#  ?   `$     �$     �$  ,   �$     �$  W   
%  _   b%  �   �%  &   C&  >   j&  P   �&  8   �&  W   3'  4   �'  �   �'  s   N(  [   �(  g   )  n   �)  7   �)  6   -*  ?  d*     �+     �+     �+     �+     ,         !                    #       *   9   6   4           0   )              8       (      '   $   %           >                     	          -          A   D   ,                                        B              ?   ;              :      
       +   .   3          /   5   1           2   =          @      "      C                7   &      <        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session.HEAD
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2021-01-31 08:45-0800
Last-Translator: A S Alam <aalam@satluj.org>
Language-Team: Punjabi <punjabi-translation@googlegroups.com>
Language: pa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Lokalize 20.08.1
Plural-Forms: nplurals=2; plural=n != 1;
  — ਗਨੋਮ ਸ਼ੈਸ਼ਨ ਮੈਨੇਜਰ %s [OPTION…] COMMAND

ਕੁਝ ਸ਼ੈਸ਼ਨ ਸਹੂਲਤਾਂ ਨੂੰ ਰੋਕਣ ਲਈ COMMAND ਚਲਾਓ।

  -h, --help        ਇਹ ਮਦਦ ਵੇਖੋ
  --version         ਪਰੋਗਰਾਮ ਵਰਜ਼ਨ ਵੇਖੋ
  --app-id ID       ਰੋਕਣ ਦੇ ਦੌਰਾਨ ਵਰਤਣ ਲਈ ਐਪਲੀਕੇਸ਼ਨ
                    id (ਚੋਣਵਾਂ)
  --reason REASON   ਰੋਕਣ ਦਾ ਕਾਰਨ (ਚੋਣਵਾਂ)
  --inhibit ARG     ਰੋਕਣ ਲਈ ਚੀਜ਼ਾਂ, ਕੌਮੇ ਪਾ ਕੇ ਵੱਖ ਕੀਤਾ:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    COMMAND ਨਾ ਚਲਾਓ ਅਤੇ ਹਮੇਸ਼ਾਂ ਲਈ ਉਡੀਕੋ
  -l, --list        ਮੌਜੂਦਾ ਰੋਕਾਂ ਦੀ ਸੂਚੀ ਚਲਾਓ ਅਤੇ ਬੰਦ ਕਰੋ

If no --inhibit option is specified, idle is assumed.
 %s ਲਈ ਆਰਗੂਮੈਂਟ ਦੀ ਲੋੜ ਹੈ
 ਕੋਈ ਸਮੱਸਿਆ ਆਈ ਹੈ ਅਤੇ ਸਿਸਟਮ ਬਹਾਲ ਨਹੀਂ ਹੋ ਸਕਿਆ।
ਲਾਗ ਆਉਟ ਕਰਕੇ ਫੇਰ ਕੋਸ਼ਿਸ਼ ਕਰੋ। ਕੋਈ ਸਮੱਸਿਆ ਆਈ ਹੈ ਅਤੇ ਸਿਸਟਮ ਬਹਾਲ ਨਹੀਂ ਹੋ ਸਕਿਆ। ਬਚਾਅ ਵਜੋਂ ਸਭ ਇਕਸਟੈਸ਼ਨ ਨੂੰ ਅਸਮਰੱਥ ਕੀਤਾ ਜਾ ਚੁੱਕਾ ਹੈ। ਕੋਈ ਸਮੱਸਿਆ ਆਈ ਹੈ ਅਤੇ ਸਿਸਟਮ ਬਹਾਲ ਨਹੀਂ ਹੋ ਸਕਿਆ। ਆਪਣੇ ਸਿਸਟਮ ਪਰਸ਼ਾਸ਼ਕ ਨਾਲ ਸੰਪਰਕ ਕਰੋ “%s “ ਨਾਂ ਨਾਲ ਸ਼ੈਸ਼ਨ ਪਹਿਲਾਂ ਹੀ ਮੌਜੂਦ ਹੈ AUTOSTART_DIR ਲਾਗਆਉਟ ਮਨਜ਼ੂਰ ਸ਼ੈਸ਼ਨ ਮੈਨੇਜਰ ਨਾਲ ਜੁੜਿਆ ਨਹੀਂ ਜਾ ਸਕਿਆ ICE ਲਿਸਨਿੰਗ ਸਾਕਟ ਬਣਾਈ ਨਹੀਂ ਜਾ ਸਕੀ: %s ਕਸਟਮ ਪਸੰਦੀਦਾ ਸ਼ੈਸ਼ਨ ਹਾਰਡਵੇਅਰ ਐਕਸਰਲੇਸ਼ਨ ਚੈਕ ਅਸਮਰੱਥ ਕਰੋ ਖਾਸ ਵਰਤੋਂਕਾਰ ਐਪਲੀਕੇਸ਼ਨਾਂ ਨੂੰ ਲੋਡ ਨਾ ਕਰੋ ਵਰਤੋਂਕਾਰ ਪੁਸ਼ਟੀ ਲਈ ਨਾ ਪੁੱਛੋ ਡੀਬੱਗ ਕੋਡ ਯੋਗ %s ਚਲਾਉਣ ਲਈ ਫੇਲ੍ਹ
 ਗਨੋਮ ਗਨੋਮ ਡੰਮੀ Xorg ਉੱਤੇ ਗਨੋਮ ਜੇ ਸਮਰੱਥ ਕੀਤਾ ਤਾਂ ਗਨੋਮ-ਸ਼ੈਸ਼ਨ ਲਾਗਇਨ ਕਰਨ ਦੇ ਬਾਅਦ ਚੇਤਾਵਨੀ ਡਾਈਲਾਗ ਵਿਖਾਏਗਾ, ਜੇ ਸ਼ੈਸ਼ਨ ਆਪਣੇ-ਆਪ ਵਾਪਸ ਨਹੀਂ ਚੱਲਿਆ ਹੋਵੇਗਾ। ਸੁਰੱਖਿਆ ਕਾਰਨਾਂ ਕਰਕੇ, ਗਨੋਮ-ਸ਼ੈਸ਼ਨ ਸ਼ੈਸ਼ਨ ਖਤਮ ਕਰਨ ਵੇਲੇ ਯੂਜ਼ਰ ਨੂੰ ਪੁੱਛੇ। ਜੇ ਚੋਣ ਕੀਤੀ ਤਾਂ ਗਨੋਮ-ਸ਼ੈਸ਼ਨ ਆਟੋਮੈਟਿਕ ਹੀ ਸ਼ੈਸ਼ਨ ਨੂੰ ਸੰਭਾਲ ਲਵੇਗਾ। ਕਿਸੇ ਵੀ ਮੌਜੂਦ ਇੰਹੈਬੇਟਰ ਨੂੰ ਅਣਡਿੱਠਾ ਕੀਤਾ ਜਾਂਦਾ ਹੈ ਲਾਗ-ਆਉਟ ਲਾਗਆਉਟ ਪੁਸ਼ਟੀ ਕੋਈ ਜਵਾਬ ਨਹੀਂ ਓਹ ਹੋ! ਕੁਝ ਗਲਤ ਹੋ ਗਿਆ। ਸਟੈਂਡਰਡ ਆਟੋ-ਸਟਾਰਟ ਡਾਇਰੈਕਟਰੀਆਂ ਅਣਡਿੱਠੀਆਂ ਚਲਾਉਣ ਲਈ ਕਸਟਮ ਸ਼ੈਸ਼ਨ ਚੁਣੋ ਜੀ ਬੰਦ ਕਰੋ ਪਰੋਗਰਾਮ ਨੂੰ ਚੋਣਾਂ ਨਾਲ ਅਪਵਾਦ ਮਿਲਿਆ ਪਰੋਗਰਾਮ ਨੂੰ ਪੂਰਾ ਠੀਕ ਪੈਰਾਮੀਟਰ ਚਾਹੀਦਾ ਹੈ ਮੁਡ਼-ਚਾਲੂ ਨਵਾਂ ਕਲਾਇਟ ਕੁਨੈਕਸ਼ਨ ਤੋਂ ਇਨਕਾਰ ਕੀਤਾ ਗਿਆ ਹੈ, ਕਿਉਂਕਿ ਸ਼ੈਸ਼ਨ ਬੰਦ ਹੋ ਰਿਹਾ ਹੈ
 ਯਾਦ ਰੱਖੇ ਹੋਏ ਐਪਲੀਕੇਸ਼ਨ ਸ਼ੈਸ਼ਨ ਨਾਂ ਬਦਲੋ(_m) ਜੇ ਚੱਲ ਰਿਹਾ ਹੋਵੇ ਤਾਂ dbus.service ਮੁੜ-ਚਾਲੂ ਕਰੋ ਸੇਵਾ ਅਸਫ਼ਲ ਹੋਣ ਉੱਤੇ gnome-session-failed.target ਸ਼ੁਰੂ ਕਰਨ ਲਈ ExecStopPost ਤੋਂ ਚਲਾਓ systemd ਸੇਵਾ ਵਜੋਂ ਚੱਲ ਰਿਹਾ ਹੈ SESSION_NAME ਸ਼ੈਸ਼ਨ ਸੰਭਾਲੋ ਇਹ ਸ਼ੈਸ਼ਨ ਸੰਭਾਲੋ ਸ਼ੈਸ਼ਨ %d ਸ਼ੈਸ਼ਨ ਨਾਂ ਵਿੱਚ  “/ “ ਅੱਖਰ ਮਨਜ਼ੂਰ ਨਹੀਂ ਸ਼ੈਸ਼ਨ ਨਾਂ  ਦੇ ਸ਼ੁਰੂ ਵਿੱਚ  “. “ ਮਨਜ਼ੂਰ ਨਹੀਂ ਸ਼ੈਸ਼ਨ ਨਾਂ ਦੇ ਸ਼ੁਰੂ ਵਿੱਚ  “. “ ਜਾਂ   “/ “ ਅੱਖਰ ਮਨਜ਼ੂਰ ਨਹੀਂ ਵਰਤਣ ਲਈ ਸ਼ੈਸ਼ਨ ਇਕਸਟੈਨਸ਼ਨ ਚੇਤਾਵਨੀ ਵੇਖਾਓ ਟੈਸਟ ਕਰਨ ਲਈ ਵੇਲ੍ਹ ਡਾਈਲਾਗ ਵੇਖਾਓ ਫਾਲਬੈਕ ਚੇਤਾਵਨੀ ਵੇਖਾਓ ਗਨੋਮ-ਸ਼ੈਸ਼ਨ ਲਈ ਸਿਗਨਲ ਸ਼ੁਰੂਆਤ ਮੁਕੰਮਲ  gnome-session-shutdown.target ਸ਼ੁਰੂ ਕਰੋ gnome-session-shutdown.target ਸ਼ੁਰੂ ਕਰੋ, ਜਦੋਂ EOF ਜਾਂ stdin ਉੱਤੇ ਸਿੰਗਲ ਬਾਈਟ ਮਿਲੇ ਇਹ ਐਂਟਰੀ ਤੁਹਾਨੂੰ ਸੰਭਾਲਿਆ ਸ਼ੈਸ਼ਨ ਚੁਣਨ ਦਿੰਦੀ ਹੈ ਇਹ ਪਰੋਗਰਾਮ ਲਾਗ-ਆਉਟ ਨੂੰ ਰੋਕ ਰਿਹਾ ਹੈ। ਇਹ ਸ਼ੈਸ਼ਨ ਤੁਹਾਨੂੰ ਗਨੋਮ ਵਿੱਚ ਲਾਗ ਕਰਦਾ ਹੈ ਇਹ ਬਿਲਟ-ਇਨ ਸ਼ੈਸ਼ਨ ਇੰਤਜ਼ਾਮ (systemd ਦੇ ਅਧਾਰਿਤ ਬਿਨਾਂ) systemd ਸ਼ੈਸ਼ਨ ਇੰਤਜ਼ਾਮ ਵਰਤੋਂ ਇਸ ਐਪਲੀਕੇਸ਼ਨ ਦਾ ਵਰਜ਼ਨ ਜੇ ਸਮਰੱਥ ਕੀਤਾ ਤਾਂ ਗਨੋਮ-ਸ਼ੈਸ਼ਨ ਨੂੰ ਲਾਗ ਆਉਟ ਕਰਨ ਉੱਤੇ ਅਗਲੇ ਸ਼ੈਸ਼ਨ ਲਈ ਸੰਭਾਲਿਆ ਜਾਵੇਗਾ, ਭਾਵੇਂ ਕਿ ਆਪਣੇ-ਆਪ ਸੰਭਾਲਣ ਨੂੰ ਅਸਮਰੱਥ ਕੀਤਾ ਹੋਵੇ। ਜਾਰੀ ਰੱਖੋ(_C) ਲਾਗ-ਆਉਟ(_L) ਲਾਗ-ਆਉਟ(_L) ਨਵਾਂ ਸ਼ੈਸ਼ਨ(_N) ਸ਼ੈਸ਼ਨ ਹਟਾਓ(_R) 