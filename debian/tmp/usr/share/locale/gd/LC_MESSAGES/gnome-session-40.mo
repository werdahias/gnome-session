��    ;      �  O   �           	  4  (     ]  T   v  i   �  \   5  '   �     �     �  (   �  )   �     (	     /	  #   >	  '   b	  $   �	     �	     �	     �	     �	     �	  q   �	  G   m
  >   �
      �
               +  !   :  '   \  %   �  	   �  '   �     �  P   �     4     K     [     h     v  
   �  ;   �  3   �  Q        U     d  &   {     �  *   �      �           )  p   E  	   �     �     �     �     �    �  !     �  /       �   !  t   �  y     /   �     �     �  9   �  @   /     p     }  A   �  8   �  0     "   >     a     }     �     �  �   �  i   2  M   �  ,   �           (     I      c  6   �  %   �  
   �  8   �     %  Y   3     �      �     �     �     �  
     :     2   L  \        �     �  4        D  K   a  7   �  .   �       �   0     �     �     �     �              9      *         %      :           4   8      ;               5            7   !   '      &              (   	          ,      #              -       .      3                0      $      +       )       2       1                        /   6               
              "        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2019-02-16 09:55+0100
Last-Translator: GunChleoc <fios@foramnagaidhlig.net>
Language-Team: Fòram na Gàidhlig
Language: gd
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 || n==11) ? 0 : (n==2 || n==12) ? 1 : (n > 2 && n < 20) ? 2 : 3;
X-Generator: Virtaal 0.7.1
X-Launchpad-Export-Date: 2013-09-15 06:24+0000
X-Project-Style: gnome
  – manaidsear nan seisean GNOME %s [ROGHAINN…] ÀITHNE

Ruith ÀITHNE fhad ’s a thèid cuid a dh’fhoincseanan seisein a bhacadh.

  -h, --help        Seall a’ chobhair seo
  --version         Seall an tionndadh dhen phrògram
  --app-id ID       ID ana h-aplacaid a thèid a chleachdadh
                    leis a’ bhacadh (roghainneil)
  --reason ADHBHAR  Adhbhar a’ bhacaidh (rogainneil)
  --inhibit ARG     Nithean ri am bacadh, liosta air a sgaradh le
                    còileanan de:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Na cuir an ÀITHNE gu dol is dèan feitheamh gun chrìch
                    ’na àite

Mura deach an roghainn --inhibit a shònrachadh, thathar an dùil
air idle.
 Tha argamaid a dhìth air %s
 Thachair duilgheadas ach nach urrainn dhan t-siostam aiseag.
An clàraich thu a-mach is a-steach airson feuchainn ris a-rithist? Thachair duilgheadas ach nach urrainn dhan t-siostam aiseag. Chaidh na h-uile leudachan a chur à comas mar earalas. Thachair duilgheadas ach nach urrainn dhan t-siostam aiseag. Feuch an cuir thu fiosrachadh gu rianaire an t-siostaim agad Tha seisean air a bheil “%s” ann mu thràth PASGAN_FÈIN_TÒISEACHAIDH Ceadaich clàradh a-mach Cha b’ urrainn dhuinn ceangal ri manaidsear nan seisean Cha b’ urrainn dhuinn socaid èisteachd ICE a chruthachadh: %s Gnàthaichte Seisean gnàthaichte Cuir an dearbhadh air luathachadh a’ bhathair-chruaidh à comas Na luchdaich aplacaidean gnàthaichte a’ chleachdaiche Na faighnich airson dearbhadh a’ chleachdaiche Cuir an comas còd dì-bhugachaidh Cha deach leinn %s a ruith
 GNOME Caochag GNOME GNOME air Xorg Ma tha seo an comas, seallaidh gnome-session còmhradh rabhaidh às dèidh a’ chlàraidh a-steach ma chaidh an seisean aiseag gu h-èiginneach. Ma tha seo an comas, cuiridh gnome-session ceist dhan chleachdaiche mus dèid crìoch a chur air seisean. Ma tha seo an comas, sàbhailidh gnome-session an seisean gu fèin-obrachail. A’ leigeil seachad gach bacadair a tha ann Clàraich a-mach Dearbhadh a’ chlàraidh a-mach Chan eil e a’ freagairt Obh Obh! Chaidh rudeigin ceàrr. Tar-àithn na pasganan fèin-tòiseachaidh stannardach Tagh seisean gnàthaichte gus a ruith Cuir dheth Chaidh prògram a ghairm le roghainnean ann an còmhstri Ath-thòisich A’ diùltadh ceangal ùr ri cliant on a tha an seisean ’ga dhùnadh sìos an-dràsta
 Aplacaid air a cuimhneachadh Thoir ain_m ùr air an t-seisean AINM_SEISEIN Sàbhail na seiseanan Sàbhail an seisean seo Seisean %d Chan fhaod caractar “/” a bhith am broinn ainm seisein Chan fhaod ainm seisein a thòiseachadh le “.” Chan fhaod ainm seisein a thòiseachadh le “.” no caractar “/” a bhith ’na bhroinn An seisean ri chleachdadh Seall rabhadh leudachain Seall an còmhradh “fail whale” a chum deuchainn Seall an rabhadh èiginneach ’S urrainn dhut seisean air a shàbhaladh a thaghadh leis an innteart seo Tha am prògram seo a’ bacadh a’ chlàraidh a-mach. Clàraidh an seisean seo a-steach gu GNOME thu Tionndadh na h-aplacaid seo Ma tha seo an comas, sàbhailidh gnome-session an ath-sheisean nuair a nithear clàradh a-mach fiù ma tha an sàbhaladh fèin-obrachail à comas. _Lean air adhart C_làraich a-mach C_làraich a-mach _Seisean ùr Thoi_r an seisean air falbh 