��    #      4  /   L           	  T   (  i   }  \   �  '   D     l     z  (   �     �     �     �     �     �     �  !   �  %     	   A     K     R     i     y     �     �  
   �     �     �      �      �       	   5     ?     H     Q     ^  �  n  6   �  �   '  �   �  �   _	  M   
     V
  *   d
  W   �
     �
  !   �
  ,        G     P     c  0   �  Y   �          !  )   4     ^     ~  )   �  2   �     �  (   �  ?   %  L   e  S   �  $        +     K     ^     q  !   �                  #                !                      	         
   "                                                                                          — the GNOME session manager A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Custom Custom Session Failed to execute %s
 GNOME Log out Not responding Oh no!  Something has gone wrong. Please select a custom session to run Power off Reboot Remembered Application Rena_me Session SESSION_NAME Save sessions Save this session Session %d Session to use Show extension warning This program is blocking logout. This session logs you into GNOME Version of this application _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session gnome-3-34
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2020-01-17 02:18+0300
Language-Team: Kurdish Sorani <ckb@li.org>
Language: ckb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Last-Translator: Jwtiyar Nariman <jwtiyar@gmail.com>
X-Generator: Poedit 2.2.4
  —بەڕێوەبردنی پیشاندانی گنۆم کێشەیەک ڕوویدا لە سیستەم ناتوانرێت بگەڕینرێتەوە.
تکایە بچۆدەرەوە و دووبارە هەوڵبدەرەوە کێشەیەک ڕوویدا لە سیستەم ناتوانرێت بگەڕینرێتەوە. هەموو پێوەکراەکان ناچالاک کران. کێشەیەک ڕوویدا لە سیستەم ناتوانرێت بگەڕینرێتەوە. تکایە پەیوەندی بە بەڕیوەبەری سیستمەوە بکە دانیشتنێک بە ناوی “%s” پێشتر بەکارهێنراوە AUTOSTART_DIR ڕێگە بەچوونەدەرەوە بدە نەتوانرا پەیوەندی بگرێت لەگەڵ بەڕێوەبردنی گنۆم ڕاسپێراو دانیشتنی ڕاسپێراو نەتوانرا %s جێبەجێبکرێت 
 گنوم _ده‌رچوون وەڵام نادرێتەوە ئۆو نا! شتێک بەهەڵە ڕوویدا. تکایە دانیشتنێکی ڕاسپێراو دیاریبکە بۆ دەسپێکردن کوژاندنەوە پێکردنەوە داوانامەی لەیادنەکراو ناوـنانی دانیشتن SESSION_NAME پاشەکەوتکردنی دانیشتن پاشەکەوتکردنی ئەم دانیشتنە دانیشتنی %d دانیشتن بۆ بەکارهێنان ئاگاداریەکانی پێوەکراوە پیشان بدە ئەم نەرمەواڵەی ڕێگە بە چوونەدەرەوە نادات. به‌م دانیشتنه‌ ئه‌که‌ویته‌ ناو گنومه‌وه‌ وەشانی ئەم پڕۆگرامە _به‌رده‌وامبوون _ده‌رچوون _ده‌رچوون ـدانیشتنێکی نوێ ـسڕینەوەی دانیشتن 