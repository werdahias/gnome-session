��    D      <  a   \      �     �  p        q  T   �  i   �  \   I	  '   �	     �	     �	  (   �	  )   
     <
     C
  #   R
  '   v
  $   �
     �
     �
     �
     �
       q     G   �  >   �           )     1     ?  !   N  '   p  %   �  	   �  '   �  #   �       P        l     �  %   �  M   �          "     /     =  
   O  ;   Z  3   �  Q   �          +  &   B     i  +   �  #   �  P   �  *   $      O      p  B   �     �     �  p     	   �     �     �     �     �  �  �     �  �  �     R  V   k  g   �  Y   *  #   �     �     �  .   �  '   �     !     /  *   E  '   p  #   �     �     �     �     �     �  u     E   �  ;   �       
   "     -     C     L  8   i  ,   �  	   �  <   �  &        =  D   O     �     �  &   �  ]   �     A     ^     k     }  
   �  4   �  -   �  J        M     ]  -   z  (   �  5   �  &     J   .  <   y  #   �  %   �  5         6     V  |   h     �     �     �                     !                    #       *   9   6   4           0   )              8       (      '   $   %           >                     	          -          A   D   ,                                        B              ?   ;              :      
       +   .   3          /   5   1           2   =          @      "      C                7   &      <        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session.HEAD
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2020-08-26 22:29+0300
Last-Translator: Aurimas Černius <aurisc4@gmail.com>
Language-Team: Lietuvių <gnome-lt@lists.akl.lt>
Language: lt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : 2)
X-Generator: Gtranslator 3.36.0
  – GNOME seanso tvarkytuvė %s [PARAMETRAS…] KOMANDA

Vykdyti KOMANDĄ, tuo tarpu kliudant daliai seanso funkcionalumo.

  -h, --help        Rodyti šį žinyną
  --version         Rodyti programos versiją
  --app-id ID       Naudotinas programos id
                    kliudant (neprivaloma)
  --reason REASON   Kliudimo priežastis (neprivaloma)
  --inhibit ARG     Kliudomi dalykai, dvitaškiai skiriamas sąrašas iš šių:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Nepaleisti KOMANDOS ir vietoj to neribotai laukti
  -l, --list        Išvardinti esamus kliudymus ir išeiti

Jei nėra parametro --inhibit, naudojama idle.
 %s reikalauja argumento
 Iškilo problema ir sistema negali atsistatyti.
Atsijunkite ir prisijunkite iš naujo. Iškilo problema ir sistema negali atsistatyti. Kaip atsargos priemonė visi plėtiniai buvo išjungti. Iškilo problema ir sistema negali atsistatyti. Susisiekite su sistemos administratoriumi Jau yra seansas pavadinimu „%s“ AUTOSTART_DIR Leisti atsijungimą Nepavyko prisijungti prie seansų tvarkytuvės Nepavyko sukurti ICE klausymo lizdo: %s Pasirinktinis Pasirinktinis seansas Išjungti aparatinio spartinimo tikrinimą Neįkelti naudotojo nurodytų programų Nereikalauti naudotojo patvirtinimo Įjungti derinimo kodą Nepavyko paleisti %s
 GNOME GNOME netikra GNOME Xorg aplinkoje Jei įjungta, gnome-session prisijungus parodys įspėjimo dialogą, jei automatiškai naudojama atsarginė veiksena. Jei įjungta, gnome-session klaus naudotojo prie užbaigiant seansą. Jei įjungta, gnome-session automatiškai įrašys seansą. Ignoruojami trukdžiai Atsijungti Atsijungimo klausimas Neatsako O ne! Nutiko kažkas negera. Nepaisyti standartinių automatinės paleisties aplankų Pasirinkite pasirinktinį seansą paleidimui Išjungti Programa iškviesta, naudojant konfliktuojančius parametrus Programai reikia vienintelio parametro Įkelti iš naujo Atsisakoma naujo kliento ryšio, kadangi seansas dabar išjungiamas
 Atsiminti programą Per_vadinti seansą Perleisti dbus.service, jei jau veikia Vykdyti iš ExecStopPost, kad tarnybai nepavykus būtų paleistas gnome-session-failed.target Vykdoma kaip systemd tarnyba SESSION_NAME Įrašyti seansus Įrašyti šį seansą Seansas %d Seansų pavadinimai negali turėti simbolių „/“ Seansų pavadinimai negali prasidėti „.“ Seansų pavadinimai negali prasidėti „.“ ar turėti simbolių „/“ Naudoti sesiją Rodyti plėtinio įspėjimą Rodyti visiškos nesėkmės dialogą bandymui Rodyti atsarginės veiksenos įspėjimą Pranešti gnome-session apie inicializacijos pabaigą Paleisti gnome-session-shutdown.target Paleisti gnome-session-shutdown.target gavus EOF arba vieno baito įvestį Šis įvesties laukas leidžia pasirinkti įrašytą seansą Ši programa neleidžia atsijungti. Šis seansas prijungia jus prie GNOME Naudoti integruotą seanso valdymą (vietoje systemd) Naudoti systemd seanso valdymą Programos versija Kai įjungta, gnome-session automatiškai įrašys kitą seansą atsijungiant net jei automatinis įrašymas yra išjungtas. _Tęsti _Atsijungti _Atsijungti _Naujas seansas _Pašalinti seansą 