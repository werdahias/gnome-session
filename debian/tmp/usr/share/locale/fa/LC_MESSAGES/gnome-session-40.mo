��  C   0   H  a   `     �     �  �             p  4     �  T   �  i   	  \   }	  '   �	     
     
  (   
  )   F
     p
     w
  #   �
  '   �
  $   �
     �
          #     )     5  q   C  G   �  >   �      <     ]     e     s  !   �  '   �  %   �  	   �  '   �  #   $     H  P   O     �     �  %   �  M   �     ;     V     c     q  ;   �  3   �  Q   �     E     T  &   k     �  +   �  #   �  P   �  *   M      x      �  B   �     �       p   8  	   �     �     �     �     �  �  �     �  ?  �  =   �  �   4  �   �  �   l  ;        =     K  3   a  6   �     �     �  M   �  ;   =  9   y  >   �      �               2  �   K  �     q   �  L        \     e     y  )   �  [   �  M        ^  W   r  D   �  "     w   2  %   �     �  ;   �  p   '   ?   �      �      �      !  S    !  I   t!  s   �!      2"  "   S"  =   v"  &   �"  :   �"  &   #  d   =#  ]   �#  =    $  @   >$  �   $  ;   %     A%  �   `%     5&  	   A&  	   K&     U&     h&         !                    #       *   8   5   3           /   )              7       (      '   $   %           =                     	          -          @   C   ,                                        A              >   :              9      
       +       2          .   4   0           1   <          ?      "      B                6   &      ;         �&  �     �&     �����&  
          ����  — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session 2.10.0
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2020-08-18 17:00+0000
Last-Translator: Danial Behzadi <dani.behzi@ubuntu.com>
Language-Team: Persian
Language: fa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: utf-8
X-Generator: Poedit 2.4.1
  — مدیر نشست گنوم %s [گزینه…] دستور

اجرای دستور هنگام منع برخی قابلیت‌های نشست.

  -h, --help        نمایش این راهنما
  --version         نمایش نگارش برنامه
  --app-id ID       شناسهٔ برنامه برای استفاده
                    هنگام منع (اختیاری)
  --reason REASON   دلیل منع (اختیاری)
  --inhibit ARG     دلیل منع، فهرستی جداشده با دونقطه از:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    دستور COMMAND اجرا نشود و به جایش تا ابد منتظر شود
  -l, --list        فهرست کردن منع‌های موجود و خروج

اگر گزینهٔ no --inhibit مشخّص شود، idle در نظر گرفته می‌شود.
 دستور %s احتیاج به یک آرگومان دارد
 مشکلی رخ داده و سامانه نمی‌تواند بازیابی شود.
لطفاً خارج شده و دوباره بیازمایید. مشکلی رخ داده و سامانه نمی‌تواند بازیابی شود. تمام افزونه‌ها جهت احتیاط غیرفعال شده‌اند. مشکلی رخ داده و سامانه نمی‌تواند بازیابی شود. لطفاً با یک مدیر سامانه تماس بگیرید نشستی با نام «%s» از پیش وجود دارد AUTOSTART_DIR اجازهٔ خروج اتصال به مدیر نشست ممکن نیست ایجاد سوکت شنود ICE ممکن نبود: %s سفارشی نشست سفارشی غیرفعال‌سازی بررسی شتابدهنده سخت‌افزاری بار نکردن برنامه‌های مختص کاربر برای تایید هویت کاربر هشدار نده کد اشکال‌زدایی به کار انداخته شود اجرای %s شکست خورد
 گنوم گنوم ساختگی گنوم روی زورگ در صورت فعّال بودن، اگر نشست به صورت خودکار به عقب افتاده باشد، gnome-session یک پنجرهٔ هشدار پس از ورود نمایش خواهد داد. در صورت فعّال بودن، gnome-session پیش از اتمام یک نشست، به کاربر اعلان می‌دهد. در صورت فعّال بودن، گنوم به صورت خودکار، نشست را ذخیره می‌کند. هرگونه مهارکننده‌های خروج را نادیده بگیر خروج اعلان خروج ناپاسخگو وای نه!  اشتباهی رخ داد. شاخه‌های استاندارد آغاز خودکار، نادیده گرفته شود لطفاً یک نشست سفارشی را برای اجرا برگزینید خاموش کردن برنامه با گزینه‌های ناسازگار فراخوانده شده است برنامه دقیقاً به یک پارامتر نیاز دارد راه‌اندازی دوباره چون سامانه در حال خاموش شدن است، درخواست اتصال کارخواه رد می‌شود
 به خاطر سپردن برنامه _تغییرنام نشست شروع دوبارهٔ dbus.service در صورت اجرا برای آغاز gnome-session-failed.target هنگام شکست خدمت، از ExecStopPost اجرا کنید در حال اجرا به عنوان خدمت سیستم‌دی SESSION_NAME _ذخیرهٔ نشست‌ها ذخیرهٔ این نشست نام نشست‌ها نمی‌توانند شامل نویسهٔ «/» باشند نام نشست‌ها نمی‌توانند با «.» شروع شوند نام نشست‌ها نمی‌توانند با «.» شروع شده یا شامل نویسهٔ «/» باشند نشست مورد استفاده نمایش هشدار افزونه نمایش بالن محاوره شکست جهت آزمایش نمایش هشدار جایگزینی آغازش سیگنال به gnome-session انجام شد شروع gnome-session-shutdown.target شروع gnome-session-shutdown.target هنگام دریافت EOF یا تک‌بایت روی stdin این ورودی می‌گذارد نشست ذخیره‌شده‌ای را برگزینید این برنامه جلوی خروج را گرفته است. این نشست شما را به گنوم وارد می‌کند استفاده از مدیریت نشست توکار (به جای استفاده از مدیریت مبتنی بر سیستم‌دی) استفاده از مدیریت نشست سیستم‌دی نگارش این برنامه هنگام فعّال بودن، gnome-session به صورت خودکار، نشست بعدی را هنگام خروج ذخیره می‌کند، حتا اگر ذخیرهٔ خودکار غیرفعّال باشد. _ادامه _خروج _خروج نشست _جدید _برداشتن نشست I Session %d نشست %d 