��    D      <  a   \      �     �  p        q  T   �  i   �  \   I	  '   �	     �	     �	  (   �	  )   
     <
     C
  #   R
  '   v
  $   �
     �
     �
     �
     �
       q     G   �  >   �           )     1     ?  !   N  '   p  %   �  	   �  '   �  #   �       P        l     �  %   �  M   �          "     /     =  
   O  ;   Z  3   �  Q   �          +  &   B     i  +   �  #   �  P   �  *   $      O      p  B   �     �     �  p     	   �     �     �     �     �  �  �  )   k  �  �     F  l   c  �   �  h   ]  ,   �     �       6     8   S     �     �  >   �  =   �     .     N     j     �     �     �  �   �  Q   H  @   �      �     �          #  -   7  -   e  5   �  	   �  1   �  7        =  b   I     �     �  ;   �  b     %   z     �     �     �  
   �  D   �  ;   /  _   k     �  .   �  .     0   =  <   n  '   �  n   �  8   B   $   {   &   �   R   �   (   !     C!  �   `!  
   �!     "     "     *"     <"         !                    #       *   9   6   4           0   )              8       (      '   $   %           >                     	          -          A   D   ,                                        B              ?   ;              :      
       +   .   3          /   5   1           2   =          @      "      C                7   &      <        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session.HEAD
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2020-08-27 17:12+0200
Last-Translator: vanadiae <vanadiae35@gmail.com>
Language-Team: GNOME French Team <gnomefr@traduc.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Gtranslator 3.36.0
  — le gestionnaire de sessions de GNOME %s [OPTION…] COMMANDE

Exécute COMMANDE tout en inhibant certaines fonctionnalités de la session.

  -h, --help        Afficher cette aide
  --version         Afficher la version du programme
  --app-id ID       L’identifiant d’application à utiliser
                    lors de l’inhibition (optionnel)
  --reason RAISON   La raison de l’inhibition (optionnel)
  --inhibit ARG     Les choses à inhiber, liste séparée par des deux-points, parmi :
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Ne pas lancer COMMANDE et attendre indéfiniment à la place

Si l’option --inhibit n’est pas précisée, « idle » est présumé.
 %s nécessite un paramètre
 Un problème est survenu et le système ne peut pas se récupérer.
Déconnectez-vous et essayez à nouveau. Un problème est survenu et le système ne peut pas se récupérer. Toutes les extensions ont été désactivées par mesure de précaution. Un problème est survenu et le système ne peut pas se récupérer. Contactez un administrateur système Une session nommée « %s » existe déjà RÉP_AUTOSTART Autoriser la déconnexion Impossible de se connecter au gestionnaire de sessions Impossible de créer le connecteur d’écoute ICE : %s Personnalisé Session personnalisée Désactiver la vérification de l’accélération matérielle Ne charge pas les applications demandées par l’utilisateur Ne pas demander de confirmation Active le code de débogage Impossible d’exécuter %s
 GNOME GNOME factice GNOME sur Xorg Si activé, gnome-session affiche une boîte de dialogue d’avertissement après la connexion si la session de secours a dû être automatiquement activée. Si activé, gnome-session interroge l’utilisateur avant de terminer la session. Si activé, gnome-session enregistre automatiquement la session. Ignorer tout inhibiteur existant Se déconnecter Invite de déconnexion Absence de réponse Oh mince ! Quelque chose s’est mal passé. Remplace les répertoires autostart standards Veuillez choisir une session personnalisée à lancer Éteindre Programme appelé avec des options conflictuelles Le programme a besoin d’un seul paramètre uniquement Redémarrer Refus de la connexion d’un nouveau client car la session est actuellement en cours de fermeture
 Applications mémorisées Reno_mmer la session Redémarrer dbus.service s’il est en cours d’exécution Démarrer depuis ExecStopPost pour lancer gnome-session-failed.target en cas d’échec du service Exécuté en tant que service systemd NOM_SESSION Enregistrer les sessions Enregistrer cette session Session %d Les noms de sessions ne peuvent pas contenir le caractère « / » Les noms de sessions ne peuvent pas commencer par « . » Les noms de sessions ne peuvent pas commencer par « . » ou contenir le caractère « / » Session à utiliser Afficher l’avertissement pour les extensions Afficher le dialogue d’erreur pour le tester Afficher l’avertissement de session de secours Signaler l’initialisation comme terminée à gnome-session Démarrer gnome-session-shutdown.target Démarrer gnome-session-shutdown.target si réception de fin de fichier (EOF) ou d’un octet unique sur stdin Ce choix permet d’accéder à une session enregistrée Ce programme bloque la déconnexion. Cette session vous connecte dans GNOME Utiliser la gestion des sessions intégrée (plutôt que celle basée sur systemd) Utiliser la gestion des sessions systemd Version de cette application Si activé, gnome-session enregistre automatiquement la prochaine session lors de la déconnexion même si l’enregistrement automatique est désactivé. _Continuer _Fermer la session _Se déconnecter _Nouvelle session Supp_rimer la session 