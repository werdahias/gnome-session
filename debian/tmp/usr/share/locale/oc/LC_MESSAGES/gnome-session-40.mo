��    3      �  G   L      h     i  4  �     �  T   �  i   +  \   �  '   �          (  (   5  )   ^     �     �  #   �  '   �  $   �     	     %	     ;	     A	     M	      [	     |	     �	  !   �	  '   �	  %   �	  	   
  '   
     5
  P   <
     �
     �
     �
  
   �
  ;   �
  3     Q   <     �     �  &   �  *   �            '     H  	   d     n     w     �     �  �  �  '   V  �  ~       e   (  �   �  g     )   }     �     �  4   �  4        9     F  6   \  5   �     �     �               #     1     @     ^     n     �  .   �  -   �     �  +        2  `   :     �     �     �  
   �  >   �  5     V   U     �  '   �  (   �  ;     (   L  (   u     �  
   �     �     �     �     �                                    	   1      $   2                         *              0   -            "   %   &      3           !   /             #                   
          +                                         .   '   (             )   ,     — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg Ignoring any existing inhibitors Log out Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Version of this application _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session.HEAD
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2018-06-27 12:29+0200
Last-Translator: Cédric Valmary (totenoc.eu) <cvalmary@yahoo.fr>
Language-Team: Tot En Òc
Language: oc
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Virtaal 0.7.1
X-Project-Style: gnome
  — lo gestionari de sessions de GNOME %s [OPTION…] COMANDA

Executa COMANDA tot en inibent de foncionalitats de la session.

  -h, --help        Afichar aquesta ajuda
  --version         Afichar la version del programa
  --app-id ID       L'identificant d'aplicacion d'utilizar
                    al moment de l'inibicion (opcional)
  --reason REASON   La rason de l'inibicion (opcional)
  --inhibit ARG     Las causas d'inibir, lista separada per de dobles punts demest :
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Avietz pas COMANDA e esperatz indefinidament a la plaça

Se pas cap d'opcion --inhibit es pas precisat, « idle » es supausat.
 %s necessita un paramètre
 Un problèma s'es produit e lo sistèma se pòt pas recuperar.
Desconnectatz-vos e ensajatz tornamai. Un problèma s'es produit e lo sistèma se pòt pas recuperar. Totas las extensions son estadas desactivadas per mesura de precaucion. Un problèma s'es produit e lo sistèma se pòt pas recuperar. Contactatz un administrator del sistèma Una session nomenada « %s » existís ja REP_AUTOSTART Autorizar la desconnexion Impossible de se connectar al gestionari de sessions Impossible de crear lo connectador d'escota ICE : %s Personalizat Session personalizada Desactivar la verificacion de l'acceleracion materiala Carga pas las aplicacions demandadas per l'utilizaire Demanda pas de confirmacion Activa lo còdi de desbugatge Impossible d'executar %s
 GNOME GNOME factici GNOME sus Xorg Ignorar tot inibitor existent Se desconnècta Abséncia de responsa Ò damne ! Quicòm a trucat. Remplaça los repertòris autostart estandards Causissètz una session personalizada d'aviar S'atuda Programa apelat amb d'opcions conflictualas Reaviar Refús de la connexion d'un novèl client perque la session es actualament en cors de tampadura
 Aplicacions memorizadas Reno_menar la session NOM_SESSION Session %d Los noms de sessions pòdon pas conténer lo caractèr « / » Los noms de sessions pòdon pas començar per « . » Los noms de sessions pòdon pas començar per « . » o conténer lo caractèr « / » Session d'utilizar Afichar l'avertiment per las extensions Afichar lo dialòg d'error per lo testar Aquesta causida permet d'accedir a una session enregistrada Aqueste programa blòca la desconnexion. Aquesta session vos connècta dins GNOME Version d'aquesta aplicacion _Contunhar _Tampar la session _Se desconnectar Session _novèla Sup_rimir la session 