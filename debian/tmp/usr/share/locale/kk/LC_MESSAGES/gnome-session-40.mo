��    D      <  a   \      �     �  p        q  T   �  i   �  \   I	  '   �	     �	     �	  (   �	  )   
     <
     C
  #   R
  '   v
  $   �
     �
     �
     �
     �
       q     G   �  >   �           )     1     ?  !   N  '   p  %   �  	   �  '   �  #   �       P        l     �  %   �  M   �          "     /     =  
   O  ;   Z  3   �  Q   �          +  &   B     i  +   �  #   �  P   �  *   $      O      p  B   �     �     �  p     	   �     �     �     �     �  �  �  4   j  �  �  .   �  �   �  �   W  �     <   �  )   �  /     e   A  H   �     �  #   	  E   -  O   s  6   �  )   �     $     D  #   J  !   n    �  �   �  �   R  \   �     3  $   K     p  .   �  p   �  R   ,         `   �   M   �      =!  {   Q!  2   �!  )    "  d   *"  �   �"  :   #     X#  #   l#  $   �#     �#  H   �#  N   $  �   ]$  $   �$  0   %  @   @%  N   �%  M   �%  /   &  �   N&  x   �&  R   ]'  :   �'  }   �'  6   i(  ,   �(  �   �(     �)     �)     �)     �)     *         !                    #       *   9   6   4           0   )              8       (      '   $   %           >                     	          -          A   D   ,                                        B              ?   ;              :      
       +   .   3          /   5   1           2   =          @      "      C                7   &      <        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session master
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2020-08-16 13:35+0500
Last-Translator: Baurzhan Muftakhidinov <baurthefirst@gmail.com>
Language-Team: Kazakh <kk_KZ@googlegroups.com>
Language: kk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 2.4.1
  — GNOME сессиялар басқарушысы %s [ОПЦИЯ…] КОМАНДА

Сессияның кейбір мүмкіндіктерін шектеп, КОМАНДАНЫ орындау.

  -h, --help        Бұл көмекті көрсету
  --version         Бағдарлама нұсқасын көрсету
  --app-id ID       Сессияны ұстау кезінде қолданылатын
                    қолданба id-і (қосымша түрде)
  --reason REASON   Шектеу (қосымша түрде)
  --inhibit ARG     Шектелетін нәрсе, келесіден тұратын үтірлермен ажыратыған тізім:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    КОМАНДАны орындамау, орнына тек мәңгі күту
  -l, --list        Бар болып тұрған шектеулерді көрсету және шығу

Егер --inhibit опциясы көрсетілмесе, бос етіп есептеледі.
 %s аргументті талап етеді
 Мәселе орын алды және жүйе өз қалпына келе алмады.
Жүйеден шығып, қайталап көріңіз. Мәселе орын алды және жүйе өз қалпына келе алмады. Барлық кеңейтулер сақтық мақсатында сөндірілген. Мәселе орын алды және жүйе өз қалпына келе алмады. Жүйелік әкімшіңізге хабарласыңыз "%s" атауы бар сессия бар болып тұр АВТОІСКЕҚОСЫЛУ_БУМАСЫ Жүйеден шығуды рұқсат ету Сессиялар басқарушысына байланысты орнату мүмкін емес ICE тындаушы сокетін жасау мүмкін емес: %s Таңдауыңызша Таңдауыңызша сесия Құрылғылық үдетуді тексеруді сөндіру Пайдаланушыға тән қолданбаларды жөнелтпеу Пайдаланушы растауын сұрамау Жөндеу кодын іске қосу %s жөнелту сәтсіз
 GNOME GNOME жалған сессиясы Xorg негізіндегі GNOME Іске қосылған болса, жүйеге кірген кезде сессия автоматты түрде бастапқы түріне орнатылған болса, gnome-session ескерту хабарламасын көрсететін болады. Іске қосылған болса, gnome-session сессияны аяқтаудың алдында пайдаланушыдан растауды сұрайтын болады. Іске қосылған болса, gnome-session сессияны автоматты түрде сақтайтын болады. Кез-келген бар болып тұрған ингибиторларды елемеу Жүйеден шығу Жүйеден шығу сұрауы Жауап бермейді Қап! Бір нәрсе қате кетті. Стандартты іске қосылу бумалардың орнына басқаларды қолдану Жөнелту үшін таңдауыңызша сессияны таңдаңыз Сөндіру Бағдарлама өазара ерегісетін опциялармен шақырылды Бағдарлама тек бір параметрді талап етеді Қайта қосу Жаңа клиенттік байланысын тайдыру, өйткені жүйе қазір сөндірілуде
 Есте сақталған қолданбалар Сессия атауын өз_герту dbus.service қызметі орындалып тұрса, оны қайта іске қосыңыз Қызмет ақаулығы кезінде gnome-session-failed.target іске қосу үшін, ExecStopPost ішінен жөнелту systemd қызметі ретінде орындалуда СЕССИЯ_АТЫ Сессияларды сақтау Бұл сессияны сақтау Сессия %d Сессия атауында "/" таңбасы болмауы тиіс Сессия атауы "." таңбасынан басталмауы тиіс Сессия атауы "." таңбасынан басталмауы және құрамында "/" таңбасы болмауы тиіс Қолдану үшін сессия Кеңейту ескертуін көрсету Сынау үшін "fail whale" сұхбатын көрсету Бастапқы сессияға оралу ескертуін көрсету gnome-session үшін сигнал инициализациясы дайын gnome-session-shutdown.target іске қосу Стандарты кіріске (stdin) EOF немесе бір байт алынған кезде, gnome-session-shutdown.target іске қосу Бұл элемент сізге сақталып тұрған сессияны таңдауға рұқсат етеді Бұл бағдарлама жүйеден шығуды болдырмай тұр. Бұл сессия арқылы GNOME-ға кіресіз Құрамындағы сессияларды басқаруды қолдану (systemd-негізіндегі орнына) systemd сессия басқаруын қолдану Осы қолданбаның нұсқасы Іске қосылған болса, gnome-session келесі сессияны шыққан кезде автосақтайды, автоматты түрде сақтау сөндірілген болса да. Жалға_стыру Жүйеден ш_ығу Жүйеден _шығу Ж_аңа сессия Сессияны ө_шіру 