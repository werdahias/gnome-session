��    ;      �  O   �           	  4  (     ]  T   v  i   �  \   5  '   �     �     �  (   �  )   �     (	     /	  #   >	  '   b	  $   �	     �	     �	     �	     �	     �	  q   �	  G   m
  >   �
      �
               +  !   :  '   \  %   �  	   �  '   �     �  P   �     4     K     [     h     v  
   �  ;   �  3   �  Q        U     d  &   {     �  *   �      �           )  p   E  	   �     �     �     �     �  �  �  f   �  y  7  d   �      k  4  4  �     �     U  U   c  �   �  �   k       [   "  �   ~  �     o   �  �     h   �           +      K   K  j   0  �!  �   �"  q   �#  '   /$  :   W$  ?   �$  s   �$  �   F%  �   �%  /   �&  �   �&  .   �'  �   �'  M   �(  ^   �(     X)  L   e)  8   �)  '   �)  z   *  �   �*  �   +  L   �+  t   ,  r   �,  e   �,  �   ]-  j   .  �   x.  G   /  �  I/     1  ,   01  ,   ]1  @   �1  X   �1         9      *         %      :           4   8      ;               5            7   !   '      &              (   	          ,      #              -       .      3                0      $      +       )       2       1                        /   6               
              "        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session.master.ml
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2018-10-26 13:13+0530
Last-Translator: Ranjith Siji <ranjith.sajeev@gmail.com>
Language-Team: Swatantra Malayalam Computing <discuss@lists.smc.org.in>
Language: ml
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.2
X-Project-Style: gnome
  — GNOME പ്രവര്‍ത്തനവേള നടത്തിപ്പുകാരന്‍ %s [OPTION...] COMMAND

ചില സെഷന്‍ പ്രവര്‍ത്തനങ്ങളെ തടസ്സപ്പെടുത്തികൊണ്ട് COMMAND നടപ്പിലാക്കുക.

  -h, --help        ഈ സഹായം കാണിക്കുക
  --version         പ്രോഗ്രാം പതിപ്പ് കാണിക്കുക
  --app-id ID       തടസ്സപ്പെടുത്തുമ്പോള്‍ ഉപയോഗിക്കേണ്ട
                                             ആപ്ലിക്കേഷന്‍ ഐഡി (വേണമെങ്കില്‍)
  --reason REASON   തടസ്സപ്പെടുത്താനുള്ള കാരണം (വേണമെങ്കില്‍)
  --inhibit ARG     എന്തൊക്കെ തടസ്സപ്പെടുത്തണം, കോളന്‍ കൊണ്ടു തിരിച്ച പട്ടിക:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    COMMAND തുടങ്ങിയിട്ട് എന്നേക്കുമായി കാത്തിരിക്കേണ്ട

--inhibit ഐച്ഛികങ്ങളൊന്നും തെരഞ്ഞെടുത്തിലെങ്കില്‍, ചുമ്മായിരിക്കുന്നതായി കരുതുന്നു.
 %s ന് ഒരു ആര്‍ഗ്യുമെന്റ് ആവശ്യ‌മുണ്ട്
 എന്തോ പ്രശ്നം കാരണം സിസ്റ്റം വീണ്ടെടുക്കുവാന്‍ സാധ്യമല്ല.
ദയവായി പുറത്തു് കടന്നു് വീണ്ടും ശ്രമിയ്ക്കുക. എന്തോ പ്രശ്നം കാരണം സിസ്റ്റം വീണ്ടെടുക്കുവാന്‍ സാധ്യമല്ല. മുന്‍കരുതലിനായി എല്ലാ ചേർപ്പുകളും പ്രവര്‍ത്തന രഹിതമാക്കിയിരിയ്ക്കുന്നു. എന്തോ പ്രശ്നം കാരണം സിസ്റ്റം വീണ്ടെടുക്കുവാന്‍ സാധ്യമല്ല. ദയവായി ഒരു സിസ്റ്റം കാര്യനിർവ്വാഹകനുമായി ബന്ധപ്പെടുക “%s” എന്ന പേരില്‍ ഒരു പ്രവർത്തനവേള നിലവിലുണ്ടു് AUTOSTART_DIR പുറത്തിറങ്ങാന്‍ അനുവദിയ്ക്കുക പ്രവര്‍ത്തനവേള നടത്തിപ്പുകാരനുമായി ബന്ധപ്പെടാന്‍ കഴിയുന്നില്ല 'ഐസ് '(ICE) കേള്‍ക്കാനുള്ള സോക്കറ്റ് ഉണ്ടാക്കാന്‍ കഴിഞ്ഞില്ല: %s യഥേഷ്ടം പ്രവര്‍ത്തനവേള ചിട്ടപ്പെടുത്തുക ഹാര്‍ഡ്‍വെയര്‍ ആക്സിലറേഷന്‍ പരിശോധന ഒഴിവാക്കുക ഉപയോക്താവ് വ്യക്തമാക്കിയ പ്രയോഗങ്ങള്‍ ലോഡ് ചെയ്യേണ്ട ഉറപ്പു വരുത്തുന്നതിനായി ഇനി ചോദിക്കേണ്ട പിഴവു് തിരുത്താന്‍ സഹായിയ്ക്കുന്ന കോഡ് പ്രാവര്‍ത്തികമാക്കുക %s നടപ്പിലാക്കുന്നതില്‍ പരാജയപ്പെട്ടു
 ഗ്നോം ഗ്നോം ഡമ്മി ഗ്നോം Xorg ല്‍ പ്രവര്‍ത്തന സജ്ജമാണെങ്കി, സെഷന്‍ ഓട്ടോമാറ്റിക്കായി ഫാള്‍ബാക്കായാല്‍, gnome-session ഒരു മുന്നറിയിപ്പ് ഡയലോഗ് പ്രദര്‍ശിപ്പിക്കും. പ്രവര്‍ത്തന സജ്ജമാണെങ്കില്‍, പ്രവര്‍ത്തനവേള അവസാനിക്കുന്നതിനു് മുമ്പു് gnome-session ഉപയോക്താവിനെ അറിയിക്കുന്നതാണു്. പ്രവര്‍ത്തന സജ്ജമാണെങ്കില്‍, gnome-session സ്വയമേ ഈ പ്രവര്‍ത്തനവേള സൂക്ഷിക്കുന്നതാണ്. തടസ്സമുണ്ടാക്കുന്നവരെ വക വയ്ക്കുന്നില്ല പുറത്തിറങ്ങുക ലോഗൗട്ട് പ്രോംപ്റ്റ് പ്രതികരിയ്ക്കുന്നില്ല അയ്യോ!   എന്തോ പിശക് സംഭവിച്ചിരിയ്ക്കുന്നു. സാധാരണയുള്ള സ്വയം തുടങ്ങാനുള്ള തട്ടുകള്‍ മറികടക്കുക പ്രവർത്തിപ്പിക്കാനായി ഇഷ്ടമുള്ള ഒരു പ്രവർത്തനവേള തിരഞ്ഞെടുക്കുക പവര്‍ ഓഫ് ചെയ്യുക പരസ്പരവിരുദ്ധമായ ഐച്ഛികങ്ങളോടെയാണു് പ്രോഗ്രാമിനെ വിളിച്ചിരിയ്ക്കുന്നതു് വീണ്ടും തുടങ്ങുക പുതിയ ക്ലയന്റ് ബന്ധം നിരസിക്കുന്നു കാരണം പ്രവര്‍ത്തനവേള ഇപ്പോള്‍ അടച്ചു പൂട്ടൂകയാണു്
 ഓര്‍ത്തു് വച്ച പ്രയോഗങ്ങള്‍ പ്രവര്‍ത്തനവേളയുടെ പേര് മാറ്റുക (_m) SESSION_NAME പ്രവര്‍ത്തനവേള സൂക്ഷിക്കുക ഈ സെഷന്‍ സൂക്ഷിക്കുക പ്രവർത്തനവേള %d പ്രവർത്തനവേളയുടെ പേരില്‍ “/” കാണാന്‍ പാടില്ല “.” വെച്ച് പ്രവർത്തനവേളയുടെ പേര് തുടങ്ങാന്‍ പാടില്ല പ്രവർത്തനവേളയുടെ പേര് “.” വെച്ച് തുടങ്ങാനോ “/” കാണാനോ പാടില്ല ഉപയോഗിയ്ക്കുവാനുള്ള സെഷന്‍ എക്സ്റ്റെന്‍ഷന്‍ മുന്നറിയിപ്പ് കാണിക്കുക പരീക്ഷണത്തിനായി പരാജയ ഡയലോഗ് കാണിയ്ക്കുക ഫാള്‍ബാക്ക് മുന്നറിയിപ്പ് കാണിക്കുക സൂക്ഷിച്ച ഒരു പ്രവര്‍ത്തനവേള തെരഞ്ഞെടുക്കാന്‍ ഇതനുവദിക്കുന്നു ഈ പ്രയോഗം പുറത്തിറങ്ങുന്നതു് തടയുന്നു. ഈ പ്രവര്‍ത്തനവേള നിങ്ങളെ ഗ്നോമിലേക്ക് കയറ്റുന്നു ഈ പ്രയോഗത്തിന്റെ പതിപ്പു് പ്രവര്‍ത്തന സജ്ജമാണെങ്കില്‍, സ്വയമേ സൂക്ഷിക്കുക എന്നത് പ്രവര്‍ത്തനരഹിതമാണെങ്കില്‍പോലും ലോഗൗട്ട് ചെയ്യുമ്പോള്‍ gnome-session സ്വയമേ അടുത്ത പ്രവര്‍ത്തനവേള സൂക്ഷിക്കുന്നതാണു്. തുടരുക (_C) പുറത്തിറങ്ങുക (_L) പുറത്തിറങ്ങുക (_L) പുതിയ പ്രവര്‍ത്തനവേള  (_N) പ്രവര്‍ത്തനവേള നീക്കം ചെയ്യുക (_R) 