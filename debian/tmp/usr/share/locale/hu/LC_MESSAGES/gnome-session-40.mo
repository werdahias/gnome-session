��    D      <  a   \      �     �  p        q  T   �  i   �  \   I	  '   �	     �	     �	  (   �	  )   
     <
     C
  #   R
  '   v
  $   �
     �
     �
     �
     �
       q     G   �  >   �           )     1     ?  !   N  '   p  %   �  	   �  '   �  #   �       P        l     �  %   �  M   �          "     /     =  
   O  ;   Z  3   �  Q   �          +  &   B     i  +   �  #   �  P   �  *   $      O      p  B   �     �     �  p     	   �     �     �     �     �  �  �  !   q  �  �     m  ^   �  y   �  `   c  '   �     �     �  5     0   T     �     �  7   �  =   �  )     "   @     c     �     �     �  �   �  f   +  L   �  $   �               .     <  A   S  +   �     �     �  5   �     $  J   3     ~     �  &   �  r   �  %   I     o          �     �  7   �  /   �  Z   *     �  2   �  .   �  )      ?   *  *   j  m   �  D      )   H   #   r   F   �   '   �      !  �    !     �!     �!     �!     �!     �!         !                    #       *   9   6   4           0   )              8       (      '   $   %           >                     	          -          A   D   ,                                        B              ?   ;              :      
       +   .   3          /   5   1           2   =          @      "      C                7   &      <        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session master
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2020-09-05 17:10+0200
Last-Translator: Meskó Balázs <mesko.balazs@fsf.hu>
Language-Team: Hungarian <gnome-hu-list at gnome dot org>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.1
Plural-Forms: nplurals=2; plural=(n != 1);
  – A GNOME munkamenet-kezelője %s [KAPCSOLÓ…] PARANCS

A PARANCS végrehajtása egyes munkamenet-funkciók tiltásával.

  -h, --help          Ezen súgó megjelenítése
  --version           Programverzió megjelenítése
  --app-id AZONOSÍTÓ  Használandó alkalmazásazonosító
                      a letiltáskor (elhagyható)
  --reason REASON     A letiltás oka (elhagyható)
  --inhibit ARG       Letiltandó funkciók vesszőkkel elválasztva:
                      logout, switch-user, suspend, idle, automount
  --inhibit-only      Ne indítsa a PARANCSOT, csak várjon a végtelenségig
  -l, --list          A meglévő letiltások felsorolása, majd kilépés

Ha nincs megadva az --inhibit kapcsoló, akkor az idle az alapértelmezett.
 %s egy paramétert igényel
 Hiba történt, és a rendszer nem képes helyreállni.
Jelentkezzen ki, és próbálja újra. Hiba történt, és a rendszer nem képes helyreállni. Elővigyázatosságból minden kiterjesztés letiltásra került. Hiba történt, és a rendszer nem képes helyreállni. Lépjen kapcsolatba a rendszergazdával. Már létezik „%s” nevű környezet AUTO_INDÍTÁS_KVT Kijelentkezés engedélyezése Sikertelen kapcsolatfelvétel a munkamenet-kezelővel Nem hozható létre az ICE-figyelő foglalat: %s Egyéni Egyéni környezet A hardveres gyorsítás ellenőrzésének kikapcsolása Ne töltsön be felhasználó által megadott alkalmazásokat Ne kérjen felhasználói megerősítést Hibakeresési kód engedélyezése %s végrehajtása sikertelen
 GNOME GNOME példa GNOME Xorgon Ha engedélyezett, akkor a gnome-session egy figyelmeztetési párbeszédablakot jelenít meg, ha a munkamenet visszaállt a tartalékra. Ha engedélyezett, akkor a gnome-session megkérdezi a felhasználót a munkamenet befejezése előtt. Ha engedélyezett, akkor a gnome-session automatikusan menti a munkamenetet. A meglévő korlátozók mellőzése Kijelentkezés Kérdés kijelentkezéskor Nem válaszol Jaj! Valami elromlott. Szabványos automatikus indítási könyvtárak felülbírálása Válasszon egyéni futtatandó környezetet Kikapcsolás A program kapcsolói ütköznek A programnak pontosan egy paraméterre van szüksége Újraindítás Új ügyfélkapcsolatok visszautasítása, mivel a rendszer éppen leáll
 Megjegyzett alkalmazás Környezet át_nevezése A dbus.service újraindítása, ha fut Futtatás az ExecStopPost beállításból a gnome-session-failed.target indításához szolgáltatáshiba esetén Futtatás systemd szolgáltatásként MUNKAMENET_NEVE Munkamenetek mentése Aktuális környezet mentése %d. környezet A környezetnevek nem tartalmazhatnak „/” karaktert A környezetnevek nem kezdődhetnek „.”-tal A környezetnevek nem kezdődhetnek „.”-tal, és nem tartalmazhatnak „/” karaktert Használandó munkamenet Kiterjesztés figyelmeztetésének megjelenítése A hibaképernyő megjelenítése teszteléshez Tartalék figyelmeztetés megjelenítése A szignál előkészítése kész a gnome-session munkamenethez A gnome-session-shutdown.target indítása A gnome-session-shutdown.target indítása, ha EOF vagy egyedülálló bájt érkezik a szabványos bemeneten Ez a bejegyzés lehetővé teszi mentett munkamenet kiválasztását Ez a program blokkolja a kijelentkezést. Bejelentkezés a GNOME környezetbe Beépített munkamenet-kezelés használata (a systemd alapú helyett) Systemd munkamenet-kezelés használata Ezen alkalmazás verziója Ha engedélyezett, akkor a gnome-session automatikusan elmenti a következő munkamenetet kijelentkezéskor, akkor is ha az automatikus mentés tiltott. _Folytatás _Kijelentkezés _Kijelentkezés Új _környezet Környezet _eltávolítása 