��    D      <  a   \      �     �  p        q  T   �  i   �  \   I	  '   �	     �	     �	  (   �	  )   
     <
     C
  #   R
  '   v
  $   �
     �
     �
     �
     �
       q     G   �  >   �           )     1     ?  !   N  '   p  %   �  	   �  '   �  #   �       P        l     �  %   �  M   �          "     /     =  
   O  ;   Z  3   �  Q   �          +  &   B     i  +   �  #   �  P   �  *   $      O      p  B   �     �     �  p     	   �     �     �     �     �  �  �  !   �  �  �     >  a   X  s   �  b   .  $   �     �     �  -   �  -        @     N  4   d  7   �  %   �  !   �          0     6     F  �   T  T   �  G   F  '   �     �     �     �     �  9   �  1   2     d  /   k  .   �  	   �  \   �     1     G  -   Z  b   �  &   �          '     8  
   M  =   X  @   �  d   �     <  '   M  ,   u  (   �  4   �  %      W   &  8   ~  4   �     �  L      $   U      z   �   �   
   *!     5!     H!     \!     k!         !                    #       *   9   6   4           0   )              8       (      '   $   %           >                     	          -          A   D   ,                                        B              ?   ;              :      
       +   .   3          /   5   1           2   =          @      "      C                7   &      <        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session.master
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2020-08-13 10:03+0200
Last-Translator: Daniel Mustieles <daniel.mustieles@gmail.com>
Language-Team: Spanish - Spain <gnome-es-list@gnome.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Gtranslator 3.36.0
Plural-Forms: nplurals=2; plural=(n != 1);
  - El gestor de sesiones de GNOME %s [OPCIÓN...] COMANDO

Ejecutar COMANDO al inhibir alguna funcionalidad de la sesión.

  -h, --help        Mostrar esta ayuda
  --version         Mostrar la versión del programa
  --app-id ID       El ID de aplicación que usar
                    al inhibir (opcional)
  --reason RAZÓN   La razón para inhibir (opcional)
  --inhibit ARG     Qué inhibir, lista de elementos separados por dos puntos de:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    No ejecutar COMANDO y esperar indefinidamente
 -l, --list        Listar las inhibiciones existentes y salir

Si no se especifica la opción --inhibit se asume «idle».
 %s requiere un argumento
 Ocurrió un problema y el sistema no se puede recuperar.
Cierre la sesión e inténtelo de nuevo. Ocurrió un problema y el sistema no se puede recuperar. Por precaución, se han desactivado todas las extensiones. Ocurrió un problema y el sistema no se puede recuperar. Contacte con un administrador del sistema Ya existe una sesión llamada «%s» CARPETA_DE_AUTOINICIO Permitir cerrar sesión No se pudo conectar con el gestor de sesiones No se pudo crear el socket de escucha ICE: %s Personalizada Sesión personalizada Desactivar la verificación de aceleración hardware No cargar las aplicaciones especificadas por el usuario No requerir confirmación del usuario Activar el código de depuración Falló al ejecutar %s
 GNOME GNOME «dummy» GNOME en Xorg Si está activado, gnome-session mostrará un diálogo de advertencia después del inicio de sesión si se ha iniciado automáticamente el modo alternativo. Si está activado, gnome-session preguntará al usuario antes de cerrar una sesión. Si está activado, gnome-session guardará la sesión automáticamente. Ignorando cualquier inhibidor existente Cerrar la sesión Diálogo de salida No responde Algo salió mal. Sobrescribir las carpetas de inicio automático estándar Seleccione una sesión personalizada que ejecutar Apagar Se llamó al programa con opciones en conflicto El programa necesita exactamente un parámetro Reiniciar Rechazando la conexión de un nuevo cliente porque actualmente se está cerrando la sesión
 Aplicación recordada Reno_mbrar sesión Reiniciar dbus.service si está en ejecución Ejecutar desde ExecStopPost para iniciar gnome-session-failed.target en caso de fallo del servicio Ejecutándose como servicio de systemd NOMBRE_DE_LA_SESIÓN Guardar sesiones Guardar esta sesión Sesión %d No se permite que los nombres de las sesiones contengan «/» No se permite que los nombres de las sesiones empiecen por «.» No se permite que los nombres de las sesiones empiecen por «.» ni que contengan el carácter «/» Sesión que usar Mostrar advertencias de las extensiones Mostrar el diálogo de la ballena de pruebas Mostrar advertencia del modo alternativo Inicialización de señales hecha para gnome-session Iniciar gnome-session-shutdown.target Iniciar gnome-session-shutdown.target cuando se reciba un EOF o un único byte en stdin Esta entrada le permite seleccionar una sesión guardada Este programa está bloqueando el cierre de sesión. Esta sesión accede a GNOME Usar la gestión de sesiones implementada (en lugar de la basada en systemd) Usar el gestor de sesión de systemd Versión de esta aplicación Si está activado, gnome-session guardará automáticamente la próxima sesión cuando salga incluso si el guardado automático está desactivado. _Continuar Ce_rrar la sesión _Cerrar la sesión Sesión _nueva Sesión _remota 