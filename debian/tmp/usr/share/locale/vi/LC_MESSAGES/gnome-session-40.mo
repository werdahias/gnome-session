��    D      <  a   \      �     �  p        q  T   �  i   �  \   I	  '   �	     �	     �	  (   �	  )   
     <
     C
  #   R
  '   v
  $   �
     �
     �
     �
     �
       q     G   �  >   �           )     1     ?  !   N  '   p  %   �  	   �  '   �  #   �       P        l     �  %   �  M   �          "     /     =  
   O  ;   Z  3   �  Q   �          +  &   B     i  +   �  #   �  P   �  *   $      O      p  B   �     �     �  p     	   �     �     �     �     �  �  �  -   o    �     �  ]   �  |   1  x   �  4   '     \     s  C   �  0   �             *   +  :   V  4   �     �     �                 �   *  b   �  V     &   s     �     �     �  0   �  6     ;   8  
   t  F     3   �     �  S        c     {  8   �  ^   �  #   3     W     c     {     �  @   �  R   �  l   B     �  %   �  7   �     ,   1   H   +   z   d   �   >   !  5   J!  ;   �!  N   �!  +   "     7"  �   W"     �"     �"     #     #     6#         !                    #       *   9   6   4           0   )              8       (      '   $   %           >                     	          -          A   D   ,                                        B              ?   ;              :      
       +   .   3          /   5   1           2   =          @      "      C                7   &      <        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session master
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2021-03-20 14:29+0700
Last-Translator: Trần Ngọc Quân <vnwildman@gmail.com>
Language-Team: Vietnamese <gnome-vi-list@gnome.org>
Language: vi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Gtranslator 2.91.7
  — bộ quản lý phiên làm việc GNOME %s [TÙY CHỌN…] LỆNH

Thực hiện LỆNH nhưng hạn chế một số chức năng liên quan đến phiên.

  -h, --help        Hiển thị trợ giúp này
  --version         Hiển thị phiên bản của chương trình
  --app-id ID       Mã số ứng dụng sẽ dùng
                    khi hạn chế (tùy chọn)
  --reason LÝ_DO    Lý do hạn chế (tùy chọn)
  --inhibit ARG     Những thứ hạn chế, danh sách ngăn cách bằng dấu hai chấm
                      của: logout, switch-user, suspend, idle, automount
  --inhibit-only    Không khởi chạy LỆNH và thay vào đó là chờ mãi mãi
  -l, --list        Liệt kê những hạn chế sẵn có rồi thoát

Nếu không đưa ra tùy chọn --inhibit, coi là idle (nghỉ).
 %s cần một đối số
 Hệ thống gặp vấn đề không thể phục hồi.
Hãy đăng xuất và thử lại. Hệ thống gặp vấn đề không thể phục hồi. Mọi phần mở rộng đã bị tắt để đề phòng lỗi. Hệ thống gặp vấn đề không thể phục hồi. Vui lòng liên lạc với người quản trị hệ thống Đã sẵn có phiên làm việc mang tên “%s” THƯ_MỤC_TỰ_CHẠY Cho phép đăng xuất Không thể kết nối với trình quản lý phiên làm việc Không thể tạo ổ cắm lắng nghe ICE: %s Tự chọn Phiên làm việc tự chọn Tắt kiểm tra tăng tốc phần cứng Đừng tải ứng dụng do người dùng chỉ định Đừng nhắc yêu cầu xác nhận người dùng Bật mã hỗ trợ tìm lỗi Gặp lỗi khi chạy %s
 GNOME Giả lập GNOME GNOME trên Xorg Nếu bật, gnome-session sẽ hiển thị hộp thoại cảnh báo sau khi đăng nhập nếu như phiên được tự động fallback. Nếu bật, gnome-session sẽ nhắc người dùng trước khi kết thúc phiên làm việc. Nếu được chọn, gnome-session sẽ tự động lưu phiên phiên làm việc. Bỏ qua các ứng dụng ngăn cản Đăng xuất Nhắc đăng xuất Không trả lời Ôi không! Có cái gì đó bị hỏng rồi. Đè lên thư mục tự khởi động tiêu chuẩn Vui lòng chọn phiên làm việc mà bạn muốn chạy Tắt máy Chương trình được gọi với các tùy chọn xung đột nhau Chương trình cần chính xác một đối số Khởi động lại Đang từ chối kết nối khách mới vì phiên làm việc đang bị tắt
 Ứng dụng đã nhớ Đổi _tên phiên làm việc Khởi động lại dbus.service nếu nó đang chạy Chạy từ ExecStopPost để khởi chạy gnome-session-failed.target khi lỗi dịch vụ Chạy như là dịch vụ systemd TÊN_PHIÊN Lưu phiên làm việc Lưu phiên làm việc này Phiên làm việc %d Tên phiên làm việc không cho phép chứa ký tự “/” Tên phiên làm việc không được phép bắt đầu bằng ký tự “.” Tên phiên làm việc không được phép bắt đầu bằng ký tự “.” hoặc có chứa “/” Phiên làm việc cần dùng Hiện cảnh báo phần mở rộng Hiện cửa sổ cá voi thất bại để kiểm tra Hiện cảnh báo fallback Khởi tạo tín hiệu xong với gnome-session Khởi chạy gnome-session-shutdown.target Khởi chạy gnome-session-shutdown.target khi nhận được EOF hay một byte đơn trên stdin Mục này cho phép bạn chọn phiên làm việc đã lưu Chương trình này đang ngăn cản đăng xuất. Phiên làm việc này sẽ đăng nhập bạn vào GNOME Dùng quản lý phiên làm việc dựng sẵn (thay vì dựa vào systemd) Dùng quản lý phiên làm việc systemd Phiên bản của ứng dụng Nếu được bật, gnome-session sẽ lưu tự động phiên làm việc kế tiếp lúc đăng xuất ngay cả khi tự động lưu bị tắt. _Tiếp tục Đăng _xuất Đăng _xuất Phiên làm việc _mới Phiên làm việc từ _xa 