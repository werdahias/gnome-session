��    D      <  a   \      �     �  p        q  T   �  i   �  \   I	  '   �	     �	     �	  (   �	  )   
     <
     C
  #   R
  '   v
  $   �
     �
     �
     �
     �
       q     G   �  >   �           )     1     ?  !   N  '   p  %   �  	   �  '   �  #   �       P        l     �  %   �  M   �          "     /     =  
   O  ;   Z  3   �  Q   �          +  &   B     i  +   �  #   �  P   �  *   $      O      p  B   �     �     �  p     	   �     �     �     �     �  �  �     h  w  �     �  ]     {   u  b   �  .   T     �     �  *   �  (   �     �        .     '   @  '   h     �     �     �     �     �  �   �  S   |  B   �  !        5     >     P  "   \  3     #   �  	   �  '   �  &   	  	   0  T   :     �     �  #   �  Z   �     8     R     _     o  
   �  0   �  )   �  I   �     3     H  *   ^     �  (   �  $   �  Y   �  .   G  %   v  #   �  D   �  !        '  �   @  
   �  	   �  	   �     �     �         !                    #       *   9   6   4           0   )              8       (      '   $   %           >                     	          -          A   D   ,                                        B              ?   ;              :      
       +   .   3          /   5   1           2   =          @      "      C                7   &      <        — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2020-08-02 01:01+0200
Last-Translator: Anders Jonsson <anders.jonsson@norsjovallen.se>
Language-Team: Swedish <tp-sv@listor.tp-sv.se>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 2.3.1
  — GNOME-sessionshanteraren %s [FLAGGA…] KOMMANDO

Exekvera KOMMANDO medan någon funktionalitet förhindras.

  -h, --help        Visa denna hjälp
  --version         Visa programversion
  --app-id ID       Program-ID att använda
                    vid förhindrande (valfri)
  --reason ORSAK    Orsaken till förhindrande (valfri)
  --inhibit ARG     Saker att förhindra, kolonseparerad lista av:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Kör inte KOMMANDO utan vänta för alltid i stället
  -l, --list        Lista befintliga förhindranden och avsluta


Om ingen --inhibit-flagga angivits, antas idle.
 %s kräver ett argument
 Ett problem har inträffat och systemet kan inte återhämta sig.
Logga ut och försök igen. Ett problem har inträffat och systemet kan inte återhämta sig. Alla tillägg har inaktiverats som en säkerhetsåtgärd. Ett problem har inträffat och systemet kan inte återhämta sig. Kontakta en systemadministratör En session med namnet ”%s” existerar redan AUTOSTARTKAT Tillåt utloggning Kunde inte ansluta till sessionshanteraren Kunde inte skapa lyssnande ICE-uttag: %s Anpassad Anpassad session Inaktivera kontroll för hårdvaruacceleration Läs inte in användarspecifika program Fråga inte efter användarbekräftelse Aktivera felsökningskod Kunde inte exekvera %s
 GNOME GNOME-attrapp GNOME med Xorg Om aktiverad kommer gnome-session att visa en varningsdialog efter inloggning om sessionen automatiskt föll tillbaka till att använda gnome-fallback. Om aktiverad kommer gnome-session att fråga användaren innan en session avslutas. Om aktiverad kommer gnome-session att spara sessionen automatiskt. Ignorera allt som förhindrar det Logga ut Utloggningsfråga Svarar inte Åh nej! Någonting har gått fel. Åsidosätt standardkataloger för automatisk start Välj en anpassad session att köra Stäng av Program anropat med motsägande flaggor Programmet behöver exakt en parameter Starta om Avvisar ny klientanslutning på grund av att sessionen är på väg att stängas av
 Ihågkommet program Byt na_mn på session Starta om dbus.service om den körs Kör från ExecStopPost för att starta gnome-session-failed.target då tjänst misslyckas Körs som systemd-tjänst SESSIONSNAMN Spara sessioner Spara denna session Session %d Sessionsnamn får inte innehålla ”/”-tecken Sessionsnamn får inte börja med ”.” Sessionsnamn får inte börja med ”.” eller innehålla ”/”-tecken Session att använda Visa tilläggsvarning Visa den ”felande valen” för testning Visa fallback-varning Signalinitiering gjord på gnome-session Starta gnome-session-shutdown.target Starta gnome-session-shutdown.target då EOF eller en ensam byte tas emot på standard in Denna post låter dig välja en sparad session Detta program blockerar utloggningen. Denna session loggar in dig i GNOME Använd inbyggd sessionshantering (snarare än den systemd-baserade) Använd systemd-sessionshantering Version av detta program Om aktiverad kommer gnome-session automatiskt att spara nästa session vid utloggning även om automatisk sparning är inaktiverad. _Fortsätt _Logga ut _Logga ut _Ny session _Ta bort session 