Þ    D      <  a   \      à     á  p        q  T     i   ß  \   I	  '   ¦	     Î	     Ü	  (   é	  )   
     <
     C
  #   R
  '   v
  $   
     Ã
     Ù
     ï
     õ
       q     G     >   É           )     1     ?  !   N  '   p  %     	   ¾  '   È  #   ð       P        l       %     M   ¹          "     /     =  
   O  ;   Z  3     Q   Ê          +  &   B     i  +     #   ¯  P   Ó  *   $      O      p  B        Ô     ó  p     	                       ©  ©  ¹      c  c       è  @   ù  T   :  <     &   Ì     ó            "   -     P     W     j  *        ®     Ä     Ô     å     ë     ÷  t   	  S   ~  A   Ò  !        6     =     J  $   W  !   |  '        Æ  !   Ó  $   õ       L   '     t       /   §  N   ×     &     ?     L     _     u  .     +   ´  A   à     "     ;  '   T     |  &     $   ¶  Y   Û  -   5  !   c  $     B   ª  '   í       z   +  
   ¦  
   ±  
   ¼     Ç     Þ         !                    #       *   9   6   4           0   )              8       (      '   $   %           >                     	          -          A   D   ,                                        B              ?   ;              :      
       +   .   3          /   5   1           2   =          @      "      C                7   &      <        â the GNOME session manager %s [OPTIONâ¦] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead
  -l, --list        List the existing inhibitions, and exit

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system canât recover.
Please log out and try again. A problem has occurred and the system canât recover. All extensions have been disabled as a precaution. A problem has occurred and the system canât recover. Please contact a system administrator A session named â%sâ already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Donât prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg If enabled, gnome-session will display a warning dialog after login if the session was automatically fallen back. If enabled, gnome-session will prompt the user before ending a session. If enabled, gnome-session will save the session automatically. Ignoring any existing inhibitors Log out Logout prompt Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Program needs exactly one parameter Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session Restart dbus.service if it is running Run from ExecStopPost to start gnome-session-failed.target on service failure Running as systemd service SESSION_NAME Save sessions Save this session Session %d Session names are not allowed to contain â/â characters Session names are not allowed to start with â.â Session names are not allowed to start with â.â or contain â/â characters Session to use Show extension warning Show the fail whale dialog for testing Show the fallback warning Signal initialization done to gnome-session Start gnome-session-shutdown.target Start gnome-session-shutdown.target when receiving EOF or a single byte on stdin This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Use builtin session management (rather than the systemd based one) Use systemd session management Version of this application When enabled, gnome-session will automatically save the next session at log out even if auto saving is disabled. _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session 3.1.92
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2020-10-12 21:01+0800
Last-Translator: Cheng-Chia Tseng <pswo10680@gmail.com>
Language-Team: Chinese <zh-l10n@linux.org.tw>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 2.4.1
  â GNOME å·¥ä½éæ®µç®¡çå¡ %s [OPTIONâ¦] COMMAND

æå¶å·¥ä½éæ®µçæäºåè½çåæå·è¡ COMMAND ã

  -h, --help        é¡¯ç¤ºéåæ±å©æä»¶
  --version         é¡¯ç¤ºç¨å¼çæ¬
  --app-id ID       æå¶æè¦ä½¿ç¨ç
                    æç¨ç¨å¼ id (é¸å¡«)
  --reason REASON   æå¶çåå  (é¸å¡«)
  --inhibit ARG     è¦æå¶çé ç®ï¼ä»¥åèåéçæ¸å®ï¼
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    ä¸è¦å·è¡ COMMAND èä»¥ç­å¾ä¾åä»£
  -l, --list        ååºæ¢æçæå¶ä¸¦é¢é

å¦ææ²ææå® --inhibit é¸é ï¼æåå®çº idleã
 %s éè¦å¼æ¸
 ç¼çåé¡ä¸ç³»çµ±ç¡æ³æ¢å¾©ã
è«ç»åºå¾åè©¦ä¸æ¬¡ã ç¼çåé¡ä¸ç³»çµ±ç¡æ³æ¢å¾©ãçºäºé é²å·²ååç¨ææçæ´ååè½ã ç¼çåé¡ä¸ç³»çµ±ç¡æ³æ¢å¾©ãè«é£çµ¡ç³»çµ±ç®¡çè å·²å­å¨åçºã%sãçå·¥ä½éæ®µ AUTOSTART_DIR åè¨±ç»åº ç¡æ³é£æ¥å·¥ä½éæ®µç¸½ç®¡ ç¡æ³å»ºç« ICE ç£è½ææ§½ï¼%s èªè¨ èªè¨å·¥ä½éæ®µ åç¨ç¡¬é«å éæª¢æ¥ ä¸è¦è¼å¥ä½¿ç¨èæå®çæç¨ç¨å¼ ç¡éä½¿ç¨èç¢ºèª åç¨é¤é¯ç¢¼ ç¡æ³å·è¡ %s
 GNOME GNOME dummy GNOME æ¡è¡ Xorg è¥åç¨æ­¤é¸é ï¼gnome-session å°æå¨å·¥ä½éæ®µèªåéå¥å¾åæï¼æ¼ç»å¥å¾é¡¯ç¤ºè­¦åå°è©±æ¡ã å¦åç¨æ¬é¸é ï¼gnome-session æå¨ä½æ¥­éæ®µçµæ­¢ååæç¤ºä½¿ç¨èã å¦åç¨æ¬é¸é ï¼gnome-session æèªåå²å­ä½æ¥­éæ®µã å¿½ç¥ä»»ä½ç¾æçéå¶å ç´  ç»åº ç»åºæç¤º æ²æåæ ç³ç³ï¼æäºå°æ¹åºåé¡äºã è¦èæ¨æºçèªåååç®é è«é¸æè¦å·è¡çèªè¨å·¥ä½éæ®µ ééé»æº ä»¥ç¸è¡çªçé¸é å¼å«ç¨å¼ ç¨å¼æç¢ºæåºéè¦ä¸ååæ¸ éæ°éæ© å çºç®åæ­¤å·¥ä½éæ®µå³å°ééé»è¦ï¼æçµæ°çå®¢æ¶ç«¯é£ç·
 è¨ä½çæç¨ç¨å¼ éæ°å½åå·¥ä½éæ®µ(_M) å¦æ dbus.service æ­£å¨å·è¡åéæ°åå å¨æåå¤±æå¾å¾ ExecStopPost å·è¡ä»¥åå gnome-session-failed.target ä»¥ systemd æåå·è¡ SESSION_NAME å²å­å·¥ä½éæ®µ å²å­æ­¤å·¥ä½éæ®µ å·¥ä½éæ®µ %d å·¥ä½éæ®µåç¨±ä¸åè¨±åå«ã/ãå­å å·¥ä½éæ®µåç¨±ä¸åè¨±ä»¥ã.ãéé ­ å·¥ä½éæ®µåç¨±ä¸åè¨±ä»¥ã.ãéé ­æåå«ã/ãå­å è¦ä½¿ç¨çå·¥ä½éæ®µ é¡¯ç¤ºæ´ååè½è­¦å é¡¯ç¤ºå´éé¯èª¤å°è©±çä»¥ä¾æ¸¬è©¦ é¡¯ç¤ºå¾åè­¦å è¨èåå§åå®æçµ¦ gnome-session åå gnome-session-shutdown.target ç¶å¨æ¨æºè¼¸å¥æ¥æ¶å° EOF æä¸åä½åçµæåå gnome-session-shutdown.target éåé ç®è®æ¨é¸æå²å­çå·¥ä½éæ®µ éåç¨å¼æ­£å¨é»æç»åºã éåå·¥ä½éæ®µè®æ¨ç»å¥ GNOME ä½¿ç¨å§å»ºï¼èéåºæ¼ systemd çï¼å·¥ä½éæ®µç®¡çå·¥å· ä½¿ç¨ systemd å·¥ä½éæ®µç®¡çå·¥å· éåç¨å¼ççæ¬ ç¶åç¨æ­¤é¸é ï¼gnome-session å°å¨ç»åºæèªåå²å­ä¸ä¸åå·¥ä½éæ®µï¼å°±ç®èªåå²å­çºåç¨çæã ç¹¼çº(_C) ç»åº(_L) ç»åº(_L) æ°å¢å·¥ä½éæ®µ(_N) ç§»é¤å·¥ä½éæ®µ(_R) 