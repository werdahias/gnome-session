��    <      �  S   �      (     )  4  F     {  R   �  g   �  Z   O  '   �     �     �     �  	   	  	   	  	   	  (   )	  )   R	     |	     �	  '   �	  "   �	     �	     �	     	
     
     
      ,
     M
     U
  !   d
     �
  '   �
  %   �
  	   �
  '   �
       P        f     }     �  
   �  ;   �  3   �  Q        g     v  &   �     �  *   �      �        /   2     b  =   ~  	   �     �     �     �     �  (   �       �  %  <   
  A  G  c   �  B  �  x  0  <  �  �   �     r  V   �  :   �          .     D  �   `  ~   �  !   t  7   �  g   �  \   6  W   �  P   �     <     B     X  o   v     �  <     H   ?     �  l   �  �      $   �   �   �   &   @!  �   g!  C   T"  I   �"     �"     �"  �   #  �   �#  �   $  4   �$  V   �$  m   E%  7   �%  �   �%  j   �&  �   '  {   �'  ,   #(  �   P(     )      .)      O)     p)  #   �)  �   �)  H   .*     $             3       0                  4                    -       	           7          1   )           #   *      (                 "                &   +      /         5   6            9   ;         <       2           '   !           ,             :   .   %       
                   8        - the GNOME session manager %s [OPTION...] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can't recover.
Please log out and try again. A problem has occurred and the system can't recover. All extensions have been disabled as a precaution. A problem has occurred and the system can't recover. Please contact a system administrator A session named ‘%s’ already exists AUTOSTART_DIR Additional startup _programs: Allow logout Browse… Co_mmand: Comm_ent: Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Do not load user-specified applications Don't prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Wayland Ignoring any existing inhibitors Log out Not responding Oh no!  Something has gone wrong. Options Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Session %d Session names are not allowed to contain ‘/’ characters Session names are not allowed to start with ‘.’ Session names are not allowed to start with ‘.’ or contain ‘/’ characters Session to use Show extension warning Show the fail whale dialog for testing Startup Programs This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME This session logs you into GNOME, using Wayland Version of this application _Automatically remember running applications when logging out _Continue _Log Out _Log out _Name: _New Session _Remember Currently Running Applications _Remove Session Project-Id-Version: gnome-session.master.kn
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-session&keywords=I18N+L10N&component=general
PO-Revision-Date: 2014-09-18 16:48+0530
Last-Translator: Shankar Prasad <svenkate AT redhat Dot com>
Language-Team: Kannada <kde-i18n-doc@kde.org>
Language: kn
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.5
 - GNOME ಅಧಿವೇಶನ ವ್ಯವಸ್ಥಾಪಕ %s [OPTION...] COMMAND

ಕೆಲವು ಅಧಿವೇಶನ ಕಾರ್ಯಶೀಲತೆಯನ್ನು ತಡೆಯುವ ಮೂಲಕ COMMAND ಅನ್ನು ಕಾರ್ಯಗತಗೊಳಿಸಿ.

  -h, --help        ಈ ನೆರವನ್ನು ತೋರಿಸು
  --version         ಕಾರ್ಯಕ್ರಮದ ಆವೃತ್ತಿಯನ್ನು ತೋರಿಸು
  --app-id ID       ತಡೆಹಿಡಿಯುವಾಗ ಬಳಸಬೇಕಿರುವ 
                    ಅನ್ವಯದ id (ಐಚ್ಛಿಕ)
  --reason REASON   ತಡೆಹಿಡಿಯಲು ಕಾರಣ (ಐಚ್ಛಿಕ)
  --inhibit ARG     ತಡೆಹಿಡಿಯಬೇಕಿರುವ ವಿಷಯಗಳು, ವಿರಾಮಚಿಹ್ನೆಯಿಂದ ಬೇರ್ಪಡಿಸಲಾದ ಇವುಗಳ ಪಟ್ಟಿ:
                    ನಿರ್ಗಮನ, ಬಳಕೆದಾರನನ್ನು-ಬದಲಿಸು, ಸ್ಥಗಿತಗೊಳಿಸು, ಜಡ, ಸ್ವಯಂಏರಿಸುವಿಕೆ
  --inhibit-only   COMMAND ಅನ್ನು ಆರಂಭಿಸಬೇಡ ಮತ್ತು ಬದಲಿಗೆ ಯಾವಾಗಲೂ ಕಾಯುತ್ತಿರು

ಯಾವುದೆ --inhibit ಆಯ್ಕೆಯನ್ನು ಸೂಚಿಸದೆ ಇದ್ದಲ್ಲಿ, ಜಡ (idle) ಎಂದು ಊಹಿಸಲಾಗುತ್ತದೆ.
 %s ಒಂದು ಆರ್ಗ್ಯುಮೆಂಟಿನ ಅಗತ್ಯವಿರುತ್ತದೆ
 ಒಂದು ತೊಂದರೆ ಉಂಟಾಗಿದೆ ಮತ್ತು ವ್ಯವಸ್ಥೆಯನ್ನು ಪುನಶ್ಚೇತನಗೊಳಿಸಲು ಸಾಧ್ಯವಿಲ್ಲ.
ದಯವಿಟ್ಟು ನಿರ್ಗಮಿಸಿ ಮತ್ತು ಇನ್ನೊಮ್ಮೆ ಪ್ರಯತ್ನಿಸಿ. ಒಂದು ತೊಂದರೆ ಉಂಟಾಗಿದೆ ಮತ್ತು ವ್ಯವಸ್ಥೆಯನ್ನು ಪುನಶ್ಚೇತನಗೊಳಿಸಲು ಸಾಧ್ಯವಿಲ್ಲ. ಸುರಕ್ಷತೆಯ ದೃಷ್ಟಿಯಿಂದ ಎಲ್ಲಾ ವಿಸ್ತರಣೆಯಗಳನ್ನು ನಿಷ್ಕ್ರಿಯಗೊಳಿಸಲಾಗಿದೆ. ಒಂದು ತೊಂದರೆ ಉಂಟಾಗಿದೆ ಮತ್ತು ವ್ಯವಸ್ಥೆಯನ್ನು ಪುನಶ್ಚೇತನಗೊಳಿಸಲು ಸಾಧ್ಯವಿಲ್ಲ. ದಯವಿಟ್ಟು ನಿಮ್ಮ ಗಣಕ ವ್ಯವಸ್ಥಾಪಕರನ್ನು ಸಂಪರ್ಕಸಿ. ’%s’ ಎಂಬ ಹೆಸರಿನ ಅಧಿವೇಶನವೊಂದು ಈಗಾಗಲೆ ಅಸ್ತಿತ್ವದಲ್ಲಿದೆ AUTOSTART_DIR ಹೆಚ್ಚುವರಿ ಆರಂಭಿಕ ಕ್ರಮವಿಧಿಗಳು (_p): ನಿರ್ಗಮನಕ್ಕೆ ಅನುಮತಿಸು ವೀಕ್ಷಿಸು... ಆಜ್ಞೆ (_m): ಟಿಪ್ಪಣಿ (_e): ಅಧಿವೇಶನ ವ್ಯವಸ್ಥಾಪಕನೊಂದಿಗೆ ಸಂಪರ್ಕ ಹೊಂದಲು ಸಾಧ್ಯವಾಗಿಲ್ಲ ICE ಆಲಿಸುವ ಸಾಕೆಟ್ ಅನ್ನು ನಿರ್ಮಿಸಲು ಸಾಧ್ಯವಾಗಿಲ್ಲ: %s ಅಗತ್ಯಾನುಗುಣ ಅಗತ್ಯಾನುಗುಣ ಅಧಿವೇಶನ ಬಳಕೆದಾರ-ಸೂಚಿತ ಅನ್ವಯಗಳನ್ನು ಲೋಡ್ ಮಾಡಬೇಡ ಬಳಕೆದಾರರಿಂದ ಖಚಿತಪಡಿಸುವಂತೆ ಕೇಳಬೇಡ ದೋಷನಿವಾರಣ ಕೋಡ್ ಅನ್ನು ಶಕ್ತಗೊಳಿಸು %s ಕಾರ್ಯಗತಗೊಳಿಸಲು ವಿಫಲಗೊಂಡಿದೆ
 GNOME GNOME ಡಮ್ಮಿ Wayland ನಲ್ಲಿ GNOME ಯಾವುದೆ ಪ್ರತಿರೋಧಗಳಿದ್ದರೂ ಅವನ್ನು ಕಡೆಗಣಿಸು ನಿರ್ಗಮಿಸು ಪ್ರತಿಕ್ರಿಯಿಸುತ್ತಿಲ್ಲ ಓಹ್ ಛೇ! ಎಲ್ಲೋ ಏನೋ ತಪ್ಪಾಗಿದೆ. ಆಯ್ಕೆಗಳು ರೂಢಿಗತ ಸ್ವಯಂಚಾಲನಾ ಕೋಶಗಳನ್ನು ಅತಿಕ್ರಮಿಸು ಚಲಾಯಿಸಲು ಒಂದು ಅಗತ್ಯಾನುಗುಣ ಅಧಿವೇಶನವನ್ನು ಆಯ್ಕೆ ಮಾಡಿ. ಸ್ಥಗಿತಗೊಳಿಸು ಅಸಮಂಜಸ ಆಯ್ಕೆಗಳೊಂದಿಗೆ ಕ್ರಮವಿಧಿಯನ್ನು ಕರೆಯಲಾಗಿದೆ ಮರಳಿ ಬೂಟ್ ಮಾಡು ಹೊಸ ಕ್ಲೈಂಟ್ ಸಂಪರ್ಕಗಳನ್ನು ತಿರಸ್ಕರಿಸಲಾಗುತ್ತಿದೆ ಏಕೆಂದರೆ ಅಧೀವೇಶವನವು ಈಗ ಮುಚ್ಚಲ್ಪಡುತ್ತಿದೆ
 ನೆನಪಿಟ್ಟುಕೊಳ್ಳಲಾದ ಅನ್ವಯ ಅಧಿವೇಶನವನ್ನು ಮರು ಹೆಸರಿಸು (_m) SESSION_NAME ಅಧಿವೇಶನ %d ಅಧಿವೇಶನದ ಹೆಸರುಗಳು ’/’ ಅಕ್ಷರಗಳನ್ನು ಹೊಂದಿರುವಂತಿಲ್ಲ ಅಧಿವೇಶನದ ಹೆಸರುಗಳು ಒಂದು ’.’ ಅಕ್ಷರದಿಂದ ಆರಂಭಿಸುವಂತಿಲ್ಲ ಅಧಿವೇಶನದ ಹೆಸರುಗಳು ’.’ ಅಥವಾ ’/’ ಅಕ್ಷರಗಳನ್ನು ಹೊಂದಿರುವಂತಿಲ್ಲ ಬಳಸಬೇಕಿರುವ ಅಧಿವೇಶನ ವಿಸ್ತರಣೆಯ ಎಚ್ಚರಿಕೆಯನ್ನು ತೋರಿಸು ಪರೀಕ್ಷೆಗಾಗಿ ಫೈಲ್ ವೇಲ್ ಸಂವಾದವನ್ನು ತೋರಿಸು ಆರಂಭಿಕ ಪ್ರೋಗ್ರಾಂಗಳು ಈ ನಮೂದು ನಿಮಗೆ ಒಂದು ಉಳಿಸಲಾದ ಅಧಿವೇಶನವನ್ನು ಆಯ್ಕೆ ಮಾಡಲು ಅನುವು ಮಾಡಿಕೊಡುತ್ತದೆ ಈ ಕ್ರಮವಿಧಿಯು ನಿರ್ಗಮನವನ್ನು ತಡೆಯುತ್ತಿದೆ. ಈ ಅಧಿವೇಶನವು ನಿಮ್ಮನ್ನು GNOME ಗೆ ಪ್ರವೇಶಿಸುವಂತೆ ಮಾಡುತ್ತದೆ ಈ ಅಧಿವೇಶನವು Wayland ಬಳಸಿಕೊಂಡು GNOME ಗೆ ಪ್ರವೇಶಿಸುತ್ತದೆ ಈ ಅನ್ವಯದ ಆವೃತ್ತಿ ನಿರ್ಗಮಿಸುವಾಗ ಚಾಲನೆಯಲ್ಲಿರುವ ಅನ್ವಯಗಳನ್ನು ಸ್ವಯಂಚಾಲಿತವಾಗಿ ನೆನಪಿಟ್ಟುಕೊ (_A) ಮುಂದುವರೆ(_C) ನಿರ್ಗಮಿಸು (_L) ನಿರ್ಗಮಿಸು (_L) ಹೆಸರು (_N): ಹೊಸ ಅಧಿವೇಶನ(_N) ಪ್ರಸಕ್ತ ಚಾಲನೆಯಲ್ಲಿರುವ ಅನ್ವಯವನ್ನು ನೆನಪಿಟ್ಟುಕೊ (_R) ಅಧಿವೇಶನವನ್ನು ತೆಗೆದು ಹಾಕು(_R) 