��    3      �  G   L      h     i  4  �     �  T   �  i   +  \   �  '   �          (  (   5  )   ^     �     �  #   �  '   �  $   �     	     %	     ;	     A	     M	      [	     |	     �	  !   �	  '   �	  %   �	  	   
  '   
     5
  P   <
     �
     �
     �
  
   �
  ;   �
  3     Q   <     �     �  &   �  *   �            '     H  	   d     n     w     �     �  �  �     q  P  �     �  h   �  y   f  b   �  )   C     m     |  '   �  &   �  	   �     �  )   �  5   %  '   [     �     �     �     �     �  *   �                  "   8  .   [  	   �  "   �     �  H   �     	          /  	   ;  *   E  $   p  G   �     �     �  8     .   F     u  "   �     �     �     �     �     �     �                                    	   1      $   2                         *              0   -            "   %   &      3           !   /             #                   
          +                                         .   '   (             )   ,     — the GNOME session manager %s [OPTION…] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can’t recover.
Please log out and try again. A problem has occurred and the system can’t recover. All extensions have been disabled as a precaution. A problem has occurred and the system can’t recover. Please contact a system administrator A session named “%s” already exists AUTOSTART_DIR Allow logout Could not connect to the session manager Could not create ICE listening socket: %s Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don’t prompt for user confirmation Enable debugging code Failed to execute %s
 GNOME GNOME dummy GNOME on Xorg Ignoring any existing inhibitors Log out Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Session %d Session names are not allowed to contain “/” characters Session names are not allowed to start with “.” Session names are not allowed to start with “.” or contain “/” characters Session to use Show extension warning Show the fail whale dialog for testing This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME Version of this application _Continue _Log Out _Log out _New Session _Remove Session Project-Id-Version: gnome-session 2.6-branch
Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-session/issues
PO-Revision-Date: 2018-02-06 17:39+0200
Last-Translator: Pieter Schalk Schoeman <pieter@sonbesie.co.za>
Language-Team: Afrikaans <pieter@sonbesie.co.za>
Language: af
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.0.3
X-Project-Style: gnome
  — die GNOME-sessiebestuurder %s [KEUSE...] OPDRAG

Voer OPDRAG uit terwyl sommige sessiefunksionaliteit onderdruk word.

  -h, --help        Wys dié hulp
  --version         Wys programweergawe
  --app-id ID       Die program-ID om te gebruik tydens
                    onderdrukking (opsioneel)
  --reason REDE     Die rede vir onderdrukking (opsioneel)
  --inhibit ARG     Dinge om te onderdruk, dubbelpuntgeskyde lys van:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Moenie OPDRAG begin nie en wag bloot vir altyd

As geen --inhibit-keuse gegee is nie, word "idle" aangeneem.
 "%s" benodig 'n parameter
 'n Fout het voorgekom en die stelsel kan nie herstel daarvan nie.
Meld af en probeer gerus weer aanmeld. 'n Fout het voorgekom en die stelsel kan nie daarvan herstel nie. Alle uitbreidings is as voorsorgmaatreël gedeaktiveer. 'n Fout het voorgekom en die stelsel kan nie daarvan herstel nie. Kontak die stelseladministrateur 'n Sessie met die naam "%s" bestaan reeds OUTOBEGIN_GIDS Laat afmelding toe Kon nie met sessiebestuurder koppel nie Kon nie 'n ICE-luistersok skep nie: %s Pasgemaak Pasgemaakte sessie Skakel die hardeware versnelling toets af Moenie gebruiker-gespesifiseerde toepassings laai nie Moenie vra vir gebruikerbevestiging nie Aktiveer ontfoutkode Uitvoering van "%s" het misluk
 GNOME GNOME dummy GNOME op Xorg Gaan enige bestaande inhibeerders ignoreer Meld af Reageer nie Gits!  Iets het verkeerd geloop. Oorheers standaard outobegin gidse Kies asb. 'n pasgemaakte sessie om uit te voer Skakel af Program geroep met botsende opsies Herbegin Nuwe klientverbinding word geweier omdat die sessie tans afgesluit word
 Onthou die toepassing _Hernoem sessie SESSIE_NAAM Sessie %d Sessiename mag nie "/"-karakters bevat nie Sessiename mag nie met "." begin nie Sessiename mag nie met "." begin nie en mag nie "/"-karakters bevat nie Sessie om te gebruik Wys uitbreidingwaarskuwing Wys die dialoog vir ernstige foute (vir toetsdoeleindes) Die inskrywing laat u 'n gestoorde sessie kies Dié program keer afmelding. Die sessie laat u by GNOME aanmeld Weergawe van hierdie program _Gaan voort _Meld af _Meld af _Nuwe sessie _Verwyder sessie 